/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.CartaoCredito;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class CartaoCreditoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CartaoCredito cartaoCredito) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cartaoCredito);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CartaoCredito cartaoCredito) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cartaoCredito = em.merge(cartaoCredito);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cartaoCredito.getId();
                if (findCartaoCredito(id) == null) {
                    throw new NonexistentEntityException("The cartaoCredito with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CartaoCredito cartaoCredito;
            try {
                cartaoCredito = em.getReference(CartaoCredito.class, id);
                cartaoCredito.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cartaoCredito with id " + id + " no longer exists.", enfe);
            }
            em.remove(cartaoCredito);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CartaoCredito> findCartaoCreditoEntities() {
        return findCartaoCreditoEntities(true, -1, -1);
    }

    public List<CartaoCredito> findCartaoCreditoEntities(int maxResults, int firstResult) {
        return findCartaoCreditoEntities(false, maxResults, firstResult);
    }

    private List<CartaoCredito> findCartaoCreditoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CartaoCredito.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CartaoCredito findCartaoCredito(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CartaoCredito.class, id);
        } finally {
            em.close();
        }
    }

    public int getCartaoCreditoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CartaoCredito> rt = cq.from(CartaoCredito.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findCartoesAtivos() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("CartaoCredito.findByStatus").setParameter("status", true).getResultList();
        } finally {
            em.close();
        }
    }

}
