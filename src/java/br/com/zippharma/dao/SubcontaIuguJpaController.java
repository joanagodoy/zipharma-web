/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.dao.exceptions.PreexistingEntityException;
import br.com.zippharma.domain.SubcontaIugu;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class SubcontaIuguJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SubcontaIugu subcontaIugu) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(subcontaIugu);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSubcontaIugu(subcontaIugu.getFarmacia()) != null) {
                throw new PreexistingEntityException("SubcontaIugu " + subcontaIugu + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SubcontaIugu subcontaIugu) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            subcontaIugu = em.merge(subcontaIugu);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = subcontaIugu.getFarmacia();
                if (findSubcontaIugu(id) == null) {
                    throw new NonexistentEntityException("The subcontaIugu with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SubcontaIugu subcontaIugu;
            try {
                subcontaIugu = em.getReference(SubcontaIugu.class, id);
                subcontaIugu.getFarmacia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The subcontaIugu with id " + id + " no longer exists.", enfe);
            }
            em.remove(subcontaIugu);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SubcontaIugu> findSubcontaIuguEntities() {
        return findSubcontaIuguEntities(true, -1, -1);
    }

    public List<SubcontaIugu> findSubcontaIuguEntities(int maxResults, int firstResult) {
        return findSubcontaIuguEntities(false, maxResults, firstResult);
    }

    private List<SubcontaIugu> findSubcontaIuguEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SubcontaIugu.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SubcontaIugu findSubcontaIugu(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SubcontaIugu.class, id);
        } finally {
            em.close();
        }
    }

    public int getSubcontaIuguCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SubcontaIugu> rt = cq.from(SubcontaIugu.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
