/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Clinica;
import br.com.zippharma.domain.Especialidade;
import br.com.zippharma.domain.Especialista;
import br.com.zippharma.domain.EspecialistaImagem;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class EspecialistaJpaController1 implements Serializable {

    public EspecialistaJpaController1(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Especialista especialista) {
        if (especialista.getEspecialistaImagemList() == null) {
            especialista.setEspecialistaImagemList(new ArrayList<EspecialistaImagem>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clinica clinica = especialista.getClinica();
            if (clinica != null) {
                clinica = em.getReference(clinica.getClass(), clinica.getId());
                especialista.setClinica(clinica);
            }
            Especialidade especialidade = especialista.getEspecialidade();
            if (especialidade != null) {
                especialidade = em.getReference(especialidade.getClass(), especialidade.getId());
                especialista.setEspecialidade(especialidade);
            }
            List<EspecialistaImagem> attachedEspecialistaImagemList = new ArrayList<EspecialistaImagem>();
            for (EspecialistaImagem especialistaImagemListEspecialistaImagemToAttach : especialista.getEspecialistaImagemList()) {
                especialistaImagemListEspecialistaImagemToAttach = em.getReference(especialistaImagemListEspecialistaImagemToAttach.getClass(), especialistaImagemListEspecialistaImagemToAttach.getId());
                attachedEspecialistaImagemList.add(especialistaImagemListEspecialistaImagemToAttach);
            }
            especialista.setEspecialistaImagemList(attachedEspecialistaImagemList);
            em.persist(especialista);
            if (clinica != null) {
                clinica.getEspecialistaList().add(especialista);
                clinica = em.merge(clinica);
            }
            if (especialidade != null) {
                especialidade.getEspecialistaList().add(especialista);
                especialidade = em.merge(especialidade);
            }
            for (EspecialistaImagem especialistaImagemListEspecialistaImagem : especialista.getEspecialistaImagemList()) {
                Especialista oldEspecialistaOfEspecialistaImagemListEspecialistaImagem = especialistaImagemListEspecialistaImagem.getEspecialista();
                especialistaImagemListEspecialistaImagem.setEspecialista(especialista);
                especialistaImagemListEspecialistaImagem = em.merge(especialistaImagemListEspecialistaImagem);
                if (oldEspecialistaOfEspecialistaImagemListEspecialistaImagem != null) {
                    oldEspecialistaOfEspecialistaImagemListEspecialistaImagem.getEspecialistaImagemList().remove(especialistaImagemListEspecialistaImagem);
                    oldEspecialistaOfEspecialistaImagemListEspecialistaImagem = em.merge(oldEspecialistaOfEspecialistaImagemListEspecialistaImagem);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Especialista especialista) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Especialista persistentEspecialista = em.find(Especialista.class, especialista.getId());
            Clinica clinicaOld = persistentEspecialista.getClinica();
            Clinica clinicaNew = especialista.getClinica();
            Especialidade especialidadeOld = persistentEspecialista.getEspecialidade();
            Especialidade especialidadeNew = especialista.getEspecialidade();
            List<EspecialistaImagem> especialistaImagemListOld = persistentEspecialista.getEspecialistaImagemList();
            List<EspecialistaImagem> especialistaImagemListNew = especialista.getEspecialistaImagemList();
            List<String> illegalOrphanMessages = null;
            for (EspecialistaImagem especialistaImagemListOldEspecialistaImagem : especialistaImagemListOld) {
                if (!especialistaImagemListNew.contains(especialistaImagemListOldEspecialistaImagem)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain EspecialistaImagem " + especialistaImagemListOldEspecialistaImagem + " since its especialista field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clinicaNew != null) {
                clinicaNew = em.getReference(clinicaNew.getClass(), clinicaNew.getId());
                especialista.setClinica(clinicaNew);
            }
            if (especialidadeNew != null) {
                especialidadeNew = em.getReference(especialidadeNew.getClass(), especialidadeNew.getId());
                especialista.setEspecialidade(especialidadeNew);
            }
            List<EspecialistaImagem> attachedEspecialistaImagemListNew = new ArrayList<EspecialistaImagem>();
            for (EspecialistaImagem especialistaImagemListNewEspecialistaImagemToAttach : especialistaImagemListNew) {
                especialistaImagemListNewEspecialistaImagemToAttach = em.getReference(especialistaImagemListNewEspecialistaImagemToAttach.getClass(), especialistaImagemListNewEspecialistaImagemToAttach.getId());
                attachedEspecialistaImagemListNew.add(especialistaImagemListNewEspecialistaImagemToAttach);
            }
            especialistaImagemListNew = attachedEspecialistaImagemListNew;
            especialista.setEspecialistaImagemList(especialistaImagemListNew);
            especialista = em.merge(especialista);
            if (clinicaOld != null && !clinicaOld.equals(clinicaNew)) {
                clinicaOld.getEspecialistaList().remove(especialista);
                clinicaOld = em.merge(clinicaOld);
            }
            if (clinicaNew != null && !clinicaNew.equals(clinicaOld)) {
                clinicaNew.getEspecialistaList().add(especialista);
                clinicaNew = em.merge(clinicaNew);
            }
            if (especialidadeOld != null && !especialidadeOld.equals(especialidadeNew)) {
                especialidadeOld.getEspecialistaList().remove(especialista);
                especialidadeOld = em.merge(especialidadeOld);
            }
            if (especialidadeNew != null && !especialidadeNew.equals(especialidadeOld)) {
                especialidadeNew.getEspecialistaList().add(especialista);
                especialidadeNew = em.merge(especialidadeNew);
            }
            for (EspecialistaImagem especialistaImagemListNewEspecialistaImagem : especialistaImagemListNew) {
                if (!especialistaImagemListOld.contains(especialistaImagemListNewEspecialistaImagem)) {
                    Especialista oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem = especialistaImagemListNewEspecialistaImagem.getEspecialista();
                    especialistaImagemListNewEspecialistaImagem.setEspecialista(especialista);
                    especialistaImagemListNewEspecialistaImagem = em.merge(especialistaImagemListNewEspecialistaImagem);
                    if (oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem != null && !oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem.equals(especialista)) {
                        oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem.getEspecialistaImagemList().remove(especialistaImagemListNewEspecialistaImagem);
                        oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem = em.merge(oldEspecialistaOfEspecialistaImagemListNewEspecialistaImagem);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = especialista.getId();
                if (findEspecialista(id) == null) {
                    throw new NonexistentEntityException("The especialista with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Especialista especialista;
            try {
                especialista = em.getReference(Especialista.class, id);
                especialista.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The especialista with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<EspecialistaImagem> especialistaImagemListOrphanCheck = especialista.getEspecialistaImagemList();
            for (EspecialistaImagem especialistaImagemListOrphanCheckEspecialistaImagem : especialistaImagemListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Especialista (" + especialista + ") cannot be destroyed since the EspecialistaImagem " + especialistaImagemListOrphanCheckEspecialistaImagem + " in its especialistaImagemList field has a non-nullable especialista field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Clinica clinica = especialista.getClinica();
            if (clinica != null) {
                clinica.getEspecialistaList().remove(especialista);
                clinica = em.merge(clinica);
            }
            Especialidade especialidade = especialista.getEspecialidade();
            if (especialidade != null) {
                especialidade.getEspecialistaList().remove(especialista);
                especialidade = em.merge(especialidade);
            }
            em.remove(especialista);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Especialista> findEspecialistaEntities() {
        return findEspecialistaEntities(true, -1, -1);
    }

    public List<Especialista> findEspecialistaEntities(int maxResults, int firstResult) {
        return findEspecialistaEntities(false, maxResults, firstResult);
    }

    private List<Especialista> findEspecialistaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Especialista.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Especialista findEspecialista(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Especialista.class, id);
        } finally {
            em.close();
        }
    }

    public int getEspecialistaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Especialista> rt = cq.from(Especialista.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
