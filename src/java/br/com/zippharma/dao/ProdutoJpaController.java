/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Produto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Subcategoria;
import java.util.ArrayList;
import java.util.List;
import br.com.zippharma.domain.ProdutoImagem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class ProdutoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Produto produto) {
        if (produto.getSubcategoriaList() == null) {
            produto.setSubcategoriaList(new ArrayList<Subcategoria>());
        }
        if (produto.getProdutoImagemList() == null) {
            produto.setProdutoImagemList(new ArrayList<ProdutoImagem>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Subcategoria> attachedSubcategoriaList = new ArrayList<Subcategoria>();
            for (Subcategoria subcategoriaListSubcategoriaToAttach : produto.getSubcategoriaList()) {
                subcategoriaListSubcategoriaToAttach = em.getReference(subcategoriaListSubcategoriaToAttach.getClass(), subcategoriaListSubcategoriaToAttach.getId());
                attachedSubcategoriaList.add(subcategoriaListSubcategoriaToAttach);
            }
            produto.setSubcategoriaList(attachedSubcategoriaList);
            List<ProdutoImagem> attachedProdutoImagemList = new ArrayList<ProdutoImagem>();
            for (ProdutoImagem produtoImagemListProdutoImagemToAttach : produto.getProdutoImagemList()) {
                produtoImagemListProdutoImagemToAttach = em.getReference(produtoImagemListProdutoImagemToAttach.getClass(), produtoImagemListProdutoImagemToAttach.getId());
                attachedProdutoImagemList.add(produtoImagemListProdutoImagemToAttach);
            }
            produto.setProdutoImagemList(attachedProdutoImagemList);
            em.persist(produto);
            for (Subcategoria subcategoriaListSubcategoria : produto.getSubcategoriaList()) {
                subcategoriaListSubcategoria.getProdutoList().add(produto);
                subcategoriaListSubcategoria = em.merge(subcategoriaListSubcategoria);
            }
            for (ProdutoImagem produtoImagemListProdutoImagem : produto.getProdutoImagemList()) {
                Produto oldProdutoOfProdutoImagemListProdutoImagem = produtoImagemListProdutoImagem.getProduto();
                produtoImagemListProdutoImagem.setProduto(produto);
                produtoImagemListProdutoImagem = em.merge(produtoImagemListProdutoImagem);
                if (oldProdutoOfProdutoImagemListProdutoImagem != null) {
                    oldProdutoOfProdutoImagemListProdutoImagem.getProdutoImagemList().remove(produtoImagemListProdutoImagem);
                    oldProdutoOfProdutoImagemListProdutoImagem = em.merge(oldProdutoOfProdutoImagemListProdutoImagem);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Produto produto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Produto persistentProduto = em.find(Produto.class, produto.getId());
            List<Subcategoria> subcategoriaListOld = persistentProduto.getSubcategoriaList();
            List<Subcategoria> subcategoriaListNew = produto.getSubcategoriaList();
            List<ProdutoImagem> produtoImagemListOld = persistentProduto.getProdutoImagemList();
            List<ProdutoImagem> produtoImagemListNew = produto.getProdutoImagemList();
            List<String> illegalOrphanMessages = null;
            for (ProdutoImagem produtoImagemListOldProdutoImagem : produtoImagemListOld) {
                if (!produtoImagemListNew.contains(produtoImagemListOldProdutoImagem)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProdutoImagem " + produtoImagemListOldProdutoImagem + " since its produto field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Subcategoria> attachedSubcategoriaListNew = new ArrayList<Subcategoria>();
            for (Subcategoria subcategoriaListNewSubcategoriaToAttach : subcategoriaListNew) {
                subcategoriaListNewSubcategoriaToAttach = em.getReference(subcategoriaListNewSubcategoriaToAttach.getClass(), subcategoriaListNewSubcategoriaToAttach.getId());
                attachedSubcategoriaListNew.add(subcategoriaListNewSubcategoriaToAttach);
            }
            subcategoriaListNew = attachedSubcategoriaListNew;
            produto.setSubcategoriaList(subcategoriaListNew);
            List<ProdutoImagem> attachedProdutoImagemListNew = new ArrayList<ProdutoImagem>();
            for (ProdutoImagem produtoImagemListNewProdutoImagemToAttach : produtoImagemListNew) {
                produtoImagemListNewProdutoImagemToAttach = em.getReference(produtoImagemListNewProdutoImagemToAttach.getClass(), produtoImagemListNewProdutoImagemToAttach.getId());
                attachedProdutoImagemListNew.add(produtoImagemListNewProdutoImagemToAttach);
            }
            produtoImagemListNew = attachedProdutoImagemListNew;
            produto.setProdutoImagemList(produtoImagemListNew);
            produto = em.merge(produto);
            for (Subcategoria subcategoriaListOldSubcategoria : subcategoriaListOld) {
                if (!subcategoriaListNew.contains(subcategoriaListOldSubcategoria)) {
                    subcategoriaListOldSubcategoria.getProdutoList().remove(produto);
                    subcategoriaListOldSubcategoria = em.merge(subcategoriaListOldSubcategoria);
                }
            }
            for (Subcategoria subcategoriaListNewSubcategoria : subcategoriaListNew) {
                if (!subcategoriaListOld.contains(subcategoriaListNewSubcategoria)) {
                    subcategoriaListNewSubcategoria.getProdutoList().add(produto);
                    subcategoriaListNewSubcategoria = em.merge(subcategoriaListNewSubcategoria);
                }
            }
            for (ProdutoImagem produtoImagemListNewProdutoImagem : produtoImagemListNew) {
                if (!produtoImagemListOld.contains(produtoImagemListNewProdutoImagem)) {
                    Produto oldProdutoOfProdutoImagemListNewProdutoImagem = produtoImagemListNewProdutoImagem.getProduto();
                    produtoImagemListNewProdutoImagem.setProduto(produto);
                    produtoImagemListNewProdutoImagem = em.merge(produtoImagemListNewProdutoImagem);
                    if (oldProdutoOfProdutoImagemListNewProdutoImagem != null && !oldProdutoOfProdutoImagemListNewProdutoImagem.equals(produto)) {
                        oldProdutoOfProdutoImagemListNewProdutoImagem.getProdutoImagemList().remove(produtoImagemListNewProdutoImagem);
                        oldProdutoOfProdutoImagemListNewProdutoImagem = em.merge(oldProdutoOfProdutoImagemListNewProdutoImagem);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = produto.getId();
                if (findProduto(id) == null) {
                    throw new NonexistentEntityException("The produto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Produto produto;
            try {
                produto = em.getReference(Produto.class, id);
                produto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The produto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ProdutoImagem> produtoImagemListOrphanCheck = produto.getProdutoImagemList();
            for (ProdutoImagem produtoImagemListOrphanCheckProdutoImagem : produtoImagemListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Produto (" + produto + ") cannot be destroyed since the ProdutoImagem " + produtoImagemListOrphanCheckProdutoImagem + " in its produtoImagemList field has a non-nullable produto field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Subcategoria> subcategoriaList = produto.getSubcategoriaList();
            for (Subcategoria subcategoriaListSubcategoria : subcategoriaList) {
                subcategoriaListSubcategoria.getProdutoList().remove(produto);
                subcategoriaListSubcategoria = em.merge(subcategoriaListSubcategoria);
            }
            em.remove(produto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Produto> findProdutoEntities() {
        return findProdutoEntities(true, -1, -1);
    }

    public List<Produto> findProdutoEntities(int maxResults, int firstResult) {
        return findProdutoEntities(false, maxResults, firstResult);
    }

    private List<Produto> findProdutoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Produto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Produto findProduto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Produto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProdutoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Produto> rt = cq.from(Produto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findProdutos(Integer offset, Integer max, Integer categoria, String query) {

        EntityManager em = getEntityManager();
        try {
            if (categoria != null) {
                return em.createNativeQuery("SELECT p.* FROM produto p LEFT JOIN produto_subcategoria ps ON ps.produto = p.id WHERE ps.subcategoria = ? LIMIT ? OFFSET ?", Produto.class).setParameter(1, categoria).setParameter(2, max).setParameter(3, offset).getResultList();
            } else if (query != null && !query.trim().isEmpty()) {
                query = query.trim().replace(" ", "%");
                return em.createNativeQuery("SELECT p.* FROM produto p RIGHT JOIN (SELECT DISTINCT p.id, (CASE WHEN (p.codigoBarra = ? OR p.id = ?) THEN 1 WHEN (p.descricao LIKE ?) THEN 2 WHEN (m.descricao LIKE ? OR sc.descricao LIKE ? OR c.descricao LIKE ?) THEN 3 WHEN (p.informacoes LIKE ?) THEN 4 ELSE 10 END) as ordem FROM produto p LEFT JOIN marca m ON p.marca = m.id LEFT JOIN produto_subcategoria ps ON ps.produto = p.id LEFT JOIN subcategoria sc ON ps.subcategoria = sc.id LEFT JOIN categoria c ON sc.categoria = c.id WHERE p.codigoBarra = ? OR p.id = ? OR p.descricao LIKE ? OR p.informacoes LIKE ? OR m.descricao LIKE ? OR sc.descricao LIKE ? OR c.descricao LIKE ? ORDER BY ordem ASC LIMIT ? OFFSET ?) t ON p.id = t.id", Produto.class).setParameter(1, query).setParameter(2, query).setParameter(3, "%" + query + "%").setParameter(4, "%" + query + "%").setParameter(5, "%" + query + "%").setParameter(6, "%" + query + "%").setParameter(7, "%" + query + "%").setParameter(8, query).setParameter(9, query).setParameter(10, "%" + query + "%").setParameter(11, "%" + query + "%").setParameter(12, "%" + query + "%").setParameter(13, "%" + query + "%").setParameter(14, "%" + query + "%").setParameter(15, max).setParameter(16, offset).getResultList();
            } else {
                return em.createNativeQuery("SELECT p.* FROM produto p WHERE 1=1 LIMIT ? OFFSET ?", Produto.class).setParameter(1, max).setParameter(2, offset).getResultList();
            }
        } finally {
            em.close();
        }

    }

    public Object findProdutosMaisVendidos(Integer max) {
        EntityManager em = getEntityManager();
        try {
            return em.createNativeQuery("SELECT p.* FROM produto p RIGHT JOIN (SELECT pi.produto,COUNT(pi.produto) FROM pedido_item pi LEFT JOIN pedido pe ON pi.pedido = pe.id GROUP BY pi.produto ORDER BY COUNT(pi.produto) DESC LIMIT ?) q ON p.id = q.produto", Produto.class).setParameter(1, max).getResultList();
        } finally {
            em.close();
        }
    }

}
