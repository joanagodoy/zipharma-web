/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Cr;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Especialidade;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Usuario
 */
public class CrJpaController implements Serializable {

    public CrJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cr cr) {
        if (cr.getEspecialidadeList() == null) {
            cr.setEspecialidadeList(new ArrayList<Especialidade>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Especialidade> attachedEspecialidadeList = new ArrayList<Especialidade>();
            for (Especialidade especialidadeListEspecialidadeToAttach : cr.getEspecialidadeList()) {
                especialidadeListEspecialidadeToAttach = em.getReference(especialidadeListEspecialidadeToAttach.getClass(), especialidadeListEspecialidadeToAttach.getId());
                attachedEspecialidadeList.add(especialidadeListEspecialidadeToAttach);
            }
            cr.setEspecialidadeList(attachedEspecialidadeList);
            em.persist(cr);
            for (Especialidade especialidadeListEspecialidade : cr.getEspecialidadeList()) {
                Cr oldCrOfEspecialidadeListEspecialidade = especialidadeListEspecialidade.getCr();
                especialidadeListEspecialidade.setCr(cr);
                especialidadeListEspecialidade = em.merge(especialidadeListEspecialidade);
                if (oldCrOfEspecialidadeListEspecialidade != null) {
                    oldCrOfEspecialidadeListEspecialidade.getEspecialidadeList().remove(especialidadeListEspecialidade);
                    oldCrOfEspecialidadeListEspecialidade = em.merge(oldCrOfEspecialidadeListEspecialidade);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cr cr) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cr persistentCr = em.find(Cr.class, cr.getId());
            List<Especialidade> especialidadeListOld = persistentCr.getEspecialidadeList();
            List<Especialidade> especialidadeListNew = cr.getEspecialidadeList();
            List<String> illegalOrphanMessages = null;
            for (Especialidade especialidadeListOldEspecialidade : especialidadeListOld) {
                if (!especialidadeListNew.contains(especialidadeListOldEspecialidade)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Especialidade " + especialidadeListOldEspecialidade + " since its cr field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Especialidade> attachedEspecialidadeListNew = new ArrayList<Especialidade>();
            for (Especialidade especialidadeListNewEspecialidadeToAttach : especialidadeListNew) {
                especialidadeListNewEspecialidadeToAttach = em.getReference(especialidadeListNewEspecialidadeToAttach.getClass(), especialidadeListNewEspecialidadeToAttach.getId());
                attachedEspecialidadeListNew.add(especialidadeListNewEspecialidadeToAttach);
            }
            especialidadeListNew = attachedEspecialidadeListNew;
            cr.setEspecialidadeList(especialidadeListNew);
            cr = em.merge(cr);
            for (Especialidade especialidadeListNewEspecialidade : especialidadeListNew) {
                if (!especialidadeListOld.contains(especialidadeListNewEspecialidade)) {
                    Cr oldCrOfEspecialidadeListNewEspecialidade = especialidadeListNewEspecialidade.getCr();
                    especialidadeListNewEspecialidade.setCr(cr);
                    especialidadeListNewEspecialidade = em.merge(especialidadeListNewEspecialidade);
                    if (oldCrOfEspecialidadeListNewEspecialidade != null && !oldCrOfEspecialidadeListNewEspecialidade.equals(cr)) {
                        oldCrOfEspecialidadeListNewEspecialidade.getEspecialidadeList().remove(especialidadeListNewEspecialidade);
                        oldCrOfEspecialidadeListNewEspecialidade = em.merge(oldCrOfEspecialidadeListNewEspecialidade);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cr.getId();
                if (findCr(id) == null) {
                    throw new NonexistentEntityException("The cr with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cr cr;
            try {
                cr = em.getReference(Cr.class, id);
                cr.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cr with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Especialidade> especialidadeListOrphanCheck = cr.getEspecialidadeList();
            for (Especialidade especialidadeListOrphanCheckEspecialidade : especialidadeListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cr (" + cr + ") cannot be destroyed since the Especialidade " + especialidadeListOrphanCheckEspecialidade + " in its especialidadeList field has a non-nullable cr field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(cr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cr> findCrEntities() {
        return findCrEntities(true, -1, -1);
    }

    public List<Cr> findCrEntities(int maxResults, int firstResult) {
        return findCrEntities(false, maxResults, firstResult);
    }

    private List<Cr> findCrEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cr.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cr findCr(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cr.class, id);
        } finally {
            em.close();
        }
    }

    public int getCrCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cr> rt = cq.from(Cr.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
