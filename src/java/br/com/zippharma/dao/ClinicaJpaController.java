/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Clinica;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Especialista;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class ClinicaJpaController implements Serializable {

//    public ClinicaJpaController(EntityManagerFactory emf) {
//        this.emf = emf;
//    }
//    private EntityManagerFactory emf = null;
    
    @PersistenceUnit
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Clinica clinica) {
        if (clinica.getEspecialistaList() == null) {
            clinica.setEspecialistaList(new ArrayList<Especialista>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Especialista> attachedEspecialistaList = new ArrayList<Especialista>();
            for (Especialista especialistaListEspecialistaToAttach : clinica.getEspecialistaList()) {
                especialistaListEspecialistaToAttach = em.getReference(especialistaListEspecialistaToAttach.getClass(), especialistaListEspecialistaToAttach.getId());
                attachedEspecialistaList.add(especialistaListEspecialistaToAttach);
            }
            clinica.setEspecialistaList(attachedEspecialistaList);
            em.persist(clinica);
            for (Especialista especialistaListEspecialista : clinica.getEspecialistaList()) {
                Clinica oldClinicaOfEspecialistaListEspecialista = especialistaListEspecialista.getClinica();
                especialistaListEspecialista.setClinica(clinica);
                especialistaListEspecialista = em.merge(especialistaListEspecialista);
                if (oldClinicaOfEspecialistaListEspecialista != null) {
                    oldClinicaOfEspecialistaListEspecialista.getEspecialistaList().remove(especialistaListEspecialista);
                    oldClinicaOfEspecialistaListEspecialista = em.merge(oldClinicaOfEspecialistaListEspecialista);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Clinica clinica) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clinica persistentClinica = em.find(Clinica.class, clinica.getId());
            List<Especialista> especialistaListOld = persistentClinica.getEspecialistaList();
            List<Especialista> especialistaListNew = clinica.getEspecialistaList();
            List<String> illegalOrphanMessages = null;
            for (Especialista especialistaListOldEspecialista : especialistaListOld) {
                if (!especialistaListNew.contains(especialistaListOldEspecialista)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Especialista " + especialistaListOldEspecialista + " since its clinica field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Especialista> attachedEspecialistaListNew = new ArrayList<Especialista>();
            for (Especialista especialistaListNewEspecialistaToAttach : especialistaListNew) {
                especialistaListNewEspecialistaToAttach = em.getReference(especialistaListNewEspecialistaToAttach.getClass(), especialistaListNewEspecialistaToAttach.getId());
                attachedEspecialistaListNew.add(especialistaListNewEspecialistaToAttach);
            }
            especialistaListNew = attachedEspecialistaListNew;
            clinica.setEspecialistaList(especialistaListNew);
            clinica = em.merge(clinica);
            for (Especialista especialistaListNewEspecialista : especialistaListNew) {
                if (!especialistaListOld.contains(especialistaListNewEspecialista)) {
                    Clinica oldClinicaOfEspecialistaListNewEspecialista = especialistaListNewEspecialista.getClinica();
                    especialistaListNewEspecialista.setClinica(clinica);
                    especialistaListNewEspecialista = em.merge(especialistaListNewEspecialista);
                    if (oldClinicaOfEspecialistaListNewEspecialista != null && !oldClinicaOfEspecialistaListNewEspecialista.equals(clinica)) {
                        oldClinicaOfEspecialistaListNewEspecialista.getEspecialistaList().remove(especialistaListNewEspecialista);
                        oldClinicaOfEspecialistaListNewEspecialista = em.merge(oldClinicaOfEspecialistaListNewEspecialista);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = clinica.getId();
                if (findClinica(id) == null) {
                    throw new NonexistentEntityException("The clinica with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clinica clinica;
            try {
                clinica = em.getReference(Clinica.class, id);
                clinica.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The clinica with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Especialista> especialistaListOrphanCheck = clinica.getEspecialistaList();
            for (Especialista especialistaListOrphanCheckEspecialista : especialistaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Clinica (" + clinica + ") cannot be destroyed since the Especialista " + especialistaListOrphanCheckEspecialista + " in its especialistaList field has a non-nullable clinica field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(clinica);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Clinica> findClinicaEntities() {
        return findClinicaEntities(true, -1, -1);
    }

    public List<Clinica> findClinicaEntities(int maxResults, int firstResult) {
        return findClinicaEntities(false, maxResults, firstResult);
    }

    private List<Clinica> findClinicaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Clinica.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Clinica findClinica(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Clinica.class, id);
        } finally {
            em.close();
        }
    }

    public int getClinicaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Clinica> rt = cq.from(Clinica.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
