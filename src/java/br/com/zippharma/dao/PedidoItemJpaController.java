/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.dao.exceptions.PreexistingEntityException;
import br.com.zippharma.domain.PedidoItem;
import br.com.zippharma.domain.PedidoItemPK;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class PedidoItemJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PedidoItem pedidoItem) throws PreexistingEntityException, Exception {
        if (pedidoItem.getPedidoItemPK() == null) {
            pedidoItem.setPedidoItemPK(new PedidoItemPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(pedidoItem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPedidoItem(pedidoItem.getPedidoItemPK()) != null) {
                throw new PreexistingEntityException("PedidoItem " + pedidoItem + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PedidoItem pedidoItem) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            pedidoItem = em.merge(pedidoItem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PedidoItemPK id = pedidoItem.getPedidoItemPK();
                if (findPedidoItem(id) == null) {
                    throw new NonexistentEntityException("The pedidoItem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PedidoItemPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PedidoItem pedidoItem;
            try {
                pedidoItem = em.getReference(PedidoItem.class, id);
                pedidoItem.getPedidoItemPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedidoItem with id " + id + " no longer exists.", enfe);
            }
            em.remove(pedidoItem);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PedidoItem> findByPedido(Long idPedido) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("PedidoItem.findByPedido").setParameter("pedido", idPedido).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public List<PedidoItem> findPedidoItemEntities() {
        return findPedidoItemEntities(true, -1, -1);
    }

    public List<PedidoItem> findPedidoItemEntities(int maxResults, int firstResult) {
        return findPedidoItemEntities(false, maxResults, firstResult);
    }

    private List<PedidoItem> findPedidoItemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PedidoItem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PedidoItem findPedidoItem(PedidoItemPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PedidoItem.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoItemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PedidoItem> rt = cq.from(PedidoItem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
