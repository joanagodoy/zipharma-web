/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Cr;
import br.com.zippharma.domain.Especialidade;
import br.com.zippharma.domain.Especialista;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class EspecialidadeJpaController implements Serializable {

//    public EspecialidadeJpaController(EntityManagerFactory emf) {
//        this.emf = emf;
//    }
//    private EntityManagerFactory emf = null;
    
    @PersistenceUnit
    private EntityManagerFactory emf;
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Especialidade especialidade) {
        if (especialidade.getEspecialistaList() == null) {
            especialidade.setEspecialistaList(new ArrayList<Especialista>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cr cr = especialidade.getCr();
            if (cr != null) {
                cr = em.getReference(cr.getClass(), cr.getId());
                especialidade.setCr(cr);
            }
            List<Especialista> attachedEspecialistaList = new ArrayList<Especialista>();
            for (Especialista especialistaListEspecialistaToAttach : especialidade.getEspecialistaList()) {
                especialistaListEspecialistaToAttach = em.getReference(especialistaListEspecialistaToAttach.getClass(), especialistaListEspecialistaToAttach.getId());
                attachedEspecialistaList.add(especialistaListEspecialistaToAttach);
            }
            especialidade.setEspecialistaList(attachedEspecialistaList);
            em.persist(especialidade);
            if (cr != null) {
                cr.getEspecialidadeList().add(especialidade);
                cr = em.merge(cr);
            }
            for (Especialista especialistaListEspecialista : especialidade.getEspecialistaList()) {
                Especialidade oldEspecialidadeOfEspecialistaListEspecialista = especialistaListEspecialista.getEspecialidade();
                especialistaListEspecialista.setEspecialidade(especialidade);
                especialistaListEspecialista = em.merge(especialistaListEspecialista);
                if (oldEspecialidadeOfEspecialistaListEspecialista != null) {
                    oldEspecialidadeOfEspecialistaListEspecialista.getEspecialistaList().remove(especialistaListEspecialista);
                    oldEspecialidadeOfEspecialistaListEspecialista = em.merge(oldEspecialidadeOfEspecialistaListEspecialista);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Especialidade especialidade) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Especialidade persistentEspecialidade = em.find(Especialidade.class, especialidade.getId());
            Cr crOld = persistentEspecialidade.getCr();
            Cr crNew = especialidade.getCr();
            List<Especialista> especialistaListOld = persistentEspecialidade.getEspecialistaList();
            List<Especialista> especialistaListNew = especialidade.getEspecialistaList();
            List<String> illegalOrphanMessages = null;
            for (Especialista especialistaListOldEspecialista : especialistaListOld) {
                if (!especialistaListNew.contains(especialistaListOldEspecialista)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Especialista " + especialistaListOldEspecialista + " since its especialidade field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (crNew != null) {
                crNew = em.getReference(crNew.getClass(), crNew.getId());
                especialidade.setCr(crNew);
            }
            List<Especialista> attachedEspecialistaListNew = new ArrayList<Especialista>();
            for (Especialista especialistaListNewEspecialistaToAttach : especialistaListNew) {
                especialistaListNewEspecialistaToAttach = em.getReference(especialistaListNewEspecialistaToAttach.getClass(), especialistaListNewEspecialistaToAttach.getId());
                attachedEspecialistaListNew.add(especialistaListNewEspecialistaToAttach);
            }
            especialistaListNew = attachedEspecialistaListNew;
            especialidade.setEspecialistaList(especialistaListNew);
            especialidade = em.merge(especialidade);
            if (crOld != null && !crOld.equals(crNew)) {
                crOld.getEspecialidadeList().remove(especialidade);
                crOld = em.merge(crOld);
            }
            if (crNew != null && !crNew.equals(crOld)) {
                crNew.getEspecialidadeList().add(especialidade);
                crNew = em.merge(crNew);
            }
            for (Especialista especialistaListNewEspecialista : especialistaListNew) {
                if (!especialistaListOld.contains(especialistaListNewEspecialista)) {
                    Especialidade oldEspecialidadeOfEspecialistaListNewEspecialista = especialistaListNewEspecialista.getEspecialidade();
                    especialistaListNewEspecialista.setEspecialidade(especialidade);
                    especialistaListNewEspecialista = em.merge(especialistaListNewEspecialista);
                    if (oldEspecialidadeOfEspecialistaListNewEspecialista != null && !oldEspecialidadeOfEspecialistaListNewEspecialista.equals(especialidade)) {
                        oldEspecialidadeOfEspecialistaListNewEspecialista.getEspecialistaList().remove(especialistaListNewEspecialista);
                        oldEspecialidadeOfEspecialistaListNewEspecialista = em.merge(oldEspecialidadeOfEspecialistaListNewEspecialista);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = especialidade.getId();
                if (findEspecialidade(id) == null) {
                    throw new NonexistentEntityException("The especialidade with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Especialidade especialidade;
            try {
                especialidade = em.getReference(Especialidade.class, id);
                especialidade.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The especialidade with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Especialista> especialistaListOrphanCheck = especialidade.getEspecialistaList();
            for (Especialista especialistaListOrphanCheckEspecialista : especialistaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Especialidade (" + especialidade + ") cannot be destroyed since the Especialista " + especialistaListOrphanCheckEspecialista + " in its especialistaList field has a non-nullable especialidade field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cr cr = especialidade.getCr();
            if (cr != null) {
                cr.getEspecialidadeList().remove(especialidade);
                cr = em.merge(cr);
            }
            em.remove(especialidade);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Especialidade> findEspecialidadeEntities() {
        return findEspecialidadeEntities(true, -1, -1);
    }

    public List<Especialidade> findEspecialidadeEntities(int maxResults, int firstResult) {
        return findEspecialidadeEntities(false, maxResults, firstResult);
    }

    private List<Especialidade> findEspecialidadeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Especialidade.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Especialidade findEspecialidade(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Especialidade.class, id);
        } finally {
            em.close();
        }
    }

    public int getEspecialidadeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Especialidade> rt = cq.from(Especialidade.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
       public List findAll() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Especialidade.findByStatus").setParameter("status", true).getResultList();
        } finally {
            em.close();
        }
    }
    
}
