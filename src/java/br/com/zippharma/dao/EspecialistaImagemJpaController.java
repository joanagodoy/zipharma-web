/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Especialista;
import br.com.zippharma.domain.EspecialistaImagem;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author Usuario
 */
public class EspecialistaImagemJpaController implements Serializable {

//    public EspecialistaImagemJpaController(EntityManagerFactory emf) {
//        this.emf = emf;
//    }
//    private EntityManagerFactory emf = null;
    
    @PersistenceUnit
    private EntityManagerFactory emf;
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EspecialistaImagem especialistaImagem) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Especialista especialista = especialistaImagem.getEspecialista();
            if (especialista != null) {
                especialista = em.getReference(especialista.getClass(), especialista.getId());
                especialistaImagem.setEspecialista(especialista);
            }
            em.persist(especialistaImagem);
            if (especialista != null) {
                especialista.getEspecialistaImagemList().add(especialistaImagem);
                especialista = em.merge(especialista);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EspecialistaImagem especialistaImagem) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EspecialistaImagem persistentEspecialistaImagem = em.find(EspecialistaImagem.class, especialistaImagem.getId());
            Especialista especialistaOld = persistentEspecialistaImagem.getEspecialista();
            Especialista especialistaNew = especialistaImagem.getEspecialista();
            if (especialistaNew != null) {
                especialistaNew = em.getReference(especialistaNew.getClass(), especialistaNew.getId());
                especialistaImagem.setEspecialista(especialistaNew);
            }
            especialistaImagem = em.merge(especialistaImagem);
            if (especialistaOld != null && !especialistaOld.equals(especialistaNew)) {
                especialistaOld.getEspecialistaImagemList().remove(especialistaImagem);
                especialistaOld = em.merge(especialistaOld);
            }
            if (especialistaNew != null && !especialistaNew.equals(especialistaOld)) {
                especialistaNew.getEspecialistaImagemList().add(especialistaImagem);
                especialistaNew = em.merge(especialistaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = especialistaImagem.getId();
                if (findEspecialistaImagem(id) == null) {
                    throw new NonexistentEntityException("The especialistaImagem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EspecialistaImagem especialistaImagem;
            try {
                especialistaImagem = em.getReference(EspecialistaImagem.class, id);
                especialistaImagem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The especialistaImagem with id " + id + " no longer exists.", enfe);
            }
            Especialista especialista = especialistaImagem.getEspecialista();
            if (especialista != null) {
                especialista.getEspecialistaImagemList().remove(especialistaImagem);
                especialista = em.merge(especialista);
            }
            em.remove(especialistaImagem);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EspecialistaImagem> findEspecialistaImagemEntities() {
        return findEspecialistaImagemEntities(true, -1, -1);
    }

    public List<EspecialistaImagem> findEspecialistaImagemEntities(int maxResults, int firstResult) {
        return findEspecialistaImagemEntities(false, maxResults, firstResult);
    }

    private List<EspecialistaImagem> findEspecialistaImagemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EspecialistaImagem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EspecialistaImagem findEspecialistaImagem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EspecialistaImagem.class, id);
        } finally {
            em.close();
        }
    }

    public int getEspecialistaImagemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EspecialistaImagem> rt = cq.from(EspecialistaImagem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
