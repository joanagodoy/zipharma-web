/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.dao.exceptions.PreexistingEntityException;
import br.com.zippharma.domain.OrcamentoItem;
import br.com.zippharma.domain.OrcamentoItemPK;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class OrcamentoItemJpaController implements Serializable {

   @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OrcamentoItem orcamentoItem) throws PreexistingEntityException, Exception {
        if (orcamentoItem.getOrcamentoItemPK() == null) {
            orcamentoItem.setOrcamentoItemPK(new OrcamentoItemPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(orcamentoItem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOrcamentoItem(orcamentoItem.getOrcamentoItemPK()) != null) {
                throw new PreexistingEntityException("OrcamentoItem " + orcamentoItem + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OrcamentoItem orcamentoItem) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            orcamentoItem = em.merge(orcamentoItem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                OrcamentoItemPK id = orcamentoItem.getOrcamentoItemPK();
                if (findOrcamentoItem(id) == null) {
                    throw new NonexistentEntityException("The orcamentoItem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(OrcamentoItemPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OrcamentoItem orcamentoItem;
            try {
                orcamentoItem = em.getReference(OrcamentoItem.class, id);
                orcamentoItem.getOrcamentoItemPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orcamentoItem with id " + id + " no longer exists.", enfe);
            }
            em.remove(orcamentoItem);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OrcamentoItem> findOrcamentoItemEntities() {
        return findOrcamentoItemEntities(true, -1, -1);
    }
    
    public List<OrcamentoItem> findByOrcamento(Long idOrcamento){
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("OrcamentoItem.findByOrcamento").setParameter("orcamento", idOrcamento).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public List<OrcamentoItem> findOrcamentoItemEntities(int maxResults, int firstResult) {
        return findOrcamentoItemEntities(false, maxResults, firstResult);
    }

    private List<OrcamentoItem> findOrcamentoItemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OrcamentoItem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OrcamentoItem findOrcamentoItem(OrcamentoItemPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OrcamentoItem.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrcamentoItemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OrcamentoItem> rt = cq.from(OrcamentoItem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
