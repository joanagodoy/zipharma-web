/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Chat;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class ChatJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Chat chat) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(chat);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Chat chat) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            chat = em.merge(chat);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = chat.getId();
                if (findChat(id) == null) {
                    throw new NonexistentEntityException("The chat with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Chat chat;
            try {
                chat = em.getReference(Chat.class, id);
                chat.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The chat with id " + id + " no longer exists.", enfe);
            }
            em.remove(chat);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Chat> findChatEntities() {
        return findChatEntities(true, -1, -1);
    }

    public List<Chat> findChatEntities(int maxResults, int firstResult) {
        return findChatEntities(false, maxResults, firstResult);
    }

    private List<Chat> findChatEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Chat.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Boolean jaSolicitouOrcamento(Long cliente, int farmacia){
        EntityManager em = getEntityManager();
        try {
            return em.createNativeQuery("SELECT id FROM chat WHERE pedido = 0 AND mensagem LIKE 'Solicitado;%' AND cliente = ? AND farmacia = ?").setParameter(1, cliente).setParameter(2, farmacia).getResultList().size()>0;
        } finally {
            em.close();
        }
    }
    
    public Chat findChat(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Chat.class, id);
        } finally {
            em.close();
        }
    }

    public int getChatCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Chat> rt = cq.from(Chat.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findConversas(Long cliente, Integer farmacia, boolean up, Long id, int max) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE chat SET lida = true WHERE cliente = ? AND farmacia = ? AND usuario IS NOT NULL").setParameter(1, cliente).setParameter(2, farmacia).executeUpdate();
            em.createNativeQuery("UPDATE notificacao SET notificada = true WHERE tipo = 'Nova Mensagem' AND cliente = ? AND idObjeto = ?").setParameter(1, cliente).setParameter(2, farmacia).executeUpdate();
            em.getTransaction().commit();
            if (up) {
                if (id == null || id == 0) {
                    id = 99999999999999999L;
                }
                return em.createNativeQuery("SELECT * FROM (SELECT * FROM chat WHERE id < ? and farmacia = ? and cliente = ? ORDER BY id DESC LIMIT ?) t ORDER BY id ASC", Chat.class).setParameter(1, id).setParameter(2, farmacia).setParameter(3, cliente).setParameter(4, max).getResultList();
            } else {
                return em.createNativeQuery("SELECT * FROM chat WHERE id > ? and farmacia = ? and cliente = ? ORDER BY id ASC", Chat.class).setParameter(1, id).setParameter(2, farmacia).setParameter(3, cliente).getResultList();
            }
        } finally {
            em.close();
        }
    }

}
