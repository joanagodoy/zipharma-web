/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.EnderecoCliente;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class EnderecoClienteJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EnderecoCliente enderecoCliente) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(enderecoCliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EnderecoCliente enderecoCliente) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            enderecoCliente = em.merge(enderecoCliente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = enderecoCliente.getId();
                if (findEnderecoCliente(id) == null) {
                    throw new NonexistentEntityException("The enderecoCliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EnderecoCliente enderecoCliente;
            try {
                enderecoCliente = em.getReference(EnderecoCliente.class, id);
                enderecoCliente.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The enderecoCliente with id " + id + " no longer exists.", enfe);
            }
            em.remove(enderecoCliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EnderecoCliente> findEnderecoClienteEntities() {
        return findEnderecoClienteEntities(true, -1, -1);
    }

    public List<EnderecoCliente> findEnderecoClienteEntities(int maxResults, int firstResult) {
        return findEnderecoClienteEntities(false, maxResults, firstResult);
    }

    private List<EnderecoCliente> findEnderecoClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EnderecoCliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EnderecoCliente findEnderecoCliente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EnderecoCliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getEnderecoClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EnderecoCliente> rt = cq.from(EnderecoCliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findByCliente(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("EnderecoCliente.findByCliente").setParameter("cliente", id).getResultList();
        } finally {
            em.close();
        }
    }
    
    public void atualizarLocalSessao(){
        
    }

}
