/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Notificacao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 *
 * @author Usuario
 */
@Component
public class NotificacaoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Notificacao notificacao) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(notificacao);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Notificacao notificacao) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            notificacao = em.merge(notificacao);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = notificacao.getId();
                if (findNotificacao(id) == null) {
                    throw new NonexistentEntityException("The notificacao with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Notificacao notificacao;
            try {
                notificacao = em.getReference(Notificacao.class, id);
                notificacao.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The notificacao with id " + id + " no longer exists.", enfe);
            }
            em.remove(notificacao);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Notificacao> findNotificacaoEntities() {
        return findNotificacaoEntities(true, -1, -1);
    }

    public List<Notificacao> findNotificacaoEntities(int maxResults, int firstResult) {
        return findNotificacaoEntities(false, maxResults, firstResult);
    }

    private List<Notificacao> findNotificacaoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Notificacao.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Notificacao findNotificacao(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Notificacao.class, id);
        } finally {
            em.close();
        }
    }

    public int getNotificacaoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Notificacao> rt = cq.from(Notificacao.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findNotificacoes(Cliente c) {
        EntityManager em = getEntityManager();
        try {
            List lista = em.createNativeQuery("SELECT tipo,IF(tipo like 'Pedido%', idObjeto, if(tipo = 'Novo Orçamento',orcamento,0)) , count(*) FROM `notificacao` n LEFT JOIN pedido p ON n.tipo <> 'Nova Mensagem' and n.idObjeto = p.id WHERE n.notificada = 0 and n.cliente = ? and n.farmacia is null and n.data between DATE_SUB(NOW(), INTERVAL 5 DAY) and NOW() GROUP BY tipo,IF(tipo like 'Pedido%', idObjeto, if(tipo = 'Novo Orçamento', orcamento, tipo))").setParameter(1, c.getId()).getResultList();
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM notificacao WHERE cliente = ? and data < NOW() and farmacia is null").setParameter(1, c.getId()).executeUpdate();
            em.getTransaction().commit();
            return lista;
        } finally {
            em.close();
        }
    }

    public List<Object[]> findNotificacoes() {
        EntityManager em = getEntityManager();
        try {
            List lista = em.createNativeQuery("SELECT n.tipo, IF(n.tipo like 'Pedido%', idObjeto, if(n.tipo = 'Novo Orçamento',orcamento,0)), count(*),c.token,group_concat(n.id) FROM `notificacao` n LEFT JOIN pedido p ON n.tipo <> 'Nova Mensagem' and n.idObjeto = p.id LEFT JOIN cliente c on n.cliente = c.id WHERE n.farmacia is null and c.token is not null and notificada = 0  and data between DATE_SUB(NOW(), INTERVAL 5 DAY) and NOW() GROUP BY n.cliente,c.token,tipo,IF(n.tipo like 'Pedido%', idObjeto, if(n.tipo = 'Novo Orçamento', orcamento, n.tipo))").getResultList();
            return lista;
        } finally {
            em.close();
        }
    }

    public void deleteNotificado(List<Integer> ids) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM notificacao WHERE id in (" + StringUtils.arrayToCommaDelimitedString(ids.toArray()) + ")").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

}
