/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Farmacia;
import br.com.zippharma.util.GeoUtils;
import br.com.zippharma.util.Utils;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class FarmaciaJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Farmacia farmacia) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(farmacia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Farmacia farmacia) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            farmacia = em.merge(farmacia);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = farmacia.getId();
                if (findFarmacia(id) == null) {
                    throw new NonexistentEntityException("The farmacia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Farmacia farmacia;
            try {
                farmacia = em.getReference(Farmacia.class, id);
                farmacia.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The farmacia with id " + id + " no longer exists.", enfe);
            }
            em.remove(farmacia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Farmacia> findByAcesso(String acesso) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Farmacia.findByAcesso").setParameter("acesso", acesso).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public List<Farmacia> findFarmaciaEntities() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Farmacia.findAll").getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public List<Farmacia> findFarmaciaEntities(int maxResults, int firstResult) {
        return findFarmaciaEntities(false, maxResults, firstResult);
    }

    public List<Farmacia> findFarmaciasOrcamento() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Farmacia.farmaciasOrcamento").getResultList();
        } finally {
            em.close();
        }
    }

    private List<Farmacia> findFarmaciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Farmacia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Farmacia findFarmacia(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Farmacia.class, id);
        } finally {
            em.close();
        }
    }

    public int getFarmaciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Farmacia> rt = cq.from(Farmacia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findConversas(final Cliente cliente) {
        EntityManager em = getEntityManager();
        try {
            List lista = Utils.arrayToMap(em.createNativeQuery("SELECT f.id,f.nome,f.endereco,f.cidade,f.estado,f.qualidade,f.rapidez,(SELECT COUNT(*) FROM chat WHERE farmacia = f.id and cliente = ? and lida = false and usuario is not null), IF(f.numero is not null,f.numero,''), IF(f.complemento is not null,f.complemento,''), IF(f.bairro is not null,f.bairro,''), IF(f.latitude is not null,f.latitude,0.0), IF(f.longitude is not null,f.longitude,0.0), (SELECT MAX(data) FROM chat WHERE farmacia = f.id and cliente = ?) as data, (SELECT COUNT(id) FROM usuario WHERE farmacia = f.id AND online = 1) as online FROM farmacia f WHERE acesso = 'Full' ORDER BY data DESC").setParameter(1, cliente.getId()).setParameter(2, cliente.getId()).getResultList(), new String[]{"id", "nome", "endereco", "cidade", "estado", "qualidade", "rapidez", "totalMensagem", "numero", "complemento", "bairro", "latitude", "longitude", "data", "online"});
            List novaLista = new ArrayList();
            List novaLista2 = new ArrayList();
            if (cliente.getLatitude() == null) {
                return lista;
            } else {
                for (Object hashFarmacia : lista) {
                    if (((HashMap) hashFarmacia).get("data") != null) {
                        novaLista.add(hashFarmacia);
                    } else{
                        novaLista2.add(hashFarmacia);
                    }
                }
                Collections.sort(novaLista2, new Comparator<HashMap>() {
                    @Override
                    public int compare(HashMap o1, HashMap o2) {
                        if (Double.valueOf(String.valueOf(o1.get("latitude"))) == 0) {
                            return -1;
                        }
                        if (Double.valueOf(String.valueOf(o2.get("latitude"))) == 0) {
                            return 1;
                        }
                        return GeoUtils.geoDistanceInKm(cliente.getLatitude().doubleValue(), cliente.getLongitude().doubleValue(), Double.valueOf(String.valueOf(o1.get("latitude"))), Double.valueOf(String.valueOf(o1.get("longitude")))) > GeoUtils.geoDistanceInKm(cliente.getLatitude().doubleValue(), cliente.getLongitude().doubleValue(), Double.valueOf(String.valueOf(o2.get("latitude"))), Double.valueOf(String.valueOf(o2.get("longitude")))) ? 1 : -1;
                    }
                });
                novaLista.addAll(novaLista2);
                return novaLista;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public boolean findSilencio(Integer farmacia, Long cliente) {
        EntityManager em = getEntityManager();
        try {
            return ((BigInteger) em.createNativeQuery("SELECT EXISTS(SELECT farmacia FROM silencio WHERE cliente = ? and farmacia = ?) FROM dual").setParameter(1, cliente).setParameter(2, farmacia).getSingleResult()).compareTo(BigInteger.ZERO) == 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }

    public void silenciar(Integer farmacia, boolean silencio, Long id) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            if (silencio) {
                em.createNativeQuery("INSERT INTO silencio (cliente,farmacia) VALUES (?,?)").setParameter(1, id).setParameter(2, farmacia).executeUpdate();
            } else {
                em.createNativeQuery("DELETE FROM silencio WHERE cliente = ? and farmacia = ?").setParameter(1, id).setParameter(2, farmacia).executeUpdate();
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

}
