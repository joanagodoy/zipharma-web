/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.dao.exceptions.PreexistingEntityException;
import br.com.zippharma.domain.Carrinho;
import br.com.zippharma.domain.CarrinhoItem;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.util.AcaoCarrinho;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class CarrinhoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Carrinho carrinho) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(carrinho);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (findCarrinho(carrinho.getId()) != null) {
                throw new PreexistingEntityException("Carrinho " + carrinho + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Carrinho carrinho) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            carrinho = em.merge(carrinho);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = carrinho.getId();
                if (findCarrinho(id) == null) {
                    throw new NonexistentEntityException("The carrinho with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Carrinho carrinho;
            try {
                carrinho = em.getReference(Carrinho.class, id);
                carrinho.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The carrinho with id " + id + " no longer exists.", enfe);
            }
            em.remove(carrinho);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Carrinho> findCarrinhoEntities() {
        return findCarrinhoEntities(true, -1, -1);
    }

    public List<Carrinho> findCarrinhoEntities(int maxResults, int firstResult) {
        return findCarrinhoEntities(false, maxResults, firstResult);
    }

    private List<Carrinho> findCarrinhoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Carrinho.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Carrinho findCarrinho(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Carrinho.class, id);
        } finally {
            em.close();
        }
    }

    public int getCarrinhoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Carrinho> rt = cq.from(Carrinho.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public boolean gerenciar(Integer id, Cliente c, AcaoCarrinho acao, Integer quantidade, Long idItem, String descricao) {
        EntityManager em = getEntityManager();
        try {
            StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("GERENCIAR_CARRINHO");
            storedProcedure.registerStoredProcedureParameter("param_cliente", Long.class, ParameterMode.IN);
            storedProcedure.setParameter("param_cliente", c != null ? c.getId() : 0);
            storedProcedure.registerStoredProcedureParameter("param_produto", Integer.class, ParameterMode.IN);
            storedProcedure.setParameter("param_produto", id != null ? id : 0);
            storedProcedure.registerStoredProcedureParameter("acao", Integer.class, ParameterMode.IN);
            storedProcedure.setParameter("acao", acao.id);
            storedProcedure.registerStoredProcedureParameter("quantidade", Integer.class, ParameterMode.IN);
            if (quantidade != null && quantidade < 0) {
                quantidade = 0;
            }
            storedProcedure.setParameter("quantidade", quantidade);
            if (acao == AcaoCarrinho.Diminuir) {
                storedProcedure.setParameter("quantidade", -1);
            } else if (acao == AcaoCarrinho.Adicionar) {
                storedProcedure.setParameter("quantidade", 1);
            } else if (acao == AcaoCarrinho.Remover) {
                storedProcedure.setParameter("quantidade", 0);
            }
            storedProcedure.registerStoredProcedureParameter("param_id", Long.class, ParameterMode.IN);
            storedProcedure.setParameter("param_id", idItem != null ? idItem : 0);
            storedProcedure.registerStoredProcedureParameter("param_descricao", String.class, ParameterMode.IN);
            storedProcedure.setParameter("param_descricao", descricao != null ? descricao : "");
            storedProcedure.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }

    public List<CarrinhoItem> findCarrinhoItens(Cliente c) {
        EntityManager em = getEntityManager();
        try {
            return em.createNativeQuery("SELECT i.* from carrinho c RIGHT JOIN carrinho_item i ON i.carrinho = c.id WHERE cliente = ?", CarrinhoItem.class).setParameter(1, c.getId()).getResultList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public Carrinho findCarrinho(Cliente c) {
        EntityManager em = getEntityManager();
        try {
            Carrinho carrinho = null;
            try {
                carrinho = (Carrinho) em.createNativeQuery("SELECT c.* from carrinho c WHERE cliente = ?", Carrinho.class).setParameter(1, c.getId()).getSingleResult();
            } catch (Exception e) {

            }
            if (carrinho == null) {
                carrinho = new Carrinho();
                carrinho.setData(Calendar.getInstance().getTime());
                carrinho.setCliente(c.getId());
                em.getTransaction().begin();
                em.persist(carrinho);
                em.getTransaction().commit();
            }
            return carrinho;
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    }

    public void deleteByCliente(Long id) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM carrinho_item WHERE carrinho IN (SELECT id FROM carrinho WHERE cliente = ?)").setParameter(1, id).executeUpdate();
            em.createNativeQuery("DELETE FROM carrinho WHERE cliente = ?").setParameter(1, id).executeUpdate();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void setCliente(Long carrinho, Long cliente) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE carrinho SET cliente = ? WHERE id = ?").setParameter(1, cliente).setParameter(2, carrinho).executeUpdate();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

}
