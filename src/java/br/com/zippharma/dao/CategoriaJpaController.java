/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.IllegalOrphanException;
import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Categoria;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.zippharma.domain.Subcategoria;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Usuario
 */
@Component
public class CategoriaJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Categoria categoria) {
        if (categoria.getSubcategoriaList() == null) {
            categoria.setSubcategoriaList(new ArrayList<Subcategoria>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Subcategoria> attachedSubcategoriaList = new ArrayList<Subcategoria>();
            for (Subcategoria subcategoriaListSubcategoriaToAttach : categoria.getSubcategoriaList()) {
                subcategoriaListSubcategoriaToAttach = em.getReference(subcategoriaListSubcategoriaToAttach.getClass(), subcategoriaListSubcategoriaToAttach.getId());
                attachedSubcategoriaList.add(subcategoriaListSubcategoriaToAttach);
            }
            categoria.setSubcategoriaList(attachedSubcategoriaList);
            em.persist(categoria);
            for (Subcategoria subcategoriaListSubcategoria : categoria.getSubcategoriaList()) {
                Categoria oldCategoriaOfSubcategoriaListSubcategoria = subcategoriaListSubcategoria.getCategoria();
                subcategoriaListSubcategoria.setCategoria(categoria);
                subcategoriaListSubcategoria = em.merge(subcategoriaListSubcategoria);
                if (oldCategoriaOfSubcategoriaListSubcategoria != null) {
                    oldCategoriaOfSubcategoriaListSubcategoria.getSubcategoriaList().remove(subcategoriaListSubcategoria);
                    oldCategoriaOfSubcategoriaListSubcategoria = em.merge(oldCategoriaOfSubcategoriaListSubcategoria);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categoria categoria) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria persistentCategoria = em.find(Categoria.class, categoria.getId());
            List<Subcategoria> subcategoriaListOld = persistentCategoria.getSubcategoriaList();
            List<Subcategoria> subcategoriaListNew = categoria.getSubcategoriaList();
            List<String> illegalOrphanMessages = null;
            for (Subcategoria subcategoriaListOldSubcategoria : subcategoriaListOld) {
                if (!subcategoriaListNew.contains(subcategoriaListOldSubcategoria)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Subcategoria " + subcategoriaListOldSubcategoria + " since its categoria field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Subcategoria> attachedSubcategoriaListNew = new ArrayList<Subcategoria>();
            for (Subcategoria subcategoriaListNewSubcategoriaToAttach : subcategoriaListNew) {
                subcategoriaListNewSubcategoriaToAttach = em.getReference(subcategoriaListNewSubcategoriaToAttach.getClass(), subcategoriaListNewSubcategoriaToAttach.getId());
                attachedSubcategoriaListNew.add(subcategoriaListNewSubcategoriaToAttach);
            }
            subcategoriaListNew = attachedSubcategoriaListNew;
            categoria.setSubcategoriaList(subcategoriaListNew);
            categoria = em.merge(categoria);
            for (Subcategoria subcategoriaListNewSubcategoria : subcategoriaListNew) {
                if (!subcategoriaListOld.contains(subcategoriaListNewSubcategoria)) {
                    Categoria oldCategoriaOfSubcategoriaListNewSubcategoria = subcategoriaListNewSubcategoria.getCategoria();
                    subcategoriaListNewSubcategoria.setCategoria(categoria);
                    subcategoriaListNewSubcategoria = em.merge(subcategoriaListNewSubcategoria);
                    if (oldCategoriaOfSubcategoriaListNewSubcategoria != null && !oldCategoriaOfSubcategoriaListNewSubcategoria.equals(categoria)) {
                        oldCategoriaOfSubcategoriaListNewSubcategoria.getSubcategoriaList().remove(subcategoriaListNewSubcategoria);
                        oldCategoriaOfSubcategoriaListNewSubcategoria = em.merge(oldCategoriaOfSubcategoriaListNewSubcategoria);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categoria.getId();
                if (findCategoria(id) == null) {
                    throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categoria categoria;
            try {
                categoria = em.getReference(Categoria.class, id);
                categoria.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoria with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Subcategoria> subcategoriaListOrphanCheck = categoria.getSubcategoriaList();
            for (Subcategoria subcategoriaListOrphanCheckSubcategoria : subcategoriaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Categoria (" + categoria + ") cannot be destroyed since the Subcategoria " + subcategoriaListOrphanCheckSubcategoria + " in its subcategoriaList field has a non-nullable categoria field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(categoria);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categoria> findCategoriaEntities() {
        return findCategoriaEntities(true, -1, -1);
    }

    public List<Categoria> findCategoriaEntities(int maxResults, int firstResult) {
        return findCategoriaEntities(false, maxResults, firstResult);
    }

    private List<Categoria> findCategoriaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categoria.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categoria findCategoria(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categoria.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categoria> rt = cq.from(Categoria.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findAll() {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Categoria.findByStatus").setParameter("status", true).getResultList();
        } finally {
            em.close();
        }
    }

}
