/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Farmacia;
import br.com.zippharma.domain.Notificacao;
import br.com.zippharma.domain.Orcamento;
import br.com.zippharma.domain.OrcamentoItem;
import br.com.zippharma.domain.Pedido;
import br.com.zippharma.domain.PedidoItem;
import br.com.zippharma.domain.PedidoItemPK;
import br.com.zippharma.domain.StatusPedido;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class PedidoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    @Autowired
    NotificacaoJpaController notJpa;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pedido pedido) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(pedido);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pedido pedido) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            pedido = em.merge(pedido);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = pedido.getId();
                if (findPedido(id) == null) {
                    throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido pedido;
            try {
                pedido = em.getReference(Pedido.class, id);
                pedido.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.", enfe);
            }
            em.remove(pedido);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pedido> findPedidoEntities() {
        return findPedidoEntities(true, -1, -1);
    }

    public List<Pedido> findPedidoEntities(int maxResults, int firstResult) {
        return findPedidoEntities(false, maxResults, firstResult);
    }

    private List<Pedido> findPedidoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pedido.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Boolean excedeuTempoEntrega(int tempoMinutos, Long idPedido) {
        EntityManager em = getEntityManager();
        try {
            return ((BigInteger) em.createNativeQuery("SELECT IF(NOW()>DATE_ADD(dataEnvio,INTERVAL ? MINUTE),true,false) FROM pedido WHERE id = ?").setParameter(1, tempoMinutos).setParameter(2, idPedido).getSingleResult()).compareTo(BigInteger.ZERO) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }

    public Pedido findPedido(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pedido.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pedido> rt = cq.from(Pedido.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List findByCliente(Long id) {
        EntityManager em = getEntityManager();
        try {
            List<Pedido> pedidos = em.createNativeQuery("SELECT * FROM pedido WHERE cliente = ? ORDER BY dataEmissao DESC", Pedido.class).setParameter(1, id).getResultList();
            for (Pedido p : pedidos) {
                p.setTotal(p.getFrete());
                for (PedidoItem pI : p.getPedidoItemLista()) {
                    p.setTotal(p.getTotal().add((pI.getPreco().multiply(new BigDecimal(pI.getQuantidade())))));
                }
            }
            return pedidos;
        } finally {
            em.close();
        }
    }

    public List findComprasByCliente(Long cliente, int filtroData) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNativeQuery("SELECT p.* FROM pedido p WHERE p.cliente = ? AND (p.status = 9 OR p.status = 8 OR p.status = 5 OR p.status = 4 OR p.status = 3)" + (filtroData != 0 ? " and p.dataEmissao > DATE_SUB(NOW(), INTERVAL ? DAY)" : "") + " ORDER BY p.id DESC", Pedido.class).setParameter(1, cliente);
            if (filtroData != 0) {
                q.setParameter(2, filtroData);
            }
            List<Pedido> pedidos = q.getResultList();
            for (Pedido p : pedidos) {
                p.setTotal(p.getFrete());
                for (PedidoItem pI : p.getPedidoItemLista()) {
                    p.setTotal(p.getTotal().add((pI.getPreco().multiply(new BigDecimal(pI.getQuantidade())))));
                }
            }
            return pedidos;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

    public Pedido findByIdCliente(long id, Long cliente) {
        EntityManager em = getEntityManager();
        try {
            Pedido p = (Pedido) em.createNativeQuery("SELECT * FROM pedido WHERE id = ? and cliente = ?", Pedido.class).setParameter(1, id).setParameter(2, cliente).getSingleResult();
            p.setTotal(p.getFrete());
            for (PedidoItem pI : p.getPedidoItemLista()) {
                p.setTotal(p.getTotal().add((pI.getPreco().multiply(new BigDecimal(pI.getQuantidade())))));
            }
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }

    public List<Pedido> findByOrcamento(Long id, Long cliente) {
        EntityManager em = getEntityManager();
        try {
            List<Pedido> pedidos = em.createNativeQuery("SELECT * FROM pedido WHERE orcamento = ? and cliente = ? ORDER BY status DESC", Pedido.class).setParameter(1, id).setParameter(2, cliente).getResultList();
            for (Pedido p : pedidos) {
                p.setTotal(p.getFrete());
                for (PedidoItem pI : p.getPedidoItemLista()) {
                    p.setTotal(p.getTotal().add((pI.getPreco().multiply(new BigDecimal(pI.getQuantidade())))));
                }
            }
            return pedidos;
        } finally {
            em.close();
        }
    }

    public void createPedido(Orcamento o, Integer... farmacia) {
        EntityManager em = getEntityManager();
        try {
            for (int f : farmacia) {
                em.getTransaction().begin();
                Pedido p = new Pedido();
                p.setBairro(o.getBairro() == null ? "" : o.getBairro());
                p.setCep(o.getCep());
                p.setCidade(o.getCidade());
                p.setCliente(o.getCliente());
                p.setComplemento(o.getComplemento());
                p.setDataEmissao(o.getDataEmissao());
                p.setEndereco(o.getEndereco());
                p.setFrete(BigDecimal.ZERO);
                p.setOrcamento(o);
                p.setStatus(StatusPedido.AguardandoOrcamento);
                p.setEstado(o.getEstado());
                p.setFormaPagamento(o.getFormaPagamento());
                p.setCartaoCredito(o.getFormaPagamento().getId() == 1 ? o.getCartaoCredito() : null);
                p.setTroco(o.getFormaPagamento().getId() == 2 ? o.getTroco() : null);
                p.setFarmacia(new Farmacia(f));
                p.setObservacao(o.getObservacao());
                em.persist(p);
                int i = 1;
                for (OrcamentoItem oI : o.getOrcamentoItemLista()) {
                    PedidoItem pI = new PedidoItem(new PedidoItemPK(p.getId(), i), BigDecimal.ZERO, oI.getQuantidade(), oI.getProduto(), oI.getDescricao());
                    em.persist(pI);
                    i++;
                }
                em.getTransaction().commit();
                Notificacao notificacao = new Notificacao();
                notificacao.setData(Calendar.getInstance().getTime());
                notificacao.setTipo("Novo Orçamento");
                notificacao.setIdObjeto(null);
                notificacao.setCliente(o.getCliente().getId());
                notificacao.setFarmacia(f);
                notificacao.setNotificada(false);
                notJpa.create(notificacao);
            }
        } finally {
            em.close();
        }
    }

    public void aprovarPedido(Long id, Long cliente) {
        EntityManager em = null;
        try {
            Pedido p = findPedido(id);
            if (p.getCliente().getId().equals(cliente)) {
                em = getEntityManager();
                em.getTransaction().begin();
                //aceita o orcamento
                em.createNativeQuery("UPDATE orcamento SET status = 2 WHERE id = ?").setParameter(1, p.getOrcamento().getId()).executeUpdate();
                //cancela os pedidos restantes
                em.createNativeQuery("UPDATE pedido SET status = 6 WHERE orcamento = ? AND id != ?").setParameter(1, p.getOrcamento().getId()).setParameter(2, id).executeUpdate();
                //aprova o atual
                em.createNativeQuery("UPDATE pedido SET status = 7 WHERE status = 2 and id = ? and cliente = ?").setParameter(1, id).setParameter(2, cliente).executeUpdate();
                em.getTransaction().commit();
                Notificacao notificacao = new Notificacao();
                notificacao.setData(Calendar.getInstance().getTime());
                notificacao.setTipo("Orçamento Aceito");
                notificacao.setIdObjeto(null);
                notificacao.setCliente(cliente);
                notificacao.setFarmacia(p.getFarmacia().getId());
                notificacao.setNotificada(false);
                notJpa.create(notificacao);
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void negarPagamentoPedido(Long id, String fatura) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE pedido SET status = 8, fatura = ? WHERE id = ?").setParameter(1, fatura).setParameter(2, id).executeUpdate();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void concluirPagamentoPedido(Long id, String fatura) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE orcamento SET status = 2 WHERE id = (SELECT orcamento FROM pedido WHERE id = ?)").setParameter(1, id).executeUpdate();
            em.createNativeQuery("UPDATE pedido SET status = 3, fatura = ? WHERE id = ?").setParameter(1, fatura).setParameter(2, id).executeUpdate();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void cancelarPedido(Long id, Long cliente) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            //coloca os pedidos para aguardar aprovacao
            em.createNativeQuery("UPDATE pedido SET status = 2 WHERE status = 6 and orcamento = (SELECT orcamento FROM pedido WHERE id = ? and cliente = ?)").setParameter(1, id).setParameter(2, cliente).executeUpdate();
            //cancela o solicitado
            em.createNativeQuery("UPDATE pedido SET status = 0 WHERE status != 5 and id = ? and cliente = ?").setParameter(1, id).setParameter(2, cliente).executeUpdate();
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
