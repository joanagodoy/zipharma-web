/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.dao;

import br.com.zippharma.dao.exceptions.NonexistentEntityException;
import br.com.zippharma.domain.Orcamento;
import br.com.zippharma.domain.StatusOrcamento;
import br.com.zippharma.domain.StatusPedido;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Component;

/**
 *
 * @author Usuario
 */
@Component
public class OrcamentoJpaController implements Serializable {

    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Orcamento orcamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(orcamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Orcamento orcamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            orcamento = em.merge(orcamento);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = orcamento.getId();
                if (findOrcamento(id) == null) {
                    throw new NonexistentEntityException("The orcamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orcamento orcamento;
            try {
                orcamento = em.getReference(Orcamento.class, id);
                orcamento.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orcamento with id " + id + " no longer exists.", enfe);
            }
            em.remove(orcamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Orcamento> findOrcamentoEntities() {
        return findOrcamentoEntities(true, -1, -1);
    }

    public List<Orcamento> findOrcamentoEntities(int maxResults, int firstResult) {
        return findOrcamentoEntities(false, maxResults, firstResult);
    }

    private List<Orcamento> findOrcamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Orcamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Orcamento findOrcamento(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Orcamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrcamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Orcamento> rt = cq.from(Orcamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Orcamento findByIdCliente(long id, Long cliente) {
        EntityManager em = getEntityManager();
        try {
            Orcamento p = (Orcamento) em.createNativeQuery("SELECT * FROM orcamento WHERE id = ? and cliente = ?", Orcamento.class).setParameter(1, id).setParameter(2, cliente).getSingleResult();
            return p;
        } finally {
            em.close();
        }
    }

    public List<HashMap<String, Object>> findByCliente(Long id, int filtroData) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT o.id, o.dataEmissao, DATE_FORMAT(o.dataEmissao,'%d/%m/%Y - %H:%i'), o.status, SUM(CASE WHEN p.status = 0 OR p.status = 6 THEN 1 ELSE 0 END) as cancelados, SUM(CASE WHEN p.status = 1 THEN 1 ELSE 0 END) as aguardandoOrcamento, SUM(CASE WHEN p.status = 7 THEN 1 ELSE 0 END) as aguardandoAprovacao, SUM(CASE WHEN p.status > 2 AND p.status < 6 THEN 1 ELSE 0 END) as ok FROM orcamento o LEFT JOIN pedido p ON p.orcamento = o.id WHERE o.cliente = ?" + (filtroData != 0 ? " and o.dataEmissao > DATE_SUB(NOW(), INTERVAL ? DAY)" : "") + " and o.status <> 2 GROUP BY o.id DESC";
            List<HashMap<String, Object>> orcamentos = new ArrayList();
            Query resultado = em.createNativeQuery(sql).setParameter(1, id);
            if (filtroData != 0) {
                resultado.setParameter(2, filtroData);
            }
            for (Object[] obj : (List<Object[]>) resultado.getResultList()) {
                HashMap<String, Object> orcamento = new HashMap<String, Object>();
                orcamento.put("id", obj[0]);
                orcamento.put("dataEmissao", obj[2]);
                orcamento.put("status", StatusOrcamento.values()[(Integer) obj[3]]);
                orcamento.put("statusDescricao", StatusOrcamento.values()[(Integer) obj[3]].getDescricao());
                orcamento.put("cancelados", obj[4]);
                orcamento.put("aguardandoOrcamento", obj[5]);
                orcamento.put("aguardandoAprovacao", obj[6]);
                orcamento.put("ok", obj[7]);
                orcamentos.add(orcamento);
            }
            return orcamentos;
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        } finally {
            em.close();
        }
    }

}
