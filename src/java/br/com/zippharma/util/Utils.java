 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.validation.ObjectError;

/**
 *
 * @author Usuario
 */
public class Utils {

    public static boolean isMobile(HttpServletRequest request) {
        return request.getRequestURI().contains("/mobile/");
    }

    public static String getCookie(Cookie[] cookies, String name) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    return cookie.getValue();
                }
            }

        }
        return null;
    }

    public static List arrayToMap(List<Object[]> array, String[] campos) throws StringIndexOutOfBoundsException {
        List result = new ArrayList();
        if (array.size() > 0 && campos.length != array.get(0).length) {
            throw new StringIndexOutOfBoundsException();
        }
        for (Object[] o : array) {
            HashMap<String, Object> map = new HashMap();
            for (int i = 0; i < o.length; i++) {
                map.put(campos[i], o[i]);
            }
            result.add(map);
        }
        return result;
    }
    
    public static void imprimirErros(List<ObjectError> erros){
        for(ObjectError erro: erros){
            System.out.println(erro);
        }
    }
    
}
