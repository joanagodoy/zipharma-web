/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.eclipse.persistence.internal.oxm.conversion.Base64;

/**
 *
 * @author Usuario
 */
public class Post {

    public static String executePost(String targetURL, String parameters, String authentication) throws MalformedURLException, IOException {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            String post = parameters;
            connection = (HttpURLConnection) url.openConnection();
            if (authentication != null && !authentication.isEmpty()) {
                connection.setRequestProperty("Authorization", authentication);
            }
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;");
            connection.setRequestProperty("charset", "ISO-8859-1");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(post.getBytes().length));
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(post);
            wr.flush();
            wr.close();
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            String lines = "";
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
                lines += line;
            }
            rd.close();
            is.close();
            return lines;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            url = null;
            if (connection != null) {
                connection.disconnect();
            }
            connection = null;
        }
        return "";
    }

}
