/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.util;

/**
 *
 * @author Usuario
 */
public enum AcaoCarrinho {

    Adicionar(1), Diminuir(1), Remover(3), Definir(2);

    private AcaoCarrinho(int id) {
        this.id = id;
    }
    
    public int id;

}
