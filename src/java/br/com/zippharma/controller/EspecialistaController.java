/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.CidadeJpaController;
import br.com.zippharma.dao.EspecialidadeJpaController;
import br.com.zippharma.dao.EspecialistaJpaController;
import static br.com.zippharma.util.Utils.isMobile;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping({"/especialista", "/mobile/especialista"})
public class EspecialistaController {

    @Autowired
    EspecialistaJpaController especialistaJpa;

    @RequestMapping(value = {""}, method = {RequestMethod.POST, RequestMethod.GET})
    public String especialista(Integer id, HttpServletRequest request) {
       if (id == null) {
            return "redirect:busca";
        } else {
            request.setAttribute("especialista", especialistaJpa.findEspecialista(id));
            if (isMobile(request)) {
                return "mobile/especialista";
            } else {
                return "especialista";
            }
        }
    }

    @RequestMapping(value = {"/busca"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String busca(String query, Integer especialidade, HttpServletRequest request) {
        request.setAttribute("query", query);
        request.setAttribute("especialidade", especialidade);
        if (isMobile(request)) {
            return "mobile/busca_especialista";
        } else {
            return "busca_especialista";
        }
    }

    @RequestMapping(value = {"/busca/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String busca2(HttpServletRequest request) {
        return "redirect:busca";
    }

    @RequestMapping(value = {"/buscarEspecialistas"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String buscarEspecialistas(Integer especialidade, String query, Integer offset, Integer max, HttpServletRequest request) {
        try {
            
//            if (maisVendidos != null) {
//                request.setAttribute("produtos", produtoJpa.findProdutosMaisVendidos(max));
//            } else {
            List especialistas = especialistaJpa.findEspecialistas(offset, max, especialidade, query);
            System.out.println(especialistas);
                request.setAttribute("especialistas", especialistas );
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/mobile/view/listagem_especialista";
    }

}
