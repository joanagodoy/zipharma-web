/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.BairroJpaController;
import br.com.zippharma.dao.CarrinhoJpaController;
import br.com.zippharma.dao.CartaoCreditoJpaController;
import br.com.zippharma.dao.CategoriaJpaController;
import br.com.zippharma.dao.CidadeJpaController;
import br.com.zippharma.dao.ConfiguracaoJpaController;
import br.com.zippharma.dao.EnderecoClienteJpaController;
import br.com.zippharma.dao.FarmaciaJpaController;
import br.com.zippharma.dao.FormaPagamentoJpaController;
import br.com.zippharma.dao.ProdutoJpaController;
import br.com.zippharma.dao.ReceitaJpaController;
import br.com.zippharma.domain.Carrinho;
import br.com.zippharma.domain.CarrinhoItem;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Farmacia;
import br.com.zippharma.domain.Receita;
import br.com.zippharma.util.AcaoCarrinho;
import br.com.zippharma.util.GeoUtils;
import static br.com.zippharma.util.Utils.isMobile;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping({"/carrinho", "/mobile/carrinho"})
public class CarrinhoController {

    @Autowired
    CidadeJpaController cidadeJpa;
    @Autowired
    BairroJpaController bairroJpa;
    @Autowired
    CategoriaJpaController categoriaJpa;
    @Autowired
    ProdutoJpaController produtoJpa;
    @Autowired
    CarrinhoJpaController carrinhoJpa;
    @Autowired
    ConfiguracaoJpaController configuracaoJpa;
    @Autowired
    EnderecoClienteJpaController enderecoJpa;
    @Autowired
    FormaPagamentoJpaController formaPagamentoJpa;
    @Autowired
    CartaoCreditoJpaController cartaoJpa;
    @Autowired
    ReceitaJpaController receitaJpa;
    @Autowired
    FarmaciaJpaController farmaciaJpa;
    @PersistenceUnit
    private EntityManagerFactory emf = null;
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @RequestMapping(value = {""}, method = {RequestMethod.POST, RequestMethod.GET})
    public String carrinho(Integer id, HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        List<CarrinhoItem> itens = carrinhoJpa.findCarrinhoItens(c);
        Carrinho carrinho = carrinhoJpa.findCarrinho(c);
        request.setAttribute("carrinho", itens);
        if (carrinho == null) {
            carrinho = new Carrinho();
            carrinho.setCliente(c.getId());
            carrinho.setData(Calendar.getInstance().getTime());
            carrinhoJpa.create(carrinho);
        }
        request.setAttribute("receitas", receitaJpa.findByCarrinho(carrinho.getId()));

        if (isMobile(request)) {
            return "mobile/carrinho";
        } else {
            return "carrinho";
        }
    }

    @RequestMapping(value = {"quantidadeItens"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Integer qtdItens(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Cliente c = (Cliente) request.getSession().getAttribute("cliente");            
            String qtd = getEntityManager().createNativeQuery("SELECT SUM(ci.quantidade) from carrinho c, carrinho_item ci WHERE c.cliente = "+c.getId()+" AND c.id = ci.carrinho").getSingleResult().toString();
            return Integer.valueOf(qtd);
        } catch (Exception e) {
            //e.printStackTrace();
            return 0;
        }
    }

    @RequestMapping(value = {"adicionar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean adicionar(Integer id, Long idItem, String descricao, HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Cliente c = (Cliente) request.getSession().getAttribute("cliente");
            return carrinhoJpa.gerenciar(id, c, AcaoCarrinho.Adicionar, null, idItem, descricao);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value = {"diminuir"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean diminuir(Integer id, Long idItem, String descricao, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        return carrinhoJpa.gerenciar(id, c, AcaoCarrinho.Diminuir, null, idItem, descricao);
    }

    @RequestMapping(value = {"remover"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean remover(Integer id, Long idItem, String descricao, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        return carrinhoJpa.gerenciar(id, c, AcaoCarrinho.Remover, null, idItem, descricao);
    }

    @RequestMapping(value = {"confirmar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    String confirmar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        List<CarrinhoItem> itens = carrinhoJpa.findCarrinhoItens(c);
        List<Receita> receitas = Collections.EMPTY_LIST;
        if (itens.isEmpty()) {
            Carrinho car = carrinhoJpa.findCarrinho(c);
            receitas = receitaJpa.findByCarrinho(car.getId());
        } else {
            receitas = receitaJpa.findByCarrinho(itens.get(0).getCarrinhoItemPK().getCarrinho());
        }
        String erro = "";
        if (itens.isEmpty() && receitas.isEmpty()) {
            erro = "E";
        }
        for (CarrinhoItem car : itens) {
            if (car.getProduto() != null && car.getProduto().getReceita()) {
                if (receitas.isEmpty()) {
                    erro = "R";
                }
                break;
            }
        }
        return erro;
    }

    @RequestMapping(value = {"finalizar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String finalizar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("cartoes", cartaoJpa.findCartaoCreditoEntities());
        List<Farmacia> farmacias = farmaciaJpa.findFarmaciaEntities();
        if (c.getLatitude() != null) {
            Collections.sort(farmacias, new Comparator<Farmacia>() {
                @Override
                public int compare(Farmacia o1, Farmacia o2) {
                    if (o1.getLatitude() == null) {
                        return -1;
                    }
                    if (o2.getLatitude() == null) {
                        return 1;
                    }
                    return GeoUtils.geoDistanceInKm(c.getLatitude().doubleValue(), c.getLongitude().doubleValue(), o1.getLatitude().doubleValue(), o1.getLongitude().doubleValue()) > GeoUtils.geoDistanceInKm(c.getLatitude().doubleValue(), c.getLongitude().doubleValue(), o2.getLatitude().doubleValue(), o2.getLongitude().doubleValue()) ? 1 : -1;
                }
            });
        }
        request.setAttribute("farmacias", farmacias);
        request.setAttribute("cidades", cidadeJpa.findCidadeEntities());
        request.setAttribute("enderecos", enderecoJpa.findByCliente(c.getId()));
        request.setAttribute("formas", formaPagamentoJpa.findFormaPagamentoEntities());
        request.setAttribute("cartoes", cartaoJpa.findCartaoCreditoEntities());
        return "mobile/finalizar";

    }

}
