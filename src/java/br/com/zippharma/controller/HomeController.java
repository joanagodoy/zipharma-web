/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.BairroJpaController;
import br.com.zippharma.dao.CategoriaJpaController;
import br.com.zippharma.dao.CidadeJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.EspecialidadeJpaController;
import br.com.zippharma.dao.ProdutoJpaController;
import br.com.zippharma.domain.Bairro;
import br.com.zippharma.domain.Cidade;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.util.Mailer;
import static br.com.zippharma.util.Utils.isMobile;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
public class HomeController {

    @Autowired
    CidadeJpaController cidadeJpa;
    @Autowired
    BairroJpaController bairroJpa;
    @Autowired
    EspecialidadeJpaController especialidadesJpa;
    @Autowired
    ProdutoJpaController produtoJpa;
    @Autowired
    ClienteJpaController clienteJpa;
    @Autowired
    CategoriaJpaController categoriaJpa;

    @RequestMapping(value = {"/", "/mobile/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String index(String cidade, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        if (cidade != null && !cidade.isEmpty()) {
            Cookie cookie = new Cookie("localizacao", URLEncoder.encode(cidade, "UTF-8"));
            cookie.setPath("/");
            cookie.setMaxAge(60 * 60 * 24 * 3);
            response.addCookie(cookie);
            request.getSession().setAttribute("localizacao", cidade);
            if (isMobile(request)) {
                return "redirect:/mobile/home/";
            } else {
                return "redirect:/home/";
            }
        } else {
            List<Cidade> cidades = cidadeJpa.findCidadeEntities();
            request.setAttribute("cidades", cidades);
            request.setAttribute("bairros", bairroJpa.findByCidade(cidades.get(0).getCidadePK().getCidade()));
//            if (isMobile(request)) {
                return "mobile/index";
//            } else {
//                return "index";
//            }
        }
    }

    @RequestMapping(value = {"/enviarContato"}, method = RequestMethod.POST)
    public String enviarContato(String nome, String email, String mensagem, String telefone) throws Exception {
        try {
            Mailer.send(nome, "Contato Zip Pharma Web", "Nome: " + nome + "\n<br>E-mail: " + email + "\n<br>Telefone: " + telefone + "\n<br>Mensagem: " + mensagem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/contato";
    }

    @RequestMapping(value = {"/contato"}, method = RequestMethod.GET)
    public String contato() throws Exception {
        return "contato";
    }

    @RequestMapping(value = {"/sobre"}, method = RequestMethod.GET)
    public String sobre() throws Exception {
        return "sobre";
    }

    @RequestMapping(value = {"/mobile/home/site"}, method = RequestMethod.GET)
    public String site() throws Exception {
        return "redirect:/mobile/home/";
    }

    @RequestMapping(value = {"/mobile/home/codigoBarras"}, method = RequestMethod.GET)
    public String codigoBarras() throws Exception {
        return "redirect:/mobile/home/";
    }
    
    @RequestMapping(value = {"/mobile/busca_categoria"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String busca_categoria(HttpServletRequest request) {
        request.setAttribute("categorias", categoriaJpa.findAll());
        return "mobile/busca_categoria";
    }

    @RequestMapping(value = {"/mobile/aceitarTermos"}, method = RequestMethod.POST)
    public @ResponseBody
    boolean aceitarTermos(HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        c.setTermos(Boolean.TRUE);
        clienteJpa.edit(c);
        request.getSession().setAttribute("termos", true);
        return true;
    }

    @RequestMapping(value = {"/mobile/atualizarDados"}, method = RequestMethod.POST)
    public @ResponseBody
    boolean atualizarDados(Double latitude, Double longitude, String token, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        c.setLatitude(new BigDecimal(latitude));
        c.setLongitude(new BigDecimal(longitude));
        c.setToken(token);
        clienteJpa.edit(c);
        return true;
    }

    @RequestMapping(value = {"", "/mobile"}, method = RequestMethod.GET)
    public String index2(HttpServletRequest request) {
        if (isMobile(request)) {
            return "redirect:mobile/";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = {"/logout", "/mobile/logout"}, method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().invalidate();
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                cookie.setValue("");
                cookie.setMaxAge(0);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
        if (isMobile(request)) {
            return "redirect:/mobile/";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = {"/buscarBairros", "/mobile/buscarBairros"}, method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List<Bairro> buscarBairros(String cidade) {
        return bairroJpa.findByCidade(cidade);
    }

    @RequestMapping(value = {"home/", "/mobile/home/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String home(HttpServletRequest request) {
        request.setAttribute("categorias", categoriaJpa.findAll());
        request.setAttribute("especialidades", especialidadesJpa.findAll());
        if (request.getParameter("open") != null) {
            request.setAttribute("open", request.getParameter("open"));
        }
        if (isMobile(request)) {
            return "mobile/home";
        } else {
            return "home";
        }
    }

    @RequestMapping(value = {"/home", "/mobile/home"}, method = RequestMethod.GET)
    public String home2(HttpServletRequest request) {
        if (isMobile(request)) {
            return "redirect:/mobile/home/";
        } else {
            return "redirect:home/";
        }
    }

    @RequestMapping(value = {"mobile/panel.left"}, method = RequestMethod.GET)
    public String panel(HttpServletRequest request) {
        return "mobile/panel.left";
    }

}
