/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;
import br.com.zippharma.dao.ChatJpaController;
import br.com.zippharma.dao.FarmaciaJpaController;
import br.com.zippharma.domain.Farmacia;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping
public class FarmaciaController {

    @Autowired
    FarmaciaJpaController farmaciaJpa;
    @Autowired
    ChatJpaController chatJpa;

    @RequestMapping(value = {"/mobile/farmacia"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String farmacia(Integer id, HttpServletRequest request) {
        request.setAttribute("farmacia", farmaciaJpa.findFarmacia(id));
        return "mobile/farmacia";
    }

    @RequestMapping(value = {"/mobile/ligar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String ligar(HttpServletRequest request) throws Exception {
        List<Farmacia> farmacias = farmaciaJpa.findByAcesso("Full");
        request.setAttribute("farmacias", farmacias);
        return "mobile/ligar";
    }
    

}
