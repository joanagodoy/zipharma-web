/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.BairroJpaController;
import br.com.zippharma.dao.CarrinhoJpaController;
import br.com.zippharma.dao.CartaoCreditoJpaController;
import br.com.zippharma.dao.CategoriaJpaController;
import br.com.zippharma.dao.CidadeJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.ConfiguracaoJpaController;
import br.com.zippharma.dao.EnderecoClienteJpaController;
import br.com.zippharma.dao.FormaPagamentoJpaController;
import br.com.zippharma.dao.ProdutoJpaController;
import br.com.zippharma.dao.ReceitaJpaController;
import br.com.zippharma.domain.Carrinho;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Receita;
import br.com.zippharma.util.ImageUtil;
import static br.com.zippharma.util.Utils.isMobile;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.sanselan.ImageFormat;
import org.apache.sanselan.Sanselan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping({"/receita", "/mobile/receita"})
public class ReceitaController {

    @Autowired
    CidadeJpaController cidadeJpa;
    @Autowired
    BairroJpaController bairroJpa;
    @Autowired
    CategoriaJpaController categoriaJpa;
    @Autowired
    ProdutoJpaController produtoJpa;
    @Autowired
    CarrinhoJpaController carrinhoJpa;
    @Autowired
    ConfiguracaoJpaController configuracaoJpa;
    @Autowired
    EnderecoClienteJpaController enderecoJpa;
    @Autowired
    FormaPagamentoJpaController formaPagamentoJpa;
    @Autowired
    CartaoCreditoJpaController cartaoJpa;
    @Autowired
    ReceitaJpaController receitaJpa;
    @Autowired
    ClienteJpaController clienteJpa;

    @RequestMapping(value = {"adicionarReceita"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String receita(Integer id, HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        if (isMobile(request)) {
            return "mobile/adicionarReceita";
        } else {
            return "adicionarReceita";
        }
    }

    @RequestMapping(value = {"remover"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean remover(Long id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        receitaJpa.destroy(id);
        return true;
    }
    
    @RequestMapping(value = {"alterar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean alterar(Long id, String medico, String crm, HttpServletRequest request) throws Exception {
       System.out.println("medico" + "" +  medico + "" + crm + "" + id);
        receitaJpa.alterarReceita(id, medico, crm);
        return true;
    }

    @RequestMapping(value = {"adicionarRMV"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String adicionarRMV(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "redirect:/mobile/home/";
    }

    @RequestMapping(value = {"adicionar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean adicionar(String medico, String crm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        if (c == null) {
            c = clienteJpa.findBySalt(request.getParameter("id"), request.getParameter("alt"));
            medico = "";
            crm = "";
        }

        String pathImg = new File( System.getProperty( "catalina.base" ) ).getAbsolutePath() + File.separatorChar + "webapps" + File.separatorChar + "Zippharma" + File.separatorChar + "files" + File.separatorChar + "receitas" + File.separatorChar;
//        String pathImg = "C:\\xampp\\htdocs\\receitas\\";
        Carrinho carrinho = carrinhoJpa.findCarrinho(c);
        try {
            Collection<Part> parts = request.getParts();
            for (Part imagem : parts) {
                if (imagem.getContentType() != null && imagem.getContentType().startsWith("image")) {
                    BufferedImage imBuff = ImageIO.read(imagem.getInputStream());
                    imBuff = ImageUtil.transformImage(imBuff, ImageUtil.getExifTransformation(ImageUtil.readImageInformation(imagem.getInputStream())));
                    int type = imBuff.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : imBuff.getType();
                    String fileName = UUID.randomUUID().toString() + ".jpg";
                    File file = new File(pathImg + fileName);                    
                    Sanselan.writeImage(imBuff, file, ImageFormat.IMAGE_FORMAT_PNG, new HashMap());
                    String fileNameMiniatura = "min_" + fileName;
                    File fileMiniature = new File(pathImg + fileNameMiniatura);
                    BufferedImage imBuffMiniature;
                    int largura;
                    int altura;
                    if (imBuff.getHeight() > imBuff.getWidth()) {
                        largura = 120 * imBuff.getWidth() / imBuff.getHeight();
                        altura = 120;
                    } else {
                        altura = 120 * imBuff.getHeight() / imBuff.getWidth();
                        largura = 120;
                    }
                    imBuffMiniature = resizeImage(imBuff, type, largura, altura);
                    Sanselan.writeImage(imBuffMiniature, fileMiniature, ImageFormat.IMAGE_FORMAT_PNG, new HashMap());
                    Receita r = new Receita();
                    r.setMedico(medico != null ? medico : null);
                    r.setCrm(crm != null ? crm : null);
                    r.setImagem(fileName);
                    r.setObservacao("");
                    r.setCarrinho(carrinho.getId());
                    receitaJpa.create(r);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

}
