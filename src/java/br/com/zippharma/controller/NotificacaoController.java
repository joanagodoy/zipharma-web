/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.ChatJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.FarmaciaJpaController;
import br.com.zippharma.dao.NotificacaoJpaController;
import br.com.zippharma.dao.PedidoJpaController;
import br.com.zippharma.domain.Cliente;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping
public class NotificacaoController {

    @Autowired
    FarmaciaJpaController farmaciaJpa;
    @Autowired
    ChatJpaController chatJpa;
    @Autowired
    ClienteJpaController clienteJpa;
    @Autowired
    PedidoJpaController pedidoJpa;
    @Autowired
    NotificacaoJpaController notJpa;

    @RequestMapping(value = {"/mobile/buscarNotificacoes"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    List buscarNotificacoes(String id, HttpServletRequest request) {
        List list = Collections.EMPTY_LIST;
        Cliente c = (id != null ? clienteJpa.findBySalt(id.split("\\|")[0], id.split("\\|")[1]) : null);
        if (c != null) {
            list = notJpa.findNotificacoes(c);
        }
        HttpSession ss = request.getSession(false);
        if (ss != null) {
            ss.invalidate();
        }
        return list;
    }
    
    @RequestMapping(value = {"/mobile/enviarNotificacoes"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    List buscarNotificacoesIos(HttpServletRequest request) {
        List<Object[]> list = notJpa.findNotificacoes();
        List<Integer> ids = new ArrayList();
        
        //exclui notificações enviadas
        for (Object[] not : list) {
            for (String s : not[4].toString().split(",")) {
                ids.add(Integer.parseInt(s));
            }
        }
        if (!ids.isEmpty()) {
            notJpa.deleteNotificado(ids);
        }

        HttpSession ss = request.getSession(false);
        if (ss != null) {
            ss.invalidate();
        }
        return list;
    }
}
