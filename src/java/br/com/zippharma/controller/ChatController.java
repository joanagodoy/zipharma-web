/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.CarrinhoJpaController;
import br.com.zippharma.dao.ChatJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.EnderecoClienteJpaController;
import br.com.zippharma.dao.FarmaciaJpaController;
import br.com.zippharma.dao.NotificacaoJpaController;
import br.com.zippharma.dao.PedidoJpaController;
import br.com.zippharma.domain.CarrinhoItem;
import br.com.zippharma.domain.Chat;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Notificacao;
import br.com.zippharma.domain.Pedido;
import br.com.zippharma.domain.StatusPedido;
import br.com.zippharma.domain.Usuario;
import br.com.zippharma.util.GeoUtils;
import br.com.zippharma.util.ImageUtil;
import br.com.zippharma.util.Utils;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.sanselan.ImageFormat;
import org.apache.sanselan.Sanselan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping
public class ChatController {

    @Autowired
    FarmaciaJpaController farmaciaJpa;
    @Autowired
    ChatJpaController chatJpa;
    @Autowired
    ClienteJpaController clienteJpa;
    @Autowired
    PedidoJpaController pedidoJpa;
    @Autowired
    NotificacaoJpaController notJpa;
    @Autowired
    EnderecoClienteJpaController endJpa;

    @RequestMapping(value = {"/mobile/chat"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String chat(Integer id, HttpServletRequest request) {
        final Cliente cliente = (Cliente) request.getSession().getAttribute("cliente");
        List<HashMap> chat = farmaciaJpa.findConversas(cliente);
        request.setAttribute("farmacias", chat);
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("farmacias", farmaciaJpa.findConversas(c));
        return "mobile/chat";
    }

    @RequestMapping(value = {"/mobile/conversa"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String conversa(Integer id, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("enderecos", endJpa.findByCliente(c.getId()));
        request.setAttribute("farmacia", farmaciaJpa.findFarmacia(id));
        request.setAttribute("silenciado", farmaciaJpa.findSilencio(id, c.getId()));
        return "mobile/conversa";
    }

    @RequestMapping(value = {"/mobile/silenciar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean silenciar(Integer farmacia, boolean silencio, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        farmaciaJpa.silenciar(farmacia, silencio, c.getId());
        return true;
    }

    @RequestMapping(value = {"/mobile/buscarPedido"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String buscarPedido(Long idPedido, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("pedido", pedidoJpa.findByIdCliente(idPedido, c.getId()));
        return "mobile/view/pedido_dialog";
    }

//    @RequestMapping(value = {"/mobile/alterarStatusPedido"}, method = {RequestMethod.POST, RequestMethod.GET})
//    public @ResponseBody
//    Boolean alterarStatusPedido(Long idPedido, Integer status, HttpServletRequest request) {
//        try {
//            Cliente c = (Cliente) request.getSession().getAttribute("cliente");
//            Pedido pedido = pedidoJpa.findByIdCliente(idPedido, c.getId());
//            if (pedido.getStatus().equals(StatusPedido.AguardandoAprovação)) {
//                pedido.setStatus(StatusPedido.values()[status]);
//                Chat chat = new Chat();
//                chat.setCliente(c.getId());
//                chat.setData(Calendar.getInstance().getTime());
//                chat.setFarmacia(pedido.getFarmacia().getId());
//                chat.setLida(false);
//                chat.setPedido(pedido.getId());
//                chat.setMensagem(status == 3 ? "Aceito" : "Negado");
//                pedidoJpa.edit(pedido);
//                chatJpa.create(chat);
//                if (status == 3) {
//                    Notificacao notificacao = new Notificacao();
//                    notificacao.setData(Calendar.getInstance().getTime());
//                    notificacao.setTipo("Orçamento Aceito");
//                    notificacao.setFarmacia(pedido.getFarmacia().getId());
//                    notificacao.setCliente(c.getId());
//                    notificacao.setNotificada(false);
//                    notJpa.create(notificacao);
//                }
//                return true;
//            } else {
//                return false;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
    @RequestMapping(value = {"/mobile/solicitarOrcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean solicitarOrcamento(int farmacia, int endereco, HttpServletRequest request) {
        try {
            Cliente c = (Cliente) request.getSession().getAttribute("cliente");
            if (!chatJpa.jaSolicitouOrcamento(c.getId(), farmacia)) {
                Chat chat = new Chat();
                chat.setCliente(c.getId());
                chat.setData(Calendar.getInstance().getTime());
                chat.setFarmacia(farmacia);
                chat.setLida(false);
                chat.setPedido(Long.valueOf(0));
                chat.setMensagem("Solicitado;" + endereco);
                chatJpa.create(chat);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @RequestMapping(value = {"/mobile/buscarConversas"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String buscarConversas(HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("farmacias", farmaciaJpa.findConversas(c));
        return "mobile/view/listagem_chat";
    }

    @RequestMapping(value = {"/mobile/buscarConversa"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String buscarConversa(Integer farmacia, boolean up, Long id, int max, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("conversa", chatJpa.findConversas(c.getId(), farmacia, up, id, max));
        return "mobile/view/listagem_conversa";
    }

    @RequestMapping(value = {"/mobile/enviarMensagem"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean enviarMensagem(Integer farmacia, String mensagem, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Chat chat = new Chat(c.getId(), farmacia, false, mensagem.replace("<", "&lt;").replace(">", "&gt;"));
        chat.setData(Calendar.getInstance().getTime());
        chatJpa.create(chat);
        try {
            Notificacao notificacao = new Notificacao();
            notificacao.setData(Calendar.getInstance().getTime());
            notificacao.setTipo("Nova Mensagem");
            notificacao.setFarmacia(farmacia);
            notificacao.setCliente(c.getId());
            notificacao.setNotificada(false);
            notJpa.create(notificacao);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @RequestMapping(value = {"mobile/chat/adicionarImagemRMV"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String adicionarRMV() throws Exception {
        return "redirect:/mobile/home/";
    }

    @RequestMapping(value = {"mobile/chat/adicionarImagem"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean adicionar(Integer farmacia, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        System.out.println(request.getParameter("id") + " + " + request.getParameter("alt"));
        if (c == null) {
            c = clienteJpa.findBySalt(request.getParameter("id"), request.getParameter("alt"));
        }
        Chat chat = new Chat();
        chat.setCliente(c.getId());
        chat.setData(Calendar.getInstance().getTime());
        chat.setFarmacia(farmacia);
        chat.setLida(false);
        chat.setMensagem(null);
        String pathImg = new File(System.getProperty("catalina.base")).getAbsolutePath() + File.separatorChar + "webapps" + File.separatorChar + "files" + File.separatorChar + "chat" + File.separatorChar;
        Collection<Part> parts = request.getParts();
        for (Part imagem : parts) {
            if (imagem.getContentType() != null && imagem.getContentType().startsWith("image")) {
                BufferedImage imBuff = ImageIO.read(imagem.getInputStream());
                imBuff = ImageUtil.transformImage(imBuff, ImageUtil.getExifTransformation(ImageUtil.readImageInformation(imagem.getInputStream())));
                int type = imBuff.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : imBuff.getType();
                String fileName = UUID.randomUUID().toString() + ".jpg";
                File file = new File(pathImg + fileName);
                Sanselan.writeImage(imBuff, file, ImageFormat.IMAGE_FORMAT_PNG, new HashMap());
                File fileMiniature = new File(pathImg + "min_" + fileName);
                BufferedImage imBuffMiniature;
                int largura;
                int altura;
                if (imBuff.getHeight() > imBuff.getWidth()) {
                    largura = 120 * imBuff.getWidth() / imBuff.getHeight();
                    altura = 120;
                } else {
                    altura = 120 * imBuff.getHeight() / imBuff.getWidth();
                    largura = 120;
                }
                imBuffMiniature = resizeImage(imBuff, type, largura, altura);
                Sanselan.writeImage(imBuffMiniature, fileMiniature, ImageFormat.IMAGE_FORMAT_PNG, new HashMap());
                chat.setImagem(fileName);
                chatJpa.create(chat);
            }
        }

        return true;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

}
