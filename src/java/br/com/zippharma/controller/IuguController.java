/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.ConfiguracaoJpaController;
import br.com.zippharma.dao.NotificacaoJpaController;
import br.com.zippharma.dao.PedidoItemJpaController;
import br.com.zippharma.dao.PedidoJpaController;
import br.com.zippharma.dao.SubcontaIuguJpaController;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Notificacao;
import br.com.zippharma.domain.Pedido;
import br.com.zippharma.domain.PedidoItem;
import br.com.zippharma.domain.SubcontaIugu;
import br.com.zippharma.util.Crypt;
import br.com.zippharma.util.Post;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.taglibs.standard.tag.common.core.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Encoder;

@Controller
@RequestMapping("/iugu")
public class IuguController {

    @Autowired
    PedidoJpaController pedidoJpa;
    @Autowired
    PedidoItemJpaController itemPedidoJpa;
    @Autowired
    ConfiguracaoJpaController configJpa;
    @Autowired
    NotificacaoJpaController notJpa;
    @Autowired
    ClienteJpaController clienteJpa;
    @Autowired
    SubcontaIuguJpaController subContaJpa;

    @RequestMapping(value = {"iniciarSessao"}, method = RequestMethod.POST)
    public @ResponseBody
    String iniciarSessao(HttpServletRequest request) {
        return configJpa.findConfiguracao("IUGU_ACCOUNT_ID").getValor();
    }

    @RequestMapping(value = {"gerarTokenCc"}, method = RequestMethod.POST)
    public @ResponseBody
    String gerarTokenCc(HttpServletRequest request, String cartao, String cod, String nome, String sobrenome, String mes, String ano) {

        String parametros = "account_id=" + configJpa.findConfiguracao("IUGU_ACCOUNT_ID").getValor();
        parametros += "&method=credit_card";
        parametros += "&test=false";
        parametros += "&data[number]=" + cartao;
        parametros += "&data[verification_value]=" + cod;
        parametros += "&data[first_name]=" + Util.URLEncode(nome,"UTF-8");
        parametros += "&data[last_name]=" + Util.URLEncode(sobrenome,"UTF-8");
        parametros += "&data[month]=" + mes;
        parametros += "&data[year]=" + ano;
        String result;
        System.out.println(parametros);
        try {
            result = Post.executePost("https://api.iugu.com/v1/payment_token", parametros, null);
            System.out.println(result);
            JsonParser parser = new JsonParser();
            JsonObject json = (JsonObject) parser.parse(result);
            return json.get("id").toString().replace("\"", "");
        } catch (Exception ex) {
            ex.printStackTrace();
            result = "erroPost";
        }
        return result;

    }

    @RequestMapping(value = {"charge"}, method = RequestMethod.POST)
    public @ResponseBody
    String charge(HttpServletRequest request, String tokenCc, String cartao, String nome, String sobrenome, String mes, String ano, Long idPedido) {

        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido pedido = pedidoJpa.findByIdCliente(idPedido, c.getId());
        if (pedido.getFatura() == null) {
            List<PedidoItem> items = itemPedidoJpa.findByPedido(idPedido);
            String parametros = "token=" + tokenCc;
//            parametros += "&email=" + configJpa.findConfiguracao("IUGU_EMAIL").getValor();
            parametros += "&email=" + (c.getEmail()!=null && !c.getEmail().equals("") ? c.getEmail() : "kayron.nunes@zippharma.com.br");
            if (pedido.getFrete() != null && pedido.getFrete().doubleValue() > 0) {
                parametros += "&items[][description]=Frete";
                parametros += "&items[][quantity]=1";
                parametros += "&items[][price_cents]=" + converterParaCents(pedido.getFrete());
            }
            for (PedidoItem item : items) {
                parametros += "&items[][description]=" + Util.URLEncode((item.getProduto() != null ? item.getProduto().getDescricao().trim() : item.getDescricao().trim()), "UTF-8");
                parametros += "&items[][quantity]=" + item.getQuantidade();
                parametros += "&items[][price_cents]=" + converterParaCents(item.getPreco());
            }
            String result;
            try {
                SubcontaIugu subConta = subContaJpa.findSubcontaIugu(pedido.getFarmacia().getId());
                System.out.println(parametros);
                result = Post.executePost("https://api.iugu.com/v1/charge", parametros, "Basic " + new BASE64Encoder().encode(subConta.getLiveApiToken().getBytes()) + ":");
                System.out.println(result);
                JsonParser parser = new JsonParser();
                JsonObject json = (JsonObject) parser.parse(result);
                String fatura = json.get("invoice_id").toString().replace("\"", "");
                if (json.get("message").toString().equals("\"Autorizado\"")) {
                    pedidoJpa.concluirPagamentoPedido(idPedido, fatura);
                    Notificacao notificacao = new Notificacao();
                    notificacao.setData(Calendar.getInstance().getTime());
                    notificacao.setTipo("Orçamento Aceito");
                    notificacao.setIdObjeto(null);
                    notificacao.setCliente(pedido.getCliente().getId());
                    notificacao.setFarmacia(pedido.getFarmacia().getId());
                    notificacao.setNotificada(false);
                    notJpa.create(notificacao);
                    HashMap<String, Object> ultimoPagamento = new HashMap();
                    ultimoPagamento.put("cartao", cartao);
                    ultimoPagamento.put("nome", nome);
                    ultimoPagamento.put("sobrenome", sobrenome);
                    ultimoPagamento.put("mes", mes);
                    ultimoPagamento.put("ano", ano);
                    clienteJpa.salvarDadosUltimoPagamento(new Gson().toJson(ultimoPagamento), c.getId());
                    c.setUltimoPagamento(new Gson().toJson(ultimoPagamento));
                    request.getSession().setAttribute("cliente", c);
                    return "true;" + fatura + ";Pagamento aprovado!";
                } else {
//                pedidoJpa.negarPagamentoPedido(idPedido, fatura);
                    return "false;naoAutorizada;Pagamento não autorizado!";
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return "false;erroPost";
            }
        } else {
            return "false;jaRealizou;Pagamento já realizado!";
        }

    }

    public static int converterParaCents(BigDecimal usd) {
        BigDecimal rounded = usd.setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal bigDecimalInCents = rounded.multiply(new BigDecimal(100));
        int cents = bigDecimalInCents.intValueExact();
        return cents;
    }

}
