/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.CarrinhoJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.EnderecoClienteJpaController;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.EnderecoCliente;
import br.com.zippharma.util.Crypt;
import static br.com.zippharma.util.Utils.isMobile;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
public class LoginController {

    @Autowired
    ClienteJpaController clienteJpa;
    @Autowired
    CarrinhoJpaController carrinhoJpa;
    @Autowired
    EnderecoClienteJpaController endJpa;
    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @RequestMapping(value = {"/login", "/mobile/login"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request) {
        if (isMobile(request)) {
            return "mobile/login";
        } else {
            return "login";
        }
    }

    @RequestMapping(value = {"/authCookie", "/mobile/authCookie"}, method = RequestMethod.GET)
    public String authCookie(HttpServletRequest request, HttpServletResponse response) {
        String t = (String) request.getSession().getAttribute("clienteTemp");
        Cliente c = (t != null ? clienteJpa.findBySalt(t.split("\\|")[0], t.split("\\|")[1]) : null);
        request.getSession().setAttribute("clienteTemp", null);
        request.getSession().setAttribute("cliente", c);
        if (c != null) {
            setarLocalSessao(c.getId(), request);
            return "redirect:home/";
        } else {
            Cookie cookie = new Cookie("cliente", null);
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            return "redirect:login";
        }
    }

    @RequestMapping(value = {"/cadastrar", "/mobile/cadastrar"}, method = RequestMethod.POST)
    public @ResponseBody
    boolean cadastrar(String nome, String email, String senha, HttpServletRequest request, HttpServletResponse response) {
        try {
            Cliente c = new Cliente();
            c.setNome(nome);
            c.setEmail(email);
            c.setSalt(Crypt.gerarSalt());
            c.setSenha(Crypt.criptografar(senha, c.getSalt()));
            c.setDataCadastro(Calendar.getInstance().getTime());
            c.setStatus(true);
            c.setTipo("F");
            c.setTermos(Boolean.FALSE);
            clienteJpa.create(c);
            request.getSession().setAttribute("cliente", c);
            Cookie cookie = new Cookie("cliente", c.getId() + "|" + c.getSalt());
            cookie.setPath("/");
            cookie.setMaxAge(60 * 60 * 24 * 3);
            response.addCookie(cookie);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @RequestMapping(value = {"/alterarSenha", "/mobile/alterarSenha"}, method = RequestMethod.POST)
    public @ResponseBody
    String alterarSenha(String senhaAtual, String novaSenha, HttpServletRequest request, HttpServletResponse response) {

        try {
            Cliente cliente = clienteJpa.findCliente(((Cliente) request.getSession().getAttribute("cliente")).getId());
            if (cliente.getSenha().equals(Crypt.criptografar(senhaAtual, cliente.getSalt()))) {
                if (!novaSenha.isEmpty()) {
                    cliente.setSenha(Crypt.criptografar(novaSenha, cliente.getSalt()));
                    clienteJpa.edit(cliente);
                    return "ok";
                } else {
                    return "senhaEmBranco";
                }
            } else {
                return "senhaIncorreta";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Erro Desconhecido";
        }

    }

    @RequestMapping(value = {"/mobile/salvarLocalPrincipal"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean salvarLocalPrincipal(@ModelAttribute(value = "endereco") EnderecoCliente endereco, BindingResult result, HttpServletRequest request) throws Exception {

        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.createNativeQuery("UPDATE endereco_cliente SET principal = 0 WHERE cliente = "+endereco.getCliente()).executeUpdate();
            em.getTransaction().commit();
            endereco.setPrincipal(true);
            endJpa.edit(endereco);
            setarLocalSessao(endereco.getCliente(), request);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally{
            em.close();
        }
        
    }

    public void setarLocalSessao(Long idCliente, HttpServletRequest request) {
        try {
            String sql = "SELECT a.* FROM endereco_cliente a WHERE a.cliente = " + idCliente + " ORDER BY principal DESC";
            List<EnderecoCliente> enderecos = (List<EnderecoCliente>) getEntityManager().createNativeQuery(sql, EnderecoCliente.class).getResultList();
            if (enderecos.isEmpty()) {
                request.getSession().setAttribute("no_address", true);
                request.getSession().setAttribute("local_endereco", "");
                request.getSession().setAttribute("local_comp", "");
                request.getSession().setAttribute("local_bairro", "");
                request.getSession().setAttribute("local_cep", "");
                request.getSession().setAttribute("local_cidade", "");
                request.getSession().setAttribute("local_estado", "");
            } else {
                EnderecoCliente endereco = enderecos.get(0);
                request.getSession().setAttribute("no_address", false);
                request.getSession().setAttribute("local_endereco", endereco.getLogradouro() != null && !endereco.getLogradouro().isEmpty() ? endereco.getLogradouro() : "");
                request.getSession().setAttribute("local_comp", endereco.getComplemento() != null && !endereco.getComplemento().isEmpty() ? endereco.getComplemento() : "");
                request.getSession().setAttribute("local_bairro", endereco.getBairro() != null && !endereco.getBairro().isEmpty() ? endereco.getBairro() : "");
                request.getSession().setAttribute("local_cep", endereco.getCep() != null && !endereco.getCep().isEmpty() ? endereco.getCep() : "");
                request.getSession().setAttribute("local_cidade", endereco.getCidade() != null && !endereco.getCidade().isEmpty() ? endereco.getCidade() : "");
                request.getSession().setAttribute("local_estado", endereco.getEstado() != null && !endereco.getEstado().isEmpty() ? endereco.getEstado() : "");
            }
            request.getSession().setAttribute("enderecos", enderecos);
        } catch (Exception e) {

        }
    }

    @RequestMapping(value = {"/efetuarLogin", "/mobile/efetuarLogin"}, method = RequestMethod.POST)
    public @ResponseBody
    boolean efetuarLogin(String facebook, String nome, String email, String senha, String nascimento, String cidade, HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("efetuarLogin");
            if (facebook != null && facebook.length() > 0) {
                System.out.println("autenticação facebook: " + facebook);
                Cliente c = clienteJpa.findClienteByFacebook(facebook);
                if (c == null) {
                    System.out.println("cadastrar");
                    c = new Cliente();
                    c.setNome(nome);
                    c.setTermos(Boolean.FALSE);
                    c.setEmail(email);
                    c.setSalt(Crypt.gerarSalt());
                    c.setCadastro(facebook);
                    c.setCidade(cidade);
                    if (nascimento != null && !nascimento.isEmpty()) {
                        c.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse(nascimento));
                    }
                    c.setDataCadastro(Calendar.getInstance().getTime());
                    c.setStatus(true);
                    c.setTipo("F");
                    clienteJpa.create(c);
                }
                request.getSession().setAttribute("cliente", c);
                setarLocalSessao(c.getId(), request);
                request.getSession().setAttribute("termos", c.isTermos());
                Cookie cookie = new Cookie("cliente", c.getId() + "|" + c.getSalt());
                cookie.setPath("/");
                cookie.setMaxAge(60 * 60 * 24 * 3);
                response.addCookie(cookie);
                return true;

            } else {
                Cliente c = clienteJpa.findClienteByEmail(email);
                if (c == null) {
                    return false;
                }
                if (c.getSenha().equals(Crypt.criptografar(senha, c.getSalt()))) {
                    request.getSession().setAttribute("cliente", c);
                    setarLocalSessao(c.getId(), request);
                    request.getSession().setAttribute("termos", c.isTermos());
                    Cookie cookie = new Cookie("cliente", c.getId() + "|" + c.getSalt());
                    cookie.setPath("/");
                    cookie.setMaxAge(60 * 60 * 24 * 3);
                    response.addCookie(cookie);
                    return true;
                }
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @RequestMapping(value = {"/mobile/conta"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String conta(HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("enderecos", endJpa.findByCliente(c.getId()));
        request.setAttribute("userConta", c);
        return "mobile/conta";
    }

}
