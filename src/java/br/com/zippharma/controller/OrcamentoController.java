/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.*;
import br.com.zippharma.domain.*;
import br.com.zippharma.util.Crypt;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

@Controller
public class OrcamentoController {

    @Autowired
    CidadeJpaController cidadeJpa;
    @Autowired
    BairroJpaController bairroJpa;
    @Autowired
    CategoriaJpaController categoriaJpa;
    @Autowired
    ProdutoJpaController produtoJpa;
    @Autowired
    CarrinhoJpaController carrinhoJpa;
    @Autowired
    ConfiguracaoJpaController configuracaoJpa;
    @Autowired
    EnderecoClienteJpaController enderecoJpa;
    @Autowired
    FormaPagamentoJpaController formaPagamentoJpa;
    @Autowired
    CartaoCreditoJpaController cartaoJpa;
    @Autowired
    OrcamentoJpaController orcamentoJpa;
    @Autowired
    OrcamentoItemJpaController orcamentoItemJpa;
    @Autowired
    ReceitaJpaController receitaJpa;
    @Autowired
    FarmaciaJpaController farmaciaJpa;
    @Autowired
    PedidoJpaController pedidoJpa;
    @Autowired
    PedidoItemJpaController pedidoItemJpa;
    @Autowired
    PontuacaoJpaController pontuacaoJpa;
    @Autowired
    NotificacaoJpaController notJpa;

    private Orcamento copiarOrcamento(Orcamento orcamentoAntigo) {
        Orcamento o = new Orcamento();
        o.setStatus(StatusOrcamento.Pendente);
        o.setBairro(orcamentoAntigo.getBairro());
        o.setCartaoCredito(orcamentoAntigo.getCartaoCredito());
        o.setCep(orcamentoAntigo.getCep() != null ? orcamentoAntigo.getCep() : null);
        o.setCidade(orcamentoAntigo.getCidade());
        o.setCliente(orcamentoAntigo.getCliente());
        o.setComplemento(orcamentoAntigo.getComplemento() != null ? orcamentoAntigo.getComplemento() : null);
        o.setDataEmissao(Calendar.getInstance().getTime());
        o.setEndereco(orcamentoAntigo.getEndereco());
        o.setEstado(orcamentoAntigo.getEstado());
        o.setFormaPagamento(orcamentoAntigo.getFormaPagamento());
        o.setTroco(orcamentoAntigo.getTroco() != null ? orcamentoAntigo.getTroco() : null);
        o.setObservacao(orcamentoAntigo.getObservacao() != null ? orcamentoAntigo.getObservacao() : null);
        List<OrcamentoItem> itensOrcamento = orcamentoItemJpa.findByOrcamento(orcamentoAntigo.getId());
        orcamentoJpa.create(o);
        for (OrcamentoItem orcamentoItem : itensOrcamento) {
            OrcamentoItemPK orcamentoItemPk = new OrcamentoItemPK(o.getId(), itensOrcamento.indexOf(orcamentoItem) + 1);
            orcamentoItem.setOrcamentoItemPK(orcamentoItemPk);
            try {
                orcamentoItemJpa.create(orcamentoItem);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        List<Receita> receitas = receitaJpa.findByOrcamento(orcamentoAntigo.getId());
        for (Receita r : receitas) {
            r.setId(Long.valueOf(0));
            r.setCarrinho(null);
            r.setOrcamento(o.getId());
            receitaJpa.create(r);
        }
        return o;
    }

    private Pedido copiarPedido(Pedido pedidoAntigo, Long idOrcamento) {
        Pedido p = new Pedido();
        p.setBairro(pedidoAntigo.getBairro() != null ? pedidoAntigo.getBairro() : null);
        p.setCartaoCodigo(pedidoAntigo.getCartaoCodigo() != null ? pedidoAntigo.getCartaoCodigo() : null);
        p.setCartaoCredito(pedidoAntigo.getCartaoCredito() != null ? pedidoAntigo.getCartaoCredito() : null);
        p.setCartaoNome(pedidoAntigo.getCartaoNome() != null ? pedidoAntigo.getCartaoNome() : null);
        p.setCartaoNumero(pedidoAntigo.getCartaoNumero() != null ? pedidoAntigo.getCartaoNumero() : null);
        p.setCartaoValidade(pedidoAntigo.getCartaoValidade() != null ? pedidoAntigo.getCartaoValidade() : null);
        p.setCep(pedidoAntigo.getCep() != null ? pedidoAntigo.getCep() : null);
        p.setCidade(pedidoAntigo.getCidade() != null ? pedidoAntigo.getCidade() : null);
        p.setCliente(pedidoAntigo.getCliente() != null ? pedidoAntigo.getCliente() : null);
        p.setComplemento(pedidoAntigo.getComplemento() != null ? pedidoAntigo.getComplemento() : null);
        p.setDataEmissao(Calendar.getInstance().getTime());
        p.setEndereco(pedidoAntigo.getEndereco() != null ? pedidoAntigo.getEndereco() : null);
        p.setEstado(pedidoAntigo.getEstado() != null ? pedidoAntigo.getEstado() : null);
        p.setFarmacia(pedidoAntigo.getFarmacia() != null ? pedidoAntigo.getFarmacia() : null);
        p.setFormaPagamento(pedidoAntigo.getFormaPagamento() != null ? pedidoAntigo.getFormaPagamento() : null);
        p.setFrete(pedidoAntigo.getFrete() != null ? pedidoAntigo.getFrete() : null);
        p.setMotivo(pedidoAntigo.getMotivo() != null ? pedidoAntigo.getMotivo() : null);
        p.setParcela(pedidoAntigo.getParcela() != null ? pedidoAntigo.getParcela() : null);
        p.setSalt(Crypt.gerarSalt());
        p.setTotal(pedidoAntigo.getTotal() != null ? pedidoAntigo.getTotal() : null);
        p.setTempoEntrega(pedidoAntigo.getTempoEntrega() != null ? pedidoAntigo.getTempoEntrega() : null);
        p.setTroco(pedidoAntigo.getTroco() != null ? pedidoAntigo.getTroco() : null);
        p.setStatus(StatusPedido.AguardandoOrcamento);
        p.setOrcamento(new Orcamento(idOrcamento));
        pedidoJpa.create(p);
        List<PedidoItem> itens = pedidoItemJpa.findByPedido(pedidoAntigo.getId());
        //Cria os itens para o novo pedido
        for (PedidoItem pedidoItem : itens) {
            PedidoItemPK pedidoItemPk = new PedidoItemPK(p.getId(), itens.indexOf(pedidoItem) + 1);
            pedidoItem.setPedidoItemPK(pedidoItemPk);
            try {
                pedidoItemJpa.create(pedidoItem);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return p;
    }

    @RequestMapping(value = {"/mobile/concluirPedido"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    String concluirPedido(Long idPedido, int status, HttpServletRequest request) throws Exception {

        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido p = pedidoJpa.findPedido(idPedido);
        if (p.getCliente().getId().equals(c.getId()) && (p.getStatus().equals(StatusPedido.Enviado) || p.getStatus().equals(StatusPedido.NaoConcluído))) {
            try {
                if (status == 0) {
                    int entregaMinutos = (Integer.valueOf(p.getTempoEntrega().split(":")[0]) * 60) + Integer.valueOf(p.getTempoEntrega().split(":")[1]);
                    if (pedidoJpa.excedeuTempoEntrega(entregaMinutos, idPedido)) {
                        p.setStatus(StatusPedido.NaoConcluído);
                        pedidoJpa.edit(p);
                        request.getSession().setAttribute("cancelouPedido", true);
                        return "true";
                    } else {
                        return "tempoEntregaNaoExcedido";
                    }
                } else {
                    p.setStatus(StatusPedido.Concluído);
                    p.setDataConclusao(Calendar.getInstance().getTime());
                    pedidoJpa.edit(p);
                    return "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "false";
            }
        } else {
            return "false";
        }

    }

    @RequestMapping(value = {"/mobile/refazerPedido"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    String refazerPedido(Long idPedido, HttpServletRequest request) throws Exception {

        Pedido pedidoAntigo = pedidoJpa.findPedido(idPedido);
        try {
            //Localiza o pedido e o orcamento para recriá-los
            Orcamento orcamentoAntigo = orcamentoJpa.findOrcamento(pedidoAntigo.getOrcamento().getId());
            Orcamento o = copiarOrcamento(orcamentoAntigo);
            Pedido p = copiarPedido(pedidoAntigo, o.getId());
            Notificacao notificacao = new Notificacao();
            notificacao.setData(Calendar.getInstance().getTime());
            notificacao.setTipo("Novo Orçamento");
            notificacao.setIdObjeto(null);
            notificacao.setCliente(o.getCliente().getId());
            notificacao.setFarmacia(p.getFarmacia().getId());
            notificacao.setNotificada(false);
            notJpa.create(notificacao);
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
            return "erro";
        }

    }

    @RequestMapping(value = {"/mobile/cancelarPedido"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean cancelarPedido(Long idPedido, HttpServletRequest request) throws Exception {

        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        try {
            pedidoJpa.cancelarPedido(idPedido, c.getId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @RequestMapping(value = {"/mobile/finalizarAvaliacao"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean finalizarAvaliacao(Long idPedido, int qualidade, int rapidez, String observacao, HttpServletRequest request) throws Exception {

        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido p = pedidoJpa.findPedido(idPedido);
        if (!pontuacaoJpa.jaExisteAvaliacao(idPedido) && c.getId().equals(p.getCliente().getId())) {
            Pontuacao pontuacaoQualidade = new Pontuacao();
            pontuacaoQualidade.setCliente(c.getId());
            pontuacaoQualidade.setData(Calendar.getInstance().getTime());
            pontuacaoQualidade.setFarmacia(p.getFarmacia().getId());
            pontuacaoQualidade.setPedido(idPedido);
            pontuacaoQualidade.setTipo("Qualidade do Atendimento");
            pontuacaoQualidade.setValor(new BigDecimal(qualidade));
            pontuacaoJpa.create(pontuacaoQualidade);
            Pontuacao pontuacaoRapidez = new Pontuacao();
            pontuacaoRapidez.setCliente(c.getId());
            pontuacaoRapidez.setData(Calendar.getInstance().getTime());
            pontuacaoRapidez.setFarmacia(p.getFarmacia().getId());
            pontuacaoRapidez.setPedido(idPedido);
            pontuacaoRapidez.setTipo("Rapidez na Entrega");
            pontuacaoRapidez.setValor(new BigDecimal(rapidez));
            pontuacaoJpa.create(pontuacaoRapidez);
            if (!observacao.equals("")) {
                Pontuacao pontuacaoObs = new Pontuacao();
                pontuacaoObs.setCliente(c.getId());
                pontuacaoObs.setData(Calendar.getInstance().getTime());
                pontuacaoObs.setFarmacia(p.getFarmacia().getId());
                pontuacaoObs.setPedido(idPedido);
                pontuacaoObs.setTipo("Observação");
                pontuacaoObs.setValor(BigDecimal.ZERO);
                pontuacaoObs.setObs(observacao);
                pontuacaoJpa.create(pontuacaoObs);
            }
            return true;
        }
        return false;

    }

    @RequestMapping(value = {"/mobile/pagar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String pagar(Long idPedido, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido p = pedidoJpa.findPedido(idPedido);
        if (p.getCliente().getId().equals(c.getId()) && p.getStatus().equals(StatusPedido.AguardandoPagamento)) {
            request.setAttribute("idPedido", idPedido);
            BigDecimal total = p.getFrete();
            for (PedidoItem item : pedidoItemJpa.findByPedido(idPedido)) {
                total = total.add(item.getPreco().multiply(new BigDecimal(item.getQuantidade())));
            }
            request.setAttribute("total", "R$ " + total.toString().replace(".", ","));
            if (c.getUltimoPagamento() != null) {
                JsonParser parser = new JsonParser();
                JsonObject jsonCard = (JsonObject) parser.parse(c.getUltimoPagamento());
                HashMap<String, Object> ultimoPagamento = new HashMap();
                ultimoPagamento.put("cartao", jsonCard.get("cartao").toString().replace("\"", ""));
                ultimoPagamento.put("nome", jsonCard.get("nome").toString().replace("\"", ""));
                ultimoPagamento.put("sobrenome", jsonCard.get("sobrenome").toString().replace("\"", ""));
                ultimoPagamento.put("mes", jsonCard.get("mes").toString().replace("\"", ""));
                ultimoPagamento.put("ano", jsonCard.get("ano").toString().replace("\"", ""));
                request.setAttribute("cartao", ultimoPagamento);
            }
            return "mobile/pagar";
        } else {
            return "redirect:orcamentos";
        }
    }

    @RequestMapping(value = {"/mobile/avaliar"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String avaliar(Long idPedido, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido p = pedidoJpa.findPedido(idPedido);
        if (p.getCliente().getId().equals(c.getId()) && !pontuacaoJpa.jaExisteAvaliacao(idPedido)) {
            request.setAttribute("idPedido", idPedido);
            return "mobile/avaliar";
        } else {
            return "redirect:orcamentos";
        }
    }

    @RequestMapping(value = {"/mobile/listarOrcamentos"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String listarOrcamentos(Integer id, int filtroData, HttpServletRequest request) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("orcamentos", orcamentoJpa.findByCliente(c.getId(), filtroData));
        return "/mobile/view/listagem_orcamentos";
    }

    @RequestMapping(value = {"/mobile/listarPedidos"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String listarPedidos(Integer id, int filtroData, HttpServletRequest request
    ) {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("pedidos", pedidoJpa.findComprasByCliente(c.getId(), filtroData));
        return "/mobile/view/listagem_pedidos";
    }

    @RequestMapping(value = {"/mobile/orcamentos"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String orcamentos(HttpServletRequest request) throws Exception {
        return "mobile/orcamentos";
    }

    @RequestMapping(value = {"/mobile/orcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String orcamento(Long id, HttpServletRequest request) throws Exception {
        if (id == null) {
            id = (Long) RequestContextUtils.getInputFlashMap(request).get("id");
        }
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        request.setAttribute("orcamento", orcamentoJpa.findByIdCliente(id, c.getId()));
        List<Farmacia> farmacias = farmaciaJpa.findFarmaciasOrcamento();
        List<Pedido> pedidos = pedidoJpa.findByOrcamento(id, c.getId());
        LinkedHashMap<Farmacia, Pedido> result = new LinkedHashMap();
        for (Pedido p : pedidos) {
            result.put(p.getFarmacia(), p);
        }
        for (Farmacia f : farmacias) {
            if (!result.containsKey(f)) {
                result.put(f, null);
            }
        }
        request.setAttribute("pedidos", result);
        return "mobile/orcamento";
    }

    @RequestMapping(value = {"/mobile/pedido"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String pedido(Long id, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Pedido pedido = pedidoJpa.findByIdCliente(id, c.getId());
        request.setAttribute("pedido", pedido);
        request.setAttribute("existeAvaliacao", pontuacaoJpa.jaExisteAvaliacao(id));
        return "mobile/pedido";
    }

    @RequestMapping(value = {"/mobile/aprovarOrcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean aprovarOrcamento(Long id, Integer farmacia, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        pedidoJpa.aprovarPedido(id, c.getId());
        return true;
    }

    @RequestMapping(value = {"/mobile/cancelarOrcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean cancelarSolicitacaoOrcamento(Long id, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        pedidoJpa.cancelarPedido(id, c.getId());
        return true;
    }

    @RequestMapping(value = {"/mobile/enviarOrcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean enviarOrcamento(Long id, Integer farmacia, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Orcamento o = orcamentoJpa.findByIdCliente(id, c.getId());
        pedidoJpa.createPedido(o, farmacia);
        return true;
    }

    @RequestMapping(value = {"/mobile/carrinho/finalizarOrcamento"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    boolean finalizarOrcamento(String farmacias, Integer cartaoCredito, String troco, Integer formaPagamento, String endereco, String descricao, String logradouro, String bairro, String cidade, String complemento, String observacao, HttpServletRequest request, RedirectAttributes redirectAttributes) throws IOException, GeneralSecurityException, Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        Carrinho car = carrinhoJpa.findCarrinho(c);
        List<CarrinhoItem> itens = carrinhoJpa.findCarrinhoItens(c);
        Orcamento orcamento = new Orcamento();
        orcamento.setCliente(c);
        orcamento.setDataEmissao(Calendar.getInstance().getTime());
        orcamento.setFormaPagamento(new FormaPagamento(formaPagamento));
        orcamento.setObservacao(observacao);
        if (formaPagamento == 1) {//Se for cartão de crédito (id = 1)
            orcamento.setCartaoCredito(new CartaoCredito(cartaoCredito));
        } else if (formaPagamento == 2) {
            orcamento.setTroco(new BigDecimal(troco.replace(",", ".")));
        }
        orcamento.setStatus(StatusOrcamento.Pendente);
        if (endereco.equalsIgnoreCase("N")) {
            enderecoJpa.create(new EnderecoCliente(null, descricao, c.getId(), logradouro, bairro, "", cidade.split(";")[0], cidade.split(";")[1], complemento));
            orcamento.setEndereco(logradouro);
            orcamento.setBairro(bairro);
            orcamento.setCidade(cidade.split(";")[0]);
            orcamento.setEstado(cidade.split(";")[1]);
            orcamento.setComplemento(complemento);
            orcamento.setCep("");
        } else {
            EnderecoCliente ec = enderecoJpa.findEnderecoCliente(Integer.parseInt(endereco));
            orcamento.setEndereco(ec.getLogradouro());
            orcamento.setBairro(ec.getBairro());
            orcamento.setCidade(ec.getCidade());
            orcamento.setEstado(ec.getEstado());
            orcamento.setComplemento(ec.getComplemento());
            orcamento.setCep("");
        }
        orcamentoJpa.create(orcamento);
        int i = 0;
        orcamento.setOrcamentoItemLista(new ArrayList());
        for (CarrinhoItem cI : itens) {
            i++;
            OrcamentoItem oI = new OrcamentoItem(new OrcamentoItemPK(orcamento.getId(), i), cI.getProduto(), cI.getDescricao(), BigDecimal.ZERO, cI.getQuantidade());
            orcamentoItemJpa.create(oI);
            orcamento.getOrcamentoItemLista().add(oI);
        }
        receitaJpa.updateReceita(car.getId(), orcamento.getId());
//        redirectAttributes.addFlashAttribute("id", orcamento.getId());
        carrinhoJpa.deleteByCliente(c.getId());
        farmacias = farmacias.replace("checkboxAll,", "");
        if (!farmacias.equals("") && farmacias.split(",").length > 0) {
            Integer[] f = new Integer[farmacias.split(",").length];
            for (int a = 0; a < farmacias.split(",").length; a++) {
                try {
                    f[a] = Integer.valueOf(farmacias.split(",")[a]);
                } catch (Exception e) {

                }
            }
            pedidoJpa.createPedido(orcamento, f);
        }
        return true;

    }

}
