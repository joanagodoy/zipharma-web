/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.controller;

import br.com.zippharma.dao.ChatJpaController;
import br.com.zippharma.dao.CidadeJpaController;
import br.com.zippharma.dao.ClienteJpaController;
import br.com.zippharma.dao.EnderecoClienteJpaController;
import br.com.zippharma.dao.FarmaciaJpaController;
import br.com.zippharma.dao.NotificacaoJpaController;
import br.com.zippharma.dao.PedidoJpaController;
import br.com.zippharma.domain.Chat;
import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.EnderecoCliente;
import br.com.zippharma.domain.Notificacao;
import br.com.zippharma.domain.Pedido;
import br.com.zippharma.domain.StatusPedido;
import br.com.zippharma.domain.Usuario;
import br.com.zippharma.util.Utils;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Usuario
 */
@Controller
@RequestMapping
public class EnderecoController {

    @Autowired
    CidadeJpaController cidadeJpa;
    @Autowired
    EnderecoClienteJpaController enderecoJpa;
    @PersistenceUnit
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @RequestMapping(value = {"/mobile/endereco"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String endereco(int id, HttpServletRequest request) throws Exception {
        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        EnderecoCliente endereco = enderecoJpa.findEnderecoCliente(id);
        try {
            if (id == 0) {
                request.setAttribute("endereco", new EnderecoCliente());
                request.setAttribute("cidades", cidadeJpa.findCidadeEntities());
                return "mobile/endereco";
            } else if (c.getId().equals(endereco.getCliente())) {
                request.setAttribute("endereco", endereco);
                request.setAttribute("cidades", cidadeJpa.findCidadeEntities());
                return "mobile/endereco";
            } else {
                return "mobile/conta";
            }
        } catch (Exception e) {
            return "redirect:/mobile/conta";
        }
    }

    @RequestMapping(value = {"/mobile/salvarEndereco"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean salvar(@ModelAttribute(value = "endereco") EnderecoCliente endereco, BindingResult result, HttpServletRequest request) throws Exception {

        Cliente c = (Cliente) request.getSession().getAttribute("cliente");
        try {
            if (result.hasErrors()) {
                Utils.imprimirErros(result.getAllErrors());
            } else {
            }
            endereco.setEstado(endereco.getCidade().split(";")[1]);
            endereco.setCidade(endereco.getCidade().split(";")[0]);
            if (endereco.getId() == null) {
                endereco.setCliente(c.getId());
                endereco.setPrincipal(false);
                enderecoJpa.create(endereco);
            } else {
                enderecoJpa.edit(endereco);
            }
            setarLocalSessao(c.getId(), request);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setarLocalSessao(Long idCliente, HttpServletRequest request) {
        try {
            String sql = "SELECT a.* FROM endereco_cliente a WHERE a.cliente = " + idCliente + " ORDER BY principal DESC";
            List<EnderecoCliente> enderecos = (List<EnderecoCliente>) getEntityManager().createNativeQuery(sql, EnderecoCliente.class).getResultList();
            if (enderecos.isEmpty()) {
                request.getSession().setAttribute("no_address", true);
                request.getSession().setAttribute("local_endereco", "");
                request.getSession().setAttribute("local_comp", "");
                request.getSession().setAttribute("local_bairro", "");
                request.getSession().setAttribute("local_cep", "");
                request.getSession().setAttribute("local_cidade", "");
                request.getSession().setAttribute("local_estado", "");
            } else {
                EnderecoCliente endereco = enderecos.get(0);
                request.getSession().setAttribute("no_address", false);
                request.getSession().setAttribute("local_endereco", endereco.getLogradouro() != null && !endereco.getLogradouro().isEmpty() ? endereco.getLogradouro() : "");
                request.getSession().setAttribute("local_comp", endereco.getComplemento() != null && !endereco.getComplemento().isEmpty() ? endereco.getComplemento() : "");
                request.getSession().setAttribute("local_bairro", endereco.getBairro() != null && !endereco.getBairro().isEmpty() ? endereco.getBairro() : "");
                request.getSession().setAttribute("local_cep", endereco.getCep() != null && !endereco.getCep().isEmpty() ? endereco.getCep() : "");
                request.getSession().setAttribute("local_cidade", endereco.getCidade() != null && !endereco.getCidade().isEmpty() ? endereco.getCidade() : "");
                request.getSession().setAttribute("local_estado", endereco.getEstado() != null && !endereco.getEstado().isEmpty() ? endereco.getEstado() : "");
            }
            request.getSession().setAttribute("enderecos", enderecos);
        } catch (Exception e) {

        }
    }

    @RequestMapping(value = {"/mobile/removerEndereco"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Boolean remover(int id, HttpServletRequest request) throws Exception {
        try {
            Cliente c = (Cliente) request.getSession().getAttribute("cliente");
            EnderecoCliente endereco = enderecoJpa.findEnderecoCliente(id);
            if (c.getId().equals(endereco.getCliente())) {
                enderecoJpa.destroy(id);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
