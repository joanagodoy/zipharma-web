/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zippharma.filter;

import br.com.zippharma.domain.Cliente;
import br.com.zippharma.domain.Usuario;
import br.com.zippharma.util.Utils;
import java.io.IOException;
import java.util.Collections;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.buf.UDecoder;

/**
 *
 * @author Usuario
 */
public class Authentication implements Filter {

    private static final boolean debug = false;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public Authentication() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        req.setCharacterEncoding("UTF-8");

        if (req.getRequestURI().contains("checkAuthentication")) {
            if (req.getSession().getAttribute("cliente") == null) {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                res.sendError(HttpServletResponse.SC_OK);
            }
            return;
        }
        System.out.println(req.getRequestURI());
        if (!req.getRequestURI().contains("/authCookie")) {
            Cliente c = (Cliente) req.getSession().getAttribute("cliente");
            String value = Utils.getCookie(req.getCookies(), "cliente");
            if (value != null && !value.isEmpty() && c == null) {
                req.getSession().setAttribute("clienteTemp", value);
                res.sendRedirect(request.getServletContext().getContextPath() + "/mobile/authCookie");
                return;
            }
        }

        if (req.getRequestURI().contains("/enviarContato") || req.getRequestURI().contains("/termos") || req.getRequestURI().contains("/contato") || req.getRequestURI().contains("/sobre") || req.getRequestURI().contains("/enviarNotificacoes") || req.getRequestURI().contains("/buscarNotificacoes") || req.getRequestURI().contains("/authCookie") || req.getRequestURI().contains("/chat/adicionarImagem") || req.getRequestURI().contains("/receita/adicionar") || req.getRequestURI().contains("/resources/") || req.getRequestURI().contains("/efetuarLogin") || req.getRequestURI().contains("/cadastrar")) {
            chain.doFilter(request, response);
            return;
        }
        String ua = req.getHeader("User-Agent").toLowerCase();
        Boolean app = req.getHeader("mobile") != null && req.getHeader("mobile").equals("1");
        if (req.getRequestURI().contains("/mobile/") && !app && !(ua.matches("(?i).*((android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|ipad|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino).*") || ua.substring(0, 4).matches("(?i)1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-"))) {
            res.sendRedirect(request.getServletContext().getContextPath());
            return;
        }

        if (req.getSession().getAttribute("cliente") == null) {
            if (req.getRequestURI().contains("/mobile/") && !req.getRequestURI().endsWith("/mobile/")) {
                res.sendRedirect(request.getServletContext().getContextPath() + "/mobile/");
                return;
            } else if (!req.getRequestURI().contains("/mobile/") && !req.getRequestURI().equals(request.getServletContext().getContextPath() + "/")) {
                res.sendRedirect(request.getServletContext().getContextPath() + "/");
                return;
            }

        } else if (req.getRequestURI().endsWith("/mobile/")) {
            res.sendRedirect(request.getServletContext().getContextPath() + "/mobile/home/");
            return;
        } else if (req.getRequestURI().equals(request.getServletContext().getContextPath() + "/")) {
            res.sendRedirect(request.getServletContext().getContextPath() + "/home/");
            return;
        }

//        boolean logado = false;
//        Usuario usuario;
//        try {
//            usuario = (Usuario) req.getSession().getAttribute("usuario");
//        } catch (Exception e) {
//            usuario = null;
//        }
//        if (usuario != null) {
//            logado = true;
//        }
//        if (req.getRequestURI().contains("/apk") || req.getRequestURI().contains("/EXT.") || req.getRequestURI().contains("/Android.") || req.getRequestURI().endsWith("/EfetuarLogin") || req.getRequestURI().contains("/css/") || req.getRequestURI().contains("/js/") || req.getRequestURI().contains("/img/")) {
//            chain.doFilter(request, response);
//            return;
//        }
//        if (!logado && (!req.getRequestURI().endsWith("/login") && !req.getRequestURI().endsWith("/efetuarLogin"))) {
//            res.sendRedirect(request.getServletContext().getContextPath()+"/login");
//            return;
//        }
//        if (req.getRequestURI().endsWith("/login") || req.getRequestURI().endsWith("/efetuarLogin")) {
//            if (logado) {
//                res.sendRedirect(request.getServletContext().getContextPath()+"/home");
//                return;
//            }
//        }
        if (req.getSession().getAttribute("usuario") != null && req.getRequestURI().contains("/cliente")) {
            res.sendRedirect(request.getServletContext().getContextPath() + "/login");
            return;
        }

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
