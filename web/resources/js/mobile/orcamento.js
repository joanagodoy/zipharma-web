$(document).on("pageinit", "#page_orcamento", function() {
    setTimeout('$("#tabInfoOrcamento").click()', 100);
});

$(document).on("pageinit", "#page_orcamentos", function() {
    listarOrcamentos();
});

function listarOrcamentos() {
    $.mobile.loading('show');
    $.post("listarOrcamentos", {
        filtroData: $("#filtroDataOrcamentos").val()
    }).done(function(data) {
        $("#abaOrcamentos").html(data);
        $("#listViewOrcamentos").listview();
        listarPedidos();
    });
}

function listarPedidos() {
    $.post("listarPedidos", {
        filtroData: $("#filtroDataOrcamentos").val()
    }).done(function(data) {
        $("#abaPedidos").html(data);
        $("#listViewPedidos").listview();
        $.mobile.loading('hide');
    });
}

function confirmarRefazerPedido() {
    setTimeout("$(\'#confirmarRefazerPedido\').popup({ positionTo: \'window\' }).popup(\'open\');", 50);
}

function dialogPagamento() {
    setTimeout("$(\'#dialogPagamento\').popup({ positionTo: \'window\' }).popup(\'open\');", 50);
}

function dialogConcluirPedido() {
    setTimeout("$(\'#confirmarConcluirPedido\').popup({ positionTo: \'window\' }).popup(\'open\');", 50);
}

function confirmarCancelarPedido() {
    setTimeout("$(\'#confirmarCancelamentoPedido\').popup({ positionTo: \'window\' }).popup(\'open\');", 50);
}

function concluirPedido(status, id) {
    $.mobile.loading('show');
    $.post("concluirPedido", {idPedido: id, status: status}).done(function(data) {
        $.mobile.loading('hide');
        if (data == 'true') {
            if (status == 1) {
                $.mobile.changePage("avaliar?idPedido=" + id, {
                    type: "post",
                    changeHash: true
                });
                alertarMobile("Pedido concluído com sucesso!");
            } else {
                $.mobile.changePage(window.location.href, {
                    allowSamePageTransition: true,
                    transition: 'none',
                    changeHash: false,
                    reloadPage: true
                });
                cancelouPedido=true;
                alertarMobile("Pedido negativado com sucesso!");
            }
        } else if (data == 'tempoEntregaNaoExcedido') {
            alertarMobile("O tempo de entrega ainda não foi excedido, aguarde!");
        } else {
            alertarMobile("Ocorreu algum erro.");
        }
        $("#confirmarConcluirPedido").popup("close");
    });
}

function refazerPedido(id) {
    $.mobile.loading('show');
    $.post("refazerPedido", {idPedido: id}).done(function(data) {
        $.mobile.loading('hide');
        if (data == 'ok') {
            alertarMobile("Pedido realizado com sucesso!");
        } else {
            alertarMobile("Ocorreu algum erro.");
        }
        $("#confirmarRefazerPedido").popup("close");
    });
}

function solicitarOrcamento() {
    $.mobile.loading('show');
    $.post("enviarOrcamento", {id: $("#solicitarOrcamentoId").val(), farmacia: $("#solicitarOrcamentoFarmacia").val()}).done(function(data) {
        if (data) {
            $.mobile.changePage(window.location.href, {
                allowSamePageTransition: true,
                transition: 'none',
                changeHash: false,
                reloadPage: true
            });
            alertarMobile("Orçamento solicitado");
        }
        $.mobile.loading('hide');
    });
}

function cancelarSolicitacao() {
    cancelarOrcamento($("#cancelarSolicitarOrcamentoId").val());
}

function cancelarOrcamento(id) {
    $.mobile.loading('show');
    $.post("cancelarOrcamento", {id: id}).done(function(data) {
        if (data) {
            $.mobile.changePage(window.location.href, {
                allowSamePageTransition: true,
                transition: 'none',
                changeHash: false,
                reloadPage: true
            });
            alertarMobile("Cancelamento realizado com sucesso");
        }
        $.mobile.loading('hide');
    });
}

function aprovarOrcamento(id) {
    $.mobile.loading('show');
    $.post("aprovarOrcamento", {id: id}).done(function(data) {
        if (data) {
            window.history.back();
            alertarMobile("Orçamento aprovado");
        }
        $.mobile.loading('hide');
    });
}