var MAX_RESULT = 10;

function alertarMobile(msg) {
    new $.nd2Toast({// The 'new' keyword is important, otherwise you would overwrite the current toast instance
        message: msg
    });
}

function logout() {
    $.mobile.loading('show');
    $.post("logout").done(function(data) {
        $.mobile.loading('hide');
        $.mobile.changePage("", {transition: "slide"});
    });
}

function currencyFormatter(valor) {
    return replaceAll("p", ",", replaceAll(",", ".", replaceAll(".", "p", new Number(valor).toFixed(2).replace(/./g, function(c, i, a) {
        return i && c !== "." && !((a.length - i) % 3) ? ',' + c : c;
    }))));
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function post(url, fields) {
    var $form = $('<form>', {
        action: url,
        method: 'post'
    });
    $.each(fields, function(key, val) {
        $form.append('<input>', {
            type: 'hidden',
            name: key,
            value: val
        });
    });
    $form.submit();
}

function logarUrl(url) {
//    alert("Acessando -> " + url);
}

function voltar(url) {
//    alert("chamando voltar('" + url + "')");
}

var lat;
var lon;
function gps(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            //alert(lat+","+lon)
        });
        //setTimeout('podeBuscar=true;',1500);
    } else {
        lat = 0;
        lon = 0;
    }
}



$(function()
{
    
//   gps();
   
});