$(document).on("pageinit", "#page", function () {
//    alert(getMobileOperatingSystem());
    new $.nd2Search({
        placeholder: "Buscar",
        defaultIcon: "globe-alt",
        source: [
        ],
        fn: function (result) {
            
        }
    });
    
    if (aceitouTermos == false) {
        setTimeout("$('#popupTermos').popup({ positionTo: 'window' }).popup('open');verificaPopupTermos()", 500);
    }


    if (enderecos.length == 0) {
        $("#enderecoPrincipal").html("Cadastre um endereço");
        $("#local_estado").val("SC");
    }

    $("#qtdCarrinho").hide();
    quantidadeItensCarrinho();
    $(".divLocal").click(function () {
        popupLocal();
    });

});

function telefonar(){
    //tel:047997148694
   var url = window.location.href;
   if(url.indexOf("?")>=0){
       url = url.substring(0,url.indexOf("?"));
   }
   if(url.indexOf("/home")>=0){
       url = url.replace('/home','/ligar');
   }
   window.location = url+"047997148694";
//   window.location = url+"?tel=047997148694";
    
}

function quantidadeItensCarrinho() {
    $.post("../carrinho/quantidadeItens").done(function (data) {
        if (data == 0) {
            $(".qtdCarrinho").hide();
        } else {
            $(".qtdCarrinho").show();
        }
        $(".qtdCarrinho").html(data);
    });
}

var index_endereco = -1;
function salvarLocal() {
    $.mobile.loading('show');
    //alert(JSON.stringify($("#localForm").serialize()))
    $.post("../salvarLocalPrincipal", $("#localForm").serialize()).done(function (data) {
        $('#popupLocal').popup('close');
        try {
            location.reload();
            //enderecos[index_endereco].cep = $("#local_cep").val();
            //enderecos[index_endereco].endereco = $("#local_endereco").val();
            //enderecos[index_endereco].complemento = $("#local_complemento").val();
            //enderecos[index_endereco].bairro = $("#local_bairro").val();
            //enderecos[index_endereco].cidade = $("#local_cidade").val();
            //$(".end_" + enderecos[index_endereco].id).remove();
            //$("#enderecos").prepend("<option class='end_" + enderecos[index_endereco].id + "' value='" + enderecos[index_endereco].id + "'>" + enderecos[index_endereco].endereco + " (" + enderecos[index_endereco].cidade + " - " + enderecos[index_endereco].estado + ")</option>");
            //$("#enderecos").val(enderecos[index_endereco].id);
            //end_principal = enderecos[index_endereco].id;
            //$("#enderecoPrincipal").html(enderecos[index_endereco].endereco);
        } catch (x) {

        }
        return false;
    });
}

function changeLocal() {
    var id = $("#enderecos").val();
    for (var a = 0; a < enderecos.length; a++) {
        if (enderecos[a].id.toString() == id.toString()) {
            $("#local_id").val(enderecos[a].id);
            $("#local_cep").val(enderecos[a].cep);
            $("#local_endereco").val(enderecos[a].endereco);
            $("#local_complemento").val(enderecos[a].complemento);
            $("#local_bairro").val(enderecos[a].bairro);
            $("#local_cidade").val(enderecos[a].cidade);
            $("#local_estado").val(enderecos[a].estado);
            $("#local_cliente").val(enderecos[a].cliente);
            $("#local_descricao").val(enderecos[a].descricao);
            index_endereco = a;
            break;
        }
    }
}


function popupLocal() {
    //$("#enderecos").val(end_principal);
    changeLocal();
    var display = $('#popupLocal').css('display').toString();
    if (display == 'block') {
        setTimeout("$('#popupLocal').popup({ positionTo: 'window' }).popup('open')", 100);
    }
}

function verificaPopupTermos() {

    var size = new Number($('#popupTermos').css('height').toString().replace("px", ""));
    if (aceitouTermos == false) {
        if (size > 450) {//fechado
            setTimeout("$('#popupTermos').popup({ positionTo: 'window' }).popup('open')", 500);
            setTimeout("verificaPopupTermos()", 100);
        } else {
            setTimeout("verificaPopupTermos()", 100);
        }
    }


}

function aceitarTermos() {
    $.mobile.loading('show');
    $.post("../aceitarTermos").done(function (data) {
        if (data) {
            aceitouTermos = true;
            $('#popupTermos').popup('close');
        }
        $.mobile.loading('hide');
    });
}

function recusarTermos() {
    $.mobile.loading('show');
    window.location = '../sair';
}

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i))
    {
        return 'iOS';

    } else if (userAgent.match(/Android/i))
    {

        return 'Android';
    } else
    {
        return 'unknown';
    }
}

function atualizarDados(latitude, longitude, token) {
    $.post("../atualizarDados", {latitude: latitude, longitude: longitude, token: token}).done(function (data) {
    });
}

function buscarProdutosDestaque() {
    $.post("../produto/buscarProdutos", {destaque: true, offset: $("#principal ul li").length, max: MAX_RESULT}).done(function (data) {
        $("#principal ul").append(data);
        $('#principal ul').listview('refresh');
        setTimeout('$("img.lazy").lazyload({effect: "fadeIn",skip_invisible: false, threshold :400});', 250);
        if (data.trim().length == 0 || $("#principal ul li").length % MAX_RESULT != 0) {
            $("#principal > a").hide();
        } else {
            $("#principal > a").show();
        }
    });
}

function buscarProdutosMaisVendidos() {
    $.post("../produto/buscarProdutos", {maisVendidos: true, offset: 0, max: 15}).done(function (data) {
        $("#tabVendidos ul").append(data);
        $('#tabVendidos ul').listview('refresh');
        setTimeout('$("img.lazy").lazyload({effect: "fadeIn",skip_invisible: false, threshold :400});', 250);
    });
}