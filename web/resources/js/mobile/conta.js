$(document).on("pageinit", "#page_conta", function() {
    setTimeout('$("#tabInfoConta").click()', 100);
});

function validaSenhas() {
    return $("#formAlterarSenha [name='novaSenha']").val() == $("#formAlterarSenha [name='novaSenha2']").val();
}

var alterandoSenha = 0;
function alterarSenha() {

    if (alterandoSenha == 0 && validaSenhas()) {
        alterandoSenha = 1;
        $.mobile.loading('show');
        $.post("alterarSenha",$("#formAlterarSenha").serializeArray()).done(function(data) {
            $.mobile.loading('hide');
            if (data == 'ok') {
                $('#alterarSenha').popup('close');
                alertarMobile("Senha alterada com sucesso!");
            } else if (data == 'senhaIncorreta') {
                alertarMobile("Senha atual incorreta!");
            } else {
                alertarMobile("Erro desconhecido!");
            }
            alterandoSenha = 0;
        });
    } else {
        alertarMobile("Senhas não coincidem!");
    }
    return false;

}