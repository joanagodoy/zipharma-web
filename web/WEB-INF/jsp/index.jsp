<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@page import="java.util.Calendar"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Calendar cal = Calendar.getInstance();
    cal.setTime(cal.getTime());
    int year = cal.get(Calendar.YEAR);
    pageContext.setAttribute("year", year);
%>
<html>
    <head>
        <!--<meta http-equiv="refresh" content="0; url=http://www.zippharma.wix.com/zippharma">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zip Pharma</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="${pageContext.servletContext.contextPath}/resources/img/icon.png" type="image/x-icon" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <style>
            body{font-family: 'Roboto';} 
            /*.footMenu tr td {cursor:pointer;}*/
            .footMenu {   
                /*                border-collapse: separate; float:left;
                                          border-spacing: 25px 0px;*/
                font-weight: 400;
                font-size: 13px;color:white;

            }
            li {display: inline;padding: 10px;cursor:pointer;}
            html,
            body {
                margin:0;
                padding:0;
                height:100%;
            }
            #wrapper {
                min-height:100%;
                position:relative;
            }
            #header {


            }
            #content {
                padding-bottom:100px; /* Height of the footer element */
            }
            #footer {
                margin-left:5%;
                width:90%;
                height:60px;
                position:absolute;
                bottom:0;
                left:0;
            }
        </style>
    </head>
    <body style="background: url('${pageContext.servletContext.contextPath}/resources/img/motoboy.png') no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;">

        <div id="wrapper">

            <div id="header">
            </div><!-- #header -->

            <div id="content">
                <center>
                    <img style="height: 20%;margin-top:0%;" src="${pageContext.servletContext.contextPath}/resources/img/home3.png"/>
                    <div style='position:relative;width:50%;;margin-top:20%;'>
                        <div style="position: absolute;left:0;top:0;margin-top:-50px;;height:100%;width:100%;color:white; z-index:1000;">
                            <br>
                            <span style=" font-size: 5em;font-weight: 600;text-shadow: 2px 1px 1px #949494;">EM</span>
                            <br>
                            <span style="font-size: 2.2em;display: block; font-weight: 600;margin-left: -4px; margin-top: -25px;text-shadow: 2px 1px 1px #949494;">BREVE</span>
                        </div>
                        <img style="width:150px;opacity: 0.4;display: inline;" src="${pageContext.servletContext.contextPath}/resources/img/playstore.png"/>
                        <img style="width:150px;opacity: 0.4;display: inline;" src="${pageContext.servletContext.contextPath}/resources/img/itunes2.png"/>
                    </div>
                </center>
            </div>

            <div id="footer">
                <div class="footMenu">
                    <ul>
                        <li onclick="window.location = '${pageContext.servletContext.contextPath}';">INÍCIO</li>
                        <li onclick="window.location = '${pageContext.servletContext.contextPath}/sobre';">COMO FUNCIONA</li>
                        <li  onclick="window.location = '${pageContext.servletContext.contextPath}/contato';">CONTATO</li>
                        <li>TERMOS</li>
                        <ul style="float:right;list-style-type:none;">
                            <li>© ${year} ZIP PHARMA</li>
                        </ul>
                    </ul>
                </div>
            </div>

        </div>

    </body>
</html>

