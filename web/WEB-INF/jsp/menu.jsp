<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="menuLateral">
    <c:forEach var="categoria" items="${categorias}">
        <c:if test="${fn:length(categoria.subcategoriaList) gt 0}">
            <div data-role="collapsible" data-inset="false" data-collapsed-icon="carat-d" data-expanded-icon="carat-d" data-iconpos="right">
                <h3>${categoria.descricao}</h3>
                <ul data-role="listview" data-icon="false">
                    <c:forEach var="sub" items="${categoria.subcategoriaList}">
                        <c:if test="${sub.status}">
                            <li><a href="${pageContext.servletContext.contextPath}/produto/busca?categoria=${sub.id}" data-ajax='true' data-transition="slide">${sub.descricao}</a></li>
                            </c:if>
                        </c:forEach>
                </ul>
            </div>
        </c:if>
    </c:forEach>
</div>