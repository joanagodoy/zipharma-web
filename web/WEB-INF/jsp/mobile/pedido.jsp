<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    var cancelouPedido = false;
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<c:if test="${cancelouPedido}">
    <script>
        cancelouPedido = true;
    </script>
    <%
        request.getSession().removeAttribute("cancelouPedido");
    %>
</c:if>
<div data-role="page" id="page_pedido">
    <div data-role="popup" id="confirmarRefazerPedido">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Confirmar Pedido</h3>
                <span title="Fechar" onclick="$('#confirmarRefazerPedido').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            Deseja realmente refazer este pedido?
            <br>
            <br>
            <center>
                <button onclick="refazerPedido(${pedido.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Sim</button>
                <button onclick="$('#confirmarRefazerPedido').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não</button>
            </center>
        </div>
    </div>

    <div data-role="popup" id="dadosSuporte">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Houve algum problema?</h3>
                <span title="Fechar" onclick="$('#dadosSuporte').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            <center>    
                Nosso suporte entrará em contato com a farmácia para identificar o problema. Logo lhe informaremos sobre o ocorrido e resolveremos tudo o mais rápido possível. Contato: xx 47 9762-6063 / suporte@zippharma.com.br
                <br>
                <br>
                <button onclick="$('#dadosSuporte').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"> OK</button>
            </center>
        </div>
    </div>

    <div data-role="popup" id="confirmarConcluirPedido">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Concluir Pedido</h3>
                <span title="Fechar" onclick="$('#confirmarConcluirPedido').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            <center>
                O pedido foi concluído com sucesso? <span style="color: red;">(Apenas conclua seu pedido com todos os produtos em mãos)</span>
                <br>
                <br>
                <button onclick="concluirPedido(1,${pedido.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Sim</button>
                <button onclick="concluirPedido(0,${pedido.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não</button>
            </center>
        </div>
    </div>
    <div data-role="popup" id="confirmarCancelamentoPedido">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Confirmar Cancelamento</h3>
                <span title="Fechar" onclick="$('#confirmarCancelamentoPedido').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            Deseja realmente cancelar este pedido?
            <br>
            <br>
            <center>
                <button onclick="cancelarOrcamento(${pedido.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Sim</button>
                <button onclick="$('#confirmarCancelamentoPedido').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não</button>
            </center>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/orcamentos" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Pedido #${pedido.id}</h1>
        <c:if test="${pedido.status == 'Concluído'}">
            <a title="Refazer Pedido" href="#" onclick='confirmarRefazerPedido();' class="ui-btn ui-btn-right wow fadeIn" <c:if test="${existeAvaliacao == false}">style="margin-right:50px;"</c:if>><i class="zmdi zmdi-redo"></i></a>
                <c:if test="${existeAvaliacao == false}">
                <a title="Avaliar" href="${pageContext.servletContext.contextPath}/mobile/avaliar?idPedido=${pedido.id}" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-star"></i></a>
                </c:if>
        </c:if>
            <c:if test="${pedido.status == 'AguardandoOrcamento'}">
            <a title="Cancelar" onclick="confirmarCancelarPedido();" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-block"></i></a>
            </c:if>
            <c:if test="${pedido.status == 'AguardandoPagamento'}">
            <a title="Efetuar Pagamento" href="${pageContext.servletContext.contextPath}/mobile/pagar?idPedido=${pedido.id}" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-money"></i></a>
            </c:if>
            <c:if test="${pedido.status == 'Enviado' || pedido.status == 'NaoConcluído'}">
            <a title="Concluir" onclick="dialogConcluirPedido();" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-check"></i></a>
            </c:if>
        <ul data-role="nd2tabs" data-swipe="true">
            <li data-tab="info" id="tabInfoPedido">Info</li>
            <li data-tab="itens">Itens</li>
        </ul>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <div data-role="nd2tab" data-tab="info">
            <label>Farmácia</label>
            <input type="text" readonly="" value="${pedido.farmacia.nome}">
            <label>Data de Emissão</label>
            <input type="text" readonly="" value="<fmt:formatDate pattern="dd/MM/yyyy - HH:mm" value="${pedido.dataEmissao}"/>">
            <c:if test="${pedido.dataEnvio != null}">
                <label>Data de Envio</label>
                <input type="text" readonly="" value="<fmt:formatDate pattern="dd/MM/yyyy - HH:mm" value="${pedido.dataEnvio}"/>">
            </c:if>
            <c:if test="${pedido.tempoEntrega != null}">
                <label>Tempo de Entrega (H:M)</label>
                <input type="text" readonly="" value="${pedido.tempoEntrega}">
            </c:if>
            <label>Forma de pagamento</label>
            <input type="text" readonly="" value="${pedido.formaPagamento.descricao}">
            <c:if test="${pedido.formaPagamento.id == 1}">
                <label>Cartão de Crédito</label>
                <input type="text" readonly="" value="${pedido.cartaoCredito.descricao}">
            </c:if>
            <c:if test="${pedido.formaPagamento.id==2}">
                <label>Troco</label>
                <input type="text" readonly="" value="${pedido.troco}"> 
            </c:if>
            <label>Frete</label>
            <input type="text" readonly="" value="R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${pedido.frete}" />">
            <label>Total</label>
            <input type="text" readonly="" value="R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${pedido.total}" />">
            <label>Status</label>
            <input type="text" readonly="" value="${pedido.status.descricao}">
        </div>
        <div data-role="nd2tab" data-tab="itens">
            <ul data-role="listview" data-icon="false">
                <c:forEach var="item" items="${pedido.pedidoItemLista}">
                    <li>
                        <a href="#" data-ajax="false" data-transition="slide">
                            <c:if test="${item.produto.produtoImagemList[0]!=null}">
                                <img src="/files/produtos/${item.produto.produtoImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
                            </c:if>
                            <c:if test="${item.produto==null}">
                                <img src="${pageContext.servletContext.contextPath}/resources/img/manual-icon2.png" class="lazy ui-thumbnail ui-thumbnail-circular" />
                            </c:if>
                            <h2 style="color: #33393d">
                                <c:if test="${item.produto!=null}">${item.produto.descricao}</c:if>
                                <c:if test="${item.produto==null}">${item.descricao}</c:if>
                                </h2>
                                <p style="font-weight:400;color: #4CAF50 !important;">
                                ${item.quantidade} x R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.preco}" /> = 
                                <b>R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.quantidade * item.preco}" /></b></p>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
    <div data-role="footer" data-position="fixed">
        <c:if test="${pedido.status == 'AguardandoPagamento'}">
            <button type="button" id="" onclick="$.mobile.changePage('pagar?idPedido=${pedido.id}', {type: 'post', changeHash: true});" data-transition="slide" data-ajax="false" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Efetuar Pagamento</button>
            <!--<a href="#aprovarOrcamento" data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Aprovar orçamento</a>-->
        </c:if>
        <div data-role="popup" id="aprovarOrcamento">
            <div data-role="header">
                <h1 class='nd-title'>Aprovar?</h1>
            </div>
            <div data-role="content">
                <input type="hidden" id="solicitarOrcamentoId"/>
                <input type="hidden" id="solicitarOrcamentoFarmacia"/>
                <p>Tem certeza que deseja aprovar este orçamento em R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${pedido.total}" /></p>
                <a href="#" onclick="aprovarOrcamento(${pedido.id});" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-check'></i> Sim</a>
                <a href="#" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-cancel'></i> Cancelar</a>
            </div>
        </div>
    </div> 
</div>
