<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_endereco">
    <div data-role="header" data-position="fixed">
        <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1><c:if test="${endereco.id == null}">Novo </c:if>Endereço ${endereco.descricao}</h1>
        <c:if test="${endereco.id != null}">
            <a title="Remover" href="#removerEndereco" class="ui-btn ui-btn-right wow fadeIn" data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop"><i class="zmdi zmdi-delete"></i></a>
            </c:if>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <form modelAttribute="endereco" method="post" id="formSalvarEndereco" onsubmit="return salvarEndereco();">
            <!--<script>alert(${endereco.principal})</script>-->
            <input type="hidden" name="principal" value="${endereco.principal}">
            <input type="hidden" name="id" value="${endereco.id}">
            <input type="hidden" name="cliente" value="${endereco.cliente}">
            <label>Descrição</label>
            <input type="text" required name="descricao" value="${endereco.descricao}">
            <label>Endereço</label>
            <input type="text" required name="logradouro" value="${endereco.logradouro}">   
            <label>Complemento</label>
            <input type="text" name="complemento" value="${endereco.complemento}">   
            <label>Bairro</label>
            <input type="text" required name="bairro" value="${endereco.bairro}">   
            <label>CEP</label>
            <input type="text" name="cep" class="cep" value="${endereco.cep}">   
            <label>Cidade</label>
            <select name="cidade">
                <c:forEach var="c" items="${cidades}">
                    <option value="${c.cidadePK.cidade};${c.cidadePK.estado}" <c:if test="${c.cidadePK.cidade == endereco.cidade && c.cidadePK.estado == endereco.estado}">selected</c:if>>${c.cidadePK.cidade}</option>
                </c:forEach>
            </select>
            <button type="submit" id="submitSalvarEndereco" style="display:none !important;"></button>
        </form>
    </div>
    <div data-role="footer" data-position="fixed">
        <button type="button" onclick="$('#submitSalvarEndereco').click();" data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop" style="font-size:1.2em; width: 100%;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Salvar</button>
        <div data-role="popup" id="removerEndereco">
            <div data-role="header" style="position: relative;">
                <center>
                    <h3 name="titulo">Confirmar Remoção</h3>
                    <span title="Fechar" onclick="$('#removerEndereco').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
                </center>
            </div>
            <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
                Deseja realmente remover este endereço?
                <br>
                <br>
                <center>
                    <button onclick="removerEndereco(${endereco.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Sim</button>
                    <button onclick="$('#removerEndereco').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não</button>
                </center>
            </div>
        </div>
    </div> 
</div>
