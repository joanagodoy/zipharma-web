<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_busca">
    <div data-role="header" data-position="fixed">
        <a data-rel="back" id="busca_back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1>Buscar Produto</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <!--Main-->
        <div id="busca">
            <ul data-role="listview" data-icon="false">

            </ul>
            <a href="#" class="ui-btn nd2-btn-icon-block" onclick="buscarProdutos();" style="width:100%;display:none;"><i class="zmdi zmdi-plus"></i> Carregar Mais</a>
        </div>
        <input type="hidden" id="busca_categoria" value="${categoria}">
        <input type="hidden" id="busca_query" value="${query}">

    </div>
</div>