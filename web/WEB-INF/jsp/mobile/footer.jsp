<%@page contentType="text/html" pageEncoding="UTF-8"%>

</div>
<div data-role="footer" data-position="fixed" data-tap-toggle="false" style="background-color: #4caf50 !important; color: white !important;">
    <div class="" style="">
        <div class='row around-xs' style="width:100%;">
            <div class='col-xs-auto' style="display:inline-block; width:32%; text-align: center;">
                <a href='${pageContext.servletContext.contextPath}/mobile/carrinho' class='ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='true'>
                    <!--<i class='zmdi zmdi-shopping-cart zmd-2x'></i><br><strong>Carrinho</strong>-->
                    <i class='zmdi zmdi-shopping-cart zmd-2x'></i><br>Carrinho
                    <div class="qtdCarrinho" id="qtdCarrinho" style="display:none;"></div>
                </a>
            </div>
            <div class='col-xs-auto' style="display:inline-block; width:33%; text-align: center;">
                <a href='#' onclick="window.location = 'lembretes';" data-ajax="true" class=' ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' >
                    <!--<i class='zmdi zmdi-alarm zmd-2x'></i><br><strong>Lembretes</strong>-->
                    <i class='zmdi zmdi-alarm zmd-2x'></i><br>Lembretes
                </a>
            </div>
            <div class='col-xs-auto' style="display:inline-block; width:32%; text-align: center; margin:0; padding:0;">
                <a href='${pageContext.servletContext.contextPath}/mobile/orcamentos' class='ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='true'>
                    <!--<i class='zmdi zmdi-assignment zmd-2x'></i><br><strong>Orçamentos</strong>-->
                    <i class='zmdi zmdi-assignment zmd-2x'></i><br>Orçamentos
                </a>
            </div>
        </div>
    </div>
</div>
</div>




</body>
</html>