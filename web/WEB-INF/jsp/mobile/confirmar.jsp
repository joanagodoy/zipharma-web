<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_confirmar">
    <div data-role="header" data-position="fixed">
        <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Finalizar Pedido</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <c:set var="totalPedido" value="0"/>
        <ul data-role="listview" data-icon="false">
            <c:forEach var="item" items="${itens}">
                <li>
                    <!--<a href="#page2" data-transition="slide">-->
                    <a href="#" data-ajax="false" data-transition="slide">
                        <img src="/files/produtos/${item.produto1.produtoImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
                        <h2 style="color: #33393d">${item.produto1.descricao}</h2>
                        <p style="font-weight:400;color: #4CAF50 !important;">
                            ${item.quantidade} x R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.produto1.preco}" /> = 
                            <b>R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.quantidade * item.produto1.preco}" /></b></p>
                    </a>
                </li>
                <c:set var="totalPedido" value="${totalPedido + (item.quantidade * item.produto1.preco)}"/>
            </c:forEach>          
        </ul>
    </div>

    <div data-role="footer" data-position="fixed">

        <div style="line-height:30px;padding:5px;;">
            <div style="width:48%;padding-left:1%;display:inline-block;">
                <label style="display:inline-block;font-size:0.9em;vertical-align: middle;">
                    Frete: </label> <span style="vertical-align: middle;font-size:1.1em;">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${frete}"/> </span>
            </div>
            <div style="width:48%;display:inline-block;text-align: right">
                <label style="display:inline-block;font-size:0.9em;vertical-align: middle;">Total: </label>
                <span style="vertical-align: middle;font-size:1.1em;">R$
                    <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${totalPedido+frete}"/> 
                </span>
            </div>
        </div>
        <a href="#" onclick="$.mobile.changePage('finalizar', {changeHash: false});" data-transition="slide" data-ajax="false" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Continuar</a>
    
    </div> 
</div>



