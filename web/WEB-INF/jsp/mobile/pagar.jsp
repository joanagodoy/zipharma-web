<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST">
    <input type="hidden" name="open" id="open">
</form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_pagar">
    <div data-role="popup" id="confirmarPagamento">
        <div data-role="header">
            <h1 class='nd-title'>Confirmar Pagamento</h1>
        </div>
        <div data-role="content">
            <input type="hidden" id="cancelarSolicitarOrcamentoId"/>
            <center>
                <p>Tem certeza que deseja realizar este pagamento? (Esta ação não poderá ser desfeita)</p>
                <button onclick="charge();" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Sim</button>
                <button onclick="$('#confirmarPagamento').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não</button>
            </center>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/pedido?id=${idPedido}" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Efetuar Pagamento #${idPedido}</h1>
    </div>
    <div role="main" style="font-size: 25px;" class="ui-content" data-inset="false">
        <center>
            <p style="color: #EC9292; font-size: 9px;">
                Verifique se o orçamento está correto antes de efetuar o pagamento!
            </p>
        </center>
        <form id="formPagamento" onsubmit="return efetuarPagamento();">
            <input type="hidden" name="idPedido" id="idPedidoPagar" value="${idPedido}">
            <p>Número do Cartão</p>
            <input required type="text" onchange="verificarCartao();" value="${cartao.cartao}" id="pagamentoCartaoCredito" maxlength="16">
            <p>Bandeira</p>
            <img src="${pageContext.servletContext.contextPath}/resources/img/bandeiras/visa.jpg" id="imgBandeiravisa" class="bandeirasCartao">
            <img src="${pageContext.servletContext.contextPath}/resources/img/bandeiras/mastercard.jpg" id="imgBandeiramastercard" class="bandeirasCartao">
            <img src="${pageContext.servletContext.contextPath}/resources/img/bandeiras/amex.jpg" id="imgBandeiraamex" class="bandeirasCartao">
            <img src="${pageContext.servletContext.contextPath}/resources/img/bandeiras/diners.jpg" id="imgBandeiradiners" class="bandeirasCartao">
            <p>Cód. Segurança</p>
            <input required type="password" id="pagamentoCod" maxlength="4">
            <p>Nome</p>
            <input required type="text" id="pagamentoNome" value="${cartao.nome}">
            <p>Sobrenome</p>
            <input required type="text" id="pagamentoSobrenome" value="${cartao.sobrenome}">
            <p>Vencimento (mês/ano)</p>
            <input required type="text" maxlength="2" value="${cartao.mes}" id="pagamentoMes" class="pagamentoMes" onblur="verificaMesAno(this);" data-role="none" style="width: 30px;border:none;border-bottom:1px #ddd solid;"> / 
            <input required type="text" maxlength="4" value="${cartao.ano}" id="pagamentoAno" class="pagamentoAno" onblur="verificaMesAno(this);" data-role="none" style="width: 60px;border:none;border-bottom:1px #ddd solid;">
            <p>Valor do Pedido</p>
            <input type="text" disabled value="${total}">
            <input type="submit" id="botaoSubmitEfetuarPagamento" style="display:none;" data-role="none">
        </form>
    </div>
    <div data-role="footer" data-position="fixed">
        <button type="button" onclick="$('#botaoSubmitEfetuarPagamento').click();" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Efetuar Pagamento</button>
    </div> 
</div>