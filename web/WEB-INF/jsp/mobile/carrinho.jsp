<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>


                                          
<div data-role="page" id="page_carrinho">
    

                                          
    <div data-role="popup"  id="adicionarProduto">
        <div data-role="header" role="banner" class="ui-header ui-bar-inherit">
            <a href="#" class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext ui-btn-right" data-rel="back">Fechar</a>
            <h1 class="ui-title" role="heading" aria-level="1">Novo Produto</h1>
        </div>
        <div role="main" class="ui-content">
            <h5>Digite um nome para o produto</h5>
            <input type="text" name="produto"  data-clear-btn="true"  id="novoProduto">
            <a href="#" onclick="adicionarProduto($('#novoProduto').val());" data-rel="back" class="ui-btn ui-corner-all">Adicionar</a>
        </div>
    </div>
    <div data-role="panel" id="bottomCarrinho" class="ui-bottom-sheet" data-animate="false" data-position='bottom' data-display="overlay">
        <div class='row around-xs'>
            <div class='col-xs-auto' style="display:inline-block;width:40%;text-align: center;">
                <button type="button" onclick="$('#adicionarProduto').popup('open');" data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop" class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='true'><i class='zmdi zmdi-edit zmd-2x'></i><strong>Adicionar Produto</strong></button>
            </div>
            <div class='col-xs-auto' style="display:inline-block;width:40%;text-align: center;">
                <button type="button" onclick="window.location = '../receita/adicionar?id=${cliente.id}&alt=${cliente.salt}';" class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='false'><i class='zmdi zmdi-camera-add zmd-2x'></i><strong>Adicionar Foto</strong></button>
            </div>
        </div>
    </div>

    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/home" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <a href="#bottomCarrinho" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-more-vert"></i></a>
        <h1>Carrinho</h1>
        <ul data-role="nd2tabs" data-swipe="true">
            <li data-tab="itens" id="tabItensCarrinho">Itens</li>
            <li data-tab="receitas" >Receitas</li>
        </ul>
    </div>
   
    <div role="main" data-inset="false">
        
        
                                          
        <div data-role="nd2tab" data-tab="itens">
            
            
                                          
            <c:if test="${fn:length(carrinho) > 0}">
                <c:forEach var="item" items="${carrinho}">
                    <c:if test="${item.produto != null}">
                    <div class="nd2-card card_${item.produto.id}">
                        </c:if>
                        <c:if test="${item.produto == null}">
                            <div class="nd2-card card_item_${item.carrinhoItemPK.id}">
                            </c:if>
                            <div class="card-title has-avatar">
                                <c:if test="${item.produto != null}">
                                    <img class="card-avatar" src="/files/produtos/${item.produto.produtoImagemList[0].imagem}">
                                    <h3 class="card-primary-title">${item.produto.descricao}</h3>
                                </c:if>
                                <c:if test="${item.produto == null}">
                                    <img class="card-avatar" src="${pageContext.servletContext.contextPath}/resources/img/manual-icon2.png">

                                    <h3 class="card-primary-title">${item.descricao}</h3>
                                </c:if>
                            </div>
                            <div class="card-supporting-text has-action">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box"> 
                                            <label for="">Quantidade:</label>
                                            <input type="number" class="quantidade noClear" readonly="" step="1" min="1" data-clear-btn="true" style="text-align: center;" value="${item.quantidade}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>                                        
                            <div class="card-action">
                                <div class="row between-xs">
                                    <div class="col-xs-4">
                                        <div class="box">
                                            <a href="#" onclick="carrinhoRemover(${empty item.produto ? 0 : item.produto.id},${item.carrinhoItemPK.id})" class="ui-btn ui-btn-inline"><i class='zmdi zmdi-close'></i> Remover</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 align-right">
                                        <div class="box">
                                            <a href="#" onclick="carrinhoDiminuir(${empty item.produto ? 0 : item.produto.id},${item.carrinhoItemPK.id})" class="ui-btn ui-btn-inline"><i class="zmdi zmdi-minus zmd-2x"></i></a>
                                            <a href="#" onclick="carrinhoAdicionar(${empty item.produto ? 0 : item.produto.id},${item.carrinhoItemPK.id})" class="ui-btn ui-btn-inline"><i class="zmdi zmdi-plus zmd-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>                       
                </c:if>
                <c:if test="${carrinho == null || fn:length(carrinho) == 0}">
                    <br>
                    <center>
                        <span style="font-size: 14px; font-weight: 100;">Seu carrinho está vazio.</span>
                        
                    </center>
                </c:if>
                <center>
                    <br>
                    <button onclick="$('#adicionarProduto').popup('open');">CRIAR PRODUTO <i class="zmdi zmdi-edit" style="margin-left: 5px; font-size: 20px;"></i></button>
                </center>
            </div>
             <!--nd2-card-->
            <div data-role="nd2tab" data-tab="receitas">
                <c:forEach var ="r" items="${receitas}">                    
                    <div class="receita_${r.id}">
                        <br>
                        <center>
                            <a href="/files/receitas/${r.imagem}" class="swipebox">
                                <img style="box-shadow: 0px 0px 5px #888888;" src="/files/receitas/min_${r.imagem}" style="height: 100%;">
                            </a>
                        </center>
                            <br>
                            <br>
                        <form id="alterarReceitaForm"  action="receita/alterar" >
                            <input type="hidden" name="id" value="${r.id}"/>
                            <label style="padding-left: 43%" >Nome do médico</label>
                            <input type="text" name="medico"/>
                            <label style="padding-left: 42%">Código Promocional</label>
                            <input type="text" name="crm" />
                        </form>
                        <center>
                            <table>
                              <tr style="padding-left: 50%" >
                                <th> <a href="#" data-ajax="false" onclick="alterarReceitaCarrinho();" class="ui-btn ui-btn-inline"><i class='zmdi zmdi-check'></i> Salvar Dados</a>
                                 <a href="#" data-ajax="false" onclick="removerReceita(${r.id});" class="ui-btn ui-btn-inline"><i class='zmdi zmdi-close'></i> Remover Receita</a> </th>
                              </tr>
                            </table>  
                        </center>
                    </div>
                </c:forEach>
                <c:if test="${receitas != null && fn:length(receitas) > 0}">
                    <br>
                    <center>
                        <p style="font-size: 10px;">Esta(s) foto(s) não tem a mesma validade que uma receita real.</p> 
                    </center>	
                </c:if>
                    
                <c:if test="${receitas == null || fn:length(receitas) == 0}">
                    <br>
                    <center>
                        <span style="font-size: 14px; font-weight: 100;">Nenhuma receita à ser exibida.</span>
                    </center>
                </c:if>

            </div>

        </div>

        <div data-role="footer" data-position="fixed">
            <button type="button" onclick="finalizarCarrinho();" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Solicitar Orçamento</button>
        </div>


    </div>

