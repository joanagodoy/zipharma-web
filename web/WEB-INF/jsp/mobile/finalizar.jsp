<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_finalizar">
    <div data-role="header" data-position="fixed">
        <a onclick="parent.history.go(-2);" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Solicitar orçamento</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <form id="page_finalizarForm" onsubmit="return finalizarPedido();">
            <div data-role="popup" id="popupFarmacias" data-transition="slidedown">
                <div data-role="header" style="position: relative; padding-left: 10px; padding-right: 10px;">
                    <center>
                        <h3>Farmácias</h3>
                        <span title="Fechar" onclick="$('#popupFarmacias').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
                    </center>
                </div>
                <div role="content" style="padding:10px;padding-top:0px;margin-top: -10px;" name="conteudo" class="finalizarFarmaciaChooser">
                    <select onchange="onCidadeFiltroChange(this)" name="filtroCidade" id="filtroCidade" data-clear-btn="true">
                        <c:set var="cidade" value=""/>
                        <c:if test="${fn:length(farmacias) gt 0}">
                            <c:set var="cidade" value="${farmacias[0].cidade}"/>
                        </c:if>
                        <c:forEach var="item" items="${cidades}">
                            <option 
                                <c:if test="${cidade == item.cidadePK.cidade}">
                                    selected="" 
                                </c:if>
                                value="${item.cidadePK.cidade};${item.cidadePK.estado}">${item.cidadePK.cidade}</option>
                        </c:forEach>
                    </select>
                    <fieldset data-role="controlgroup" class="finalizarFarmacias">

                        <label for="checkboxAll">
                            <b>Selecionar todos</b> 
                        </label>
                        <input class="checkboxFarmacia" onclick="checkAllFarmacia()" type="checkbox" name="checkboxAll" id="checkboxAll"/>

                        <c:forEach var="f" items="${farmacias}">

                            <label for="checkboxFarmacia${f.id}">
                                <b>${f.nome}</b> 
                                (<span class="cidade">${f.cidade}</span> - ${f.estado})
                                <br>
                                ${f.endereco}<c:if test="${f.numero!=null && f.numero!=''}"> ${f.numero}</c:if><c:if test="${f.complemento!=null && f.complemento!=''}"> ${f.complemento}</c:if><c:if test="${f.bairro != null && f.bairro != ''}"> - ${f.bairro}</c:if>
                                    <br>
                                    <span class="internal zmdi zmdi-male" style="padding-right: 3px; vertical-align: middle; font-size: 18px;"></span>
                                <c:forEach var="i" begin="1" end="5">
                                    <c:if test="${i<=f.qualidade}">
                                        <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                    </c:if>
                                    <c:if test="${i>f.qualidade}">
                                        <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                    </c:if>
                                </c:forEach>
                                <br>
                                <span class="internal zmdi zmdi-local-shipping" style="vertical-align: middle; font-size: 18px;"></span>
                                <c:forEach var="i" begin="1" end="5">
                                    <c:if test="${i<=f.rapidez}">
                                        <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                    </c:if>
                                    <c:if test="${i>f.rapidez}">
                                        <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                    </c:if>
                                </c:forEach>
                            </label>
                            <input onchange="onClickCheckFarmacia(this);" class="checkboxFarmacia" type="checkbox" name="checkboxFarmacia${f.id}" id="checkboxFarmacia${f.id}">

                        </c:forEach>
                    </fieldset>
                    <center>
                        <button onclick="$('#popupFarmacias').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white; width: 100%;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> OK</button>
                    </center>
                </div>
            </div>
            <button type="button" style="background-color: #4CAF50; color: white;" onclick="dialogFarmacias();" class="btn" data-ajax="false">Selecione as farmácias</button>
            <select disabled id="selectFarmaciaDisabled" name="farmaciaDisabled" data-clear-btn="true" data-ajax="false" multiple="multiple" data-native-menu="false">
                <option value="" data-placeholder="true">Nenhuma farmácia selecionada</option>
                <c:forEach var="f" items="${farmacias}">
                    <option value="${f.id}">${f.nome}</option>
                </c:forEach>
            </select>
            <h4>Informações de pagamento</h4>
            <label for="">Forma de Pagamento:</label>
            <select data-clear-btn="true" name="formaPagamento" onchange="formaPagamentoChanged(this);">
                <c:forEach var="item" items="${formas}">
                    <option value="${item.id}">${item.descricao}</option>
                </c:forEach>
            </select>
            <div class="dadosCartao" style="display:none;">
                <label>Bandeira:</label>
                <select data-clear-btn="true" name="cartaoCredito">
                    <c:forEach var="item" items="${cartoes}">
                        <option value="${item.id}">${item.descricao}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="dadosDinheiro" style="display:none;">
                <label>Troco:</label>
                <input type="text" data-clear-btn="true" name="troco" onchange="checkNmb(this);" value="0,00"/>
            </div>
            <h4>Informações de entrega</h4>
            <label for="name2b">Endereço entrega:</label>
            <select data-clear-btn="true" name="endereco" onchange="enderecoChanged(this);">
                <c:forEach var="item" items="${enderecos}">
                    <option value="${item.id}">${item.descricao} (${item.logradouro} - ${item.cidade})</option>
                </c:forEach>
                <option value="N">Novo</option>
            </select>
            <div class="dadosEndereco" <c:if test="${fn:length(enderecos) gt 0}">style="display:none;"</c:if>>
                    <label>Descritivo</label>
                    <input type="text" data-clear-btn="true" placeholder="Ex: Minha casa, trabalho" name="descricao">
                    <label>Endereço</label>
                    <input type="text" data-clear-btn="true" placeholder="Ex: Rua José Pereira Liberato, 155" name="logradouro">
                    <label>Bairro</label>
                    <input type="text" data-clear-btn="true" placeholder="Ex: São João" name="bairro">
                    <label>Cidade</label>
                    <select data-clear-btn="true" name="cidade">
                    <c:forEach var="item" items="${cidades}">
                        <option value="${item.cidadePK.cidade};${item.cidadePK.estado}">${item.cidadePK.cidade}</option>
                    </c:forEach>
                </select>
                <label>Complemento</label>
                <input type="text" data-clear-btn="true"  placeholder="Ex: Casa, ou AP 503" name="complemento">
            </div>
            <h4>Observação</h4>
            <textarea name="observacao"></textarea>
            <button type="submit" class="submit" style="display:none"></button>
        </form>
    </div>
    <div data-role="footer" data-position="fixed">
        <button type="button" id="page_finalizarSubmit" onclick="$('#page_finalizarForm .submit').click();" data-transition="slide" data-ajax="false" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Enviar</button>
    </div> 
</div>
