<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- panel left -->
<div data-role="panel" id="leftpanel" data-display="overlay" data-position-fixed="true" style="background-color: #white ;margin-top: -1px;">

    <!--<div class='nd2-sidepanel-profile wow fadeInDown' style="background-color: #459E48;box-shadow: 0px 0px 100px #014600 inset;">-->
    <div class='nd2-sidepanel-profile wow fadeInDown' style="background-color: #459E48;">
        <!--<img class='profile-background' src="//lorempixel.com/400/200/abstract/2/" />-->
        <div class="row">
            <div class='col-xs-4 center-xs'>
                <div class='box'>
                    <img class="" style="width:80px;" src="${pageContext.servletContext.contextPath}/resources/img/capsula.png" />
                </div>
            </div>
            <div class='col-xs-8'>
                <div class='box profile-text'>
                    <strong>Bem vindo</strong>
                    <c:if test="${cliente != null}">
                        <span class='subline'>${cliente.nome}</span>
                    </c:if>
                    <c:if test="${cliente == null}">
                        <span class='subline'>Você está em ${localizacao}</span>
                    </c:if>
                </div>
            </div>
        </div>
    </div>


    <ul data-role="listview" data-inset="false">
        <li data-role="list-divider">Encontre um especialista na sua região</li>
            <c:forEach var="especialidade" items="${especialidades}">
                <c:if test="${especialidade.status}">
                <li><a href="${pageContext.servletContext.contextPath}/mobile/especialista/busca?especialidade=${especialidade.id}" data-ajax='true' data-transition="slide">${especialidade.descricao}</a></li>
                </c:if>
            </c:forEach>
    </ul>

    <br>            
    <br>            

    <div>
        <span style="color: #0294d8">Precisa de ajuda?</span><br>        
        <a href="" onclick="telefonar();" span style="color: #c82951">Ligue para o farmacêutico <span class="zmdi zmdi-phone"></span></a>
    </div>
    
    
</div>
<!-- /panel left -->
