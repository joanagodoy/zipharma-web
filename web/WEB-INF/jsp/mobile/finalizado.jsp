<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_finalizar">
    <div data-role="header" data-position="fixed">
        <a onclick="parent.history.go(-2);" class="ui-btn ui-btn-left"><i class="zmdi zmdi-home"></i></a>
        <h1>Pedido cadastrado</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <h3>Pedido #${pedido} cadastrado com sucesso!</h3>
        <c:if test="${itensReceita != null}">
            <h5>Porém os itens abaixo dependem de receita, clique no botão abaixo para enviar as receitas!</h5>
        </c:if>
        <ul data-role="listview" data-icon="false">
            <c:forEach var="item" items="${itensReceita}">
                <li>
                    <a href="#" data-ajax="false" data-transition="slide">
                        <img src="/files/produtos/${item.produto1.produtoImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
                        <h2 style="color: #33393d">${item.produto1.descricao}</h2>
                    </a>
                </li>
                <c:set var="totalPedido" value="${totalPedido + (item.quantidade * item.produto1.preco)}"/>
            </c:forEach>          
        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
         <a href="pedido/consulta?id=${pedido}" data-transition="slide" data-ajax="false" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button"
        <c:if test="${itensReceita != null}">
            Enviar receita
        </c:if>
        <c:if test="${itensReceita == null}">
            Visualizar pedido
        </c:if>
       </a>
    </div> 
</div>