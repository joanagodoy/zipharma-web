<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<input type="hidden" value="${pedido.id}" id="pedidoId">
<table>
    <c:forEach var="i" items="${pedido.pedidoItemLista}">
        <tr><td><div title="${i.produto.descricao}" style="word-break: break-all; overflow-x: hidden; overflow-y: hidden; word-wrap: break-word; height: 20px;"><b><c:if test="${i.quantidade<10}">0</c:if>${i.quantidade} <span style="color:#949494;">x</span> <c:if test="${i.descricao==null}">${i.produto.descricao}</c:if><c:if test="${i.descricao!=null}">${i.descricao}</c:if></b></div></td><td width="25%" align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${i.preco}"/></td></tr>
                    </c:forEach>
    <tr><td><b style="color: #949494;">Frete</b></td><td align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${pedido.frete}"/></td></tr>
    <tr style="line-height: 8px;"><td></td></tr>
    <tr><td><b>Total</b></td><td align="right"><b><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${pedido.total}"/></b></td></tr>
</table>
<div style="width: 60%; border-bottom: 1px #AFADAD solid; margin-top: 5px; margin-bottom: 15px;"></div>
<table style="width: 100%; margin-bottom: 15px;">
    <tr>
        <td align="center"><span class="internal zmdi zmdi-time" style="font-size: 18px; vertical-align: -1px;"></span></td>
        <td>Tempo de Entrega</td>
        <td width="40%" align="right">${pedido.tempoEntrega}</td>
    </tr>    
</table>
<center>  
    <c:if test="${pedido.status == 'AguardandoPagamento'}">
        <button onclick="$.mobile.changePage('pagar?idPedido=${pedido.id}', {type: 'post', changeHash: true});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Pagar</button>
        <button onclick="$('#popupPedido').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Fechar</button>
    </c:if>
    <c:if test="${pedido.status != 'AguardandoPagamento'}">
        <b>Orçamento 
            <c:if test="${pedido.status == 'Cancelado'}"><span style="color: red;">cancelado</span></c:if>
            <c:if test="${pedido.status == 'AguardandoEnvio' || pedido.status == 'Concluído' || pedido.status == 'Enviado'}"><span style="color: green;">aceito/pago</span></c:if>
            </b>!
    </c:if>
</center>