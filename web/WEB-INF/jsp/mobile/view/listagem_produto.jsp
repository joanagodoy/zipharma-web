<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:forEach var="item" items="${produtos}">
    <li>
        <!--<a href="#page2" data-transition="slide">-->
        <a href="${pageContext.servletContext.contextPath}/mobile/produto?id=${item.id}" data-ajax="true" data-transition="slide">
            <img data-original="/files/produtos/${item.produtoImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
            <h2 style="color: #33393d">${item.descricao}</h2>
            
            <c:if test="${item.preco != null && item.preco > 0}">
            <p style="font-weight:400;color: #4CAF50 !important;">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                              groupingUsed="true" value="${item.preco}" /> (preço médio)</p>
            </c:if>
        </a>
    </li>
</c:forEach>