<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${fn:length(pedidos)>0}">
    <ul data-role="listview" id="listViewPedidos" data-icon="false">
        <c:forEach var="o" items="${pedidos}">
            <c:set var="color" value="#D6FFD6"/>
            <c:if test="${o.status == 'PagamentoNegado' || o.status == 'NaoConcluído'}">
                <c:set var="color" value="#FFDAD6"/>
            </c:if>
            <c:if test="${o.status == 'Enviado'}">
                <c:set var="color" value="#D6DFFF"/>
            </c:if>
            <c:if test="${o.status == 'AguardandoEnvio'}">
                <c:set var="color" value="#FEFFD6"/>
            </c:if>
            <li style="background-color: ${color}">
                <a href="${pageContext.servletContext.contextPath}/mobile/pedido?id=${o.id}" data-ajax="true" data-transition="slide">
                    <h2 style="color: #33393d">Pedido #${o.id}<span style="font-weight: 400;font-size:13px;"> - <fmt:formatDate pattern="dd/MM/yyyy - HH:mm" value="${o.dataEmissao}"/></span><span style="margin-left: 5px; font-size: 20px; color: #626562;vertical-align: middle; " class="internal zmdi zmdi-calendar-note"></span></h2>
                    <p style="color: #33393d;font-weight: bold;font-size:13px;"><span style="vertical-align: middle;">${o.farmacia.nome}</span> - <span style="font-weight: 400;vertical-align: middle;">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${o.total}"/></span> 
                        <!--<span style="font-weight: bold;margin-left: 5px; font-size: 18px; color: #626562; vertical-align: middle;" class="internal zmdi zmdi-money"></span>-->
                    </p>
                    <p><b>${o.status.descricao}</b></p>
                </a>
            </li>
        </c:forEach>
    </ul>
</c:if>
<c:if test="${fn:length(pedidos)==0}">
    <br>
    <center>
        <span style="font-size: 14px; font-weight: 100;">Nenhum registro à ser exibido.</span>
    </center>
</c:if>