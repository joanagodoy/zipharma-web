<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:forEach var="item" items="${farmacias}">
    <li>
        <a href="${pageContext.servletContext.contextPath}/mobile/conversa?id=${item.id}" data-ajax="true" data-transition="slide">
            <h3 style="text-transform: uppercase;font-weight:400;">
                <c:if test="${item.online > 0}">
                    <span class="internal zmdi zmdi-circle" style="color:#317928" title="Online"></span>
                </c:if>
                <c:if test="${item.online == 0}">
                    <span class="internal zmdi zmdi-circle" style="color:#CE4646" title="Offline"></span>
                </c:if>
                <b>${item.nome}</b> 
                (${item.cidade} - ${item.estado})
                <c:if test="${item.totalMensagem != 0}"><span class="ui-li-count ui-btn-corner-all countBubl">${item.totalMensagem}</span></c:if>
            </h3>
                <!--<p style="text-transform: uppercase;">${item.endereco}<c:if test="${item.numero!=null && item.numero!=''}"> ${item.numero}</c:if><c:if test="${item.complemento!=null && item.complemento!=''}"> ${item.complemento}</c:if><c:if test="${item.bairro != null && item.bairro != ''}"> - ${item.bairro}</c:if></p>-->
            <p><span class="internal zmdi zmdi-male" style="padding-right: 3px; vertical-align: middle; font-size: 18px;"></span>
                <c:forEach var="i" begin="1" end="5">
                    <c:if test="${i<=item.qualidade}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(230, 100, 120); text-shadow: 1px 1px 1px black;"></span>
                    </c:if>
                    <c:if test="${i>item.qualidade}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                    </c:if>
                </c:forEach>
            </p>
            <p>
                <span class="internal zmdi zmdi-local-shipping" style="vertical-align: middle; font-size: 18px;"></span>
                <c:forEach var="i" begin="1" end="5">
                    <c:if test="${i<=item.rapidez}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(230, 100, 120); text-shadow: 1px 1px 1px black;"></span>
                    </c:if>
                    <c:if test="${i>item.rapidez}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                    </c:if>
                </c:forEach>
            </p>
        </a>
    </li>
</c:forEach>