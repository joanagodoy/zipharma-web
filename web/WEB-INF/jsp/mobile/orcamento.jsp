<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_orcamento">
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/orcamentos" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1>Orçamento #${orcamento.id}</h1>
        <ul data-role="nd2tabs" data-swipe="true">
            <li data-tab="orcamentos" id="tabInfoOrcamento">Orçamentos</li>
            <li data-tab="info" >Info</li>
        </ul>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <div data-role="nd2tab" data-tab="info">
            <h4>Informações</h4>
            <label>Data emissão</label>
            <input type="text" readonly="" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${orcamento.dataEmissao}"/>">
            <label>Forma de pagamento</label>
            <input type="text" readonly="" value="${orcamento.formaPagamento.descricao}">
            <c:if test="${orcamento.formaPagamento.id==1}">
                <label>Cartão de Crédito</label>
                <input type="text" readonly="" value="${orcamento.cartaoCredito.descricao}"> 
            </c:if>
            <c:if test="${orcamento.formaPagamento.id==2}">
                <label>Troco</label>
                <input type="text" readonly="" value="${orcamento.troco}"> 
            </c:if>
            <label>Status</label>
            <input type="text" readonly="" value="${orcamento.status.descricao}">
            <ul data-role="listview" data-icon="false">
                <li data-role="list-divider">Endereço entrega</li>
            </ul>
            <label>Endereço</label>
            <input type="text" readonly="" value="${orcamento.endereco}">
            <label>Complemento</label>
            <input type="text" readonly="" value="${orcamento.complemento}">
            <label>Cidade</label>
            <input type="text" readonly="" value="${orcamento.cidade}">
            <c:if test="${orcamento.observacao != null && orcamento.observacao!=''}">
                <label>Observação</label>
                <textarea disabled>${orcamento.observacao}</textarea>
            </c:if>
            <h4>Itens</h4>
            <ul data-role="listview" data-icon="false">
                <li data-role="list-divider">Produtos</li>
                    <c:forEach var="item" items="${orcamento.orcamentoItemLista}">
                    <li>
                        <a href="#" data-ajax="false" data-transition="slide">
                            <c:if test="${item.produto.produtoImagemList[0]!=null}">
                                <img src="/files/produtos/${item.produto.produtoImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
                            </c:if>
                            <h2 style="color: #33393d">
                                <c:if test="${item.produto!=null}">${item.produto.descricao}</c:if>
                                <c:if test="${item.produto==null}">${item.descricao}</c:if>
                                </h2>
                                <span style="font-weight: normal;">(${item.quantidade}x)</span>
                        </a>
                    </li>
                </c:forEach>
                <li data-role="list-divider">Receitas</li>
                    <c:forEach var="item" items="${orcamento.receitaLista}">
                    <li>
                        <!--<a href="#page2" data-transition="slide">-->
                        <a href="#" data-ajax="false" data-transition="slide">
                            <img src="/files/receitas/min_${item.imagem}" class="lazy" />
                            <h2 style="color: #33393d">${item.medico}</h2>
                            <p>${item.crm}</p>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
        <div data-role="nd2tab" data-tab="orcamentos">
            <ul data-role="listview" data-filter="false" data-icon="false">
                <c:forEach var="item" items="${pedidos}">  
                    <c:if test="${item.key.acesso != 'Free'}">
                        <c:if test="${item.value != null}">
                            <c:choose>
                                <c:when test="${item.value.status == 'Cancelado' || item.value.status == 'OutraFarmacia'}">
                                    <c:set var="bgcolor" value="#FFDAD6"/>
                                </c:when>
                                <c:when test="${item.value.status == 'AguardandoOrcamento' || item.value.status == 'AguardandoAprovação' || item.value.status == 'AguardandoPagamento'}">
                                    <c:set var="bgcolor" value="#FFFED6"/>
                                </c:when>
                                <c:when test="${item.value.status == 'AguardandoEnvio' || item.value.status == 'Enviado' || item.value.status == 'Concluído'}">
                                    <c:set var="bgcolor" value="#D6FFD6"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="bgcolor" value="none"/>
                                </c:otherwise>
                            </c:choose>
                            <li>
                                <a data-transition="slide" 
                                   <c:if test="${item.value.status == 'AguardandoOrcamento'}">href="#cancelarOrcamento" onclick="$('#cancelarSolicitarOrcamentoId').val('${item.value.id}');"  data-rel="popup" data-position-to="window"</c:if>
                                   <c:if test="${item.value.status != 'AguardandoOrcamento'}">href="pedido?id=${item.value.id}"</c:if>
                                   style="background-color: ${bgcolor};">
                                    <h3 style="text-transform: uppercase;font-weight:400;">
                                        <b>${item.key.nome}</b> 
                                        (${item.key.cidade} - ${item.key.estado})
                                    </h3>
                                    <!--<p style="text-transform: uppercase;">${item.key.endereco}<c:if test="${item.key.numero!=null && item.key.numero!=''}"> ${item.key.numero}</c:if><c:if test="${item.key.complemento!=null && item.key.complemento!=''}"> ${item.key.complemento}</c:if><c:if test="${item.key.bairro != null && item.key.bairro != ''}"> - ${item.key.bairro}</c:if></p>-->
                                        <p><span class="internal zmdi zmdi-male" style="padding-right: 3px; vertical-align: middle; font-size: 18px;"></span>
                                        <c:forEach var="i" begin="1" end="5">
                                            <c:if test="${i<=item.key.qualidade}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                            </c:if>
                                            <c:if test="${i>item.key.qualidade}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                            </c:if>
                                        </c:forEach>
                                    </p><p>
                                        <span class="internal zmdi zmdi-local-shipping" style="vertical-align: middle; font-size: 18px;"></span>
                                        <c:forEach var="i" begin="1" end="5">
                                            <c:if test="${i<=item.key.rapidez}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                            </c:if>
                                            <c:if test="${i>item.key.rapidez}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                            </c:if>
                                        </c:forEach>
                                    </p>
                                    <c:choose>
                                        <c:when test="${item.value.status == 'AguardandoOrcamento'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-alarm"></span> ${item.value.status.descricao}</p>
                                        </c:when>
                                        <c:when test="${item.value.status == 'AguardandoAprovação'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-alert-circle-o"></span> ${item.value.status.descricao} <span class="orcamentoValor">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.value.total}" /></span></p>    
                                        </c:when>
                                        <c:when test="${item.value.status == 'AguardandoPagamento'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-money"></span> ${item.value.status.descricao} <span class="orcamentoValor">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.value.total}" /></span></p>    
                                        </c:when>
                                        <c:when test="${item.value.status == 'AguardandoEnvio'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-alert-circle-o"></span> ${item.value.status.descricao} <span class="orcamentoValor">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.value.total}" /></span></p>
                                        </c:when>
                                        <c:when test="${item.value.status == 'Enviado'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-local-shipping"></span> ${item.value.status.descricao} <span class="orcamentoValor">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.value.total}" /></span></p>
                                        </c:when>
                                        <c:when test="${item.value.status == 'Concluído'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-thumb-up"></span>  ${item.value.status.descricao} <span class="orcamentoValor">R$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${item.value.total}" /> </span></p>
                                        </c:when>
                                        <c:when test="${item.value.status == 'Cancelado' || item.value.status == 'OutraFarmacia'}">
                                            <p style="font-size:13px;color: #9E9E9E;"><span class="zmdi zmdi-block"></span> ${item.value.status.descricao}</p>
                                        </c:when>
                                    </c:choose>




                                </a>
                            </li>
                        </c:if>
                        <c:if test="${item.value == null }">
                            <li>
                                <a href="#solicitarOrcamento" onclick="$('#solicitarOrcamentoNome').html('${item.key.nome}');
                                        $('#solicitarOrcamentoFarmacia').val('${item.key.id}');
                                        $('#solicitarOrcamentoId').val('${orcamento.id}');"  data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop">
                                    <h3 style="text-transform: uppercase;font-weight:400;">
                                        <b>${item.key.nome}</b> 
                                        (${item.key.cidade} - ${item.key.estado})
                                    </h3>
                                    <!--<p style="text-transform: uppercase;">${item.key.endereco}<c:if test="${item.key.numero!=null && item.key.numero!=''}"> ${item.key.numero}</c:if><c:if test="${item.key.complemento!=null && item.key.complemento!=''}"> ${item.key.complemento}</c:if><c:if test="${item.key.bairro != null && item.key.bairro != ''}"> - ${item.key.bairro}</c:if></p>-->
                                        <p><span class="internal zmdi zmdi-male" style="padding-right: 3px; vertical-align: middle; font-size: 18px;"></span>
                                        <c:forEach var="i" begin="1" end="5">
                                            <c:if test="${i<=item.key.qualidade}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                            </c:if>
                                            <c:if test="${i>item.key.qualidade}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                            </c:if>
                                        </c:forEach>
                                    </p><p>
                                        <span class="internal zmdi zmdi-local-shipping" style="vertical-align: middle; font-size: 18px;"></span>
                                        <c:forEach var="i" begin="1" end="5">
                                            <c:if test="${i<=item.key.rapidez}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(226, 197, 39); text-shadow: 1px 1px 1px black;"></span>
                                            </c:if>
                                            <c:if test="${i>item.key.rapidez}">
                                                <span class="internal zmdi zmdi-star" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                                            </c:if>
                                        </c:forEach>
                                    </p>
                                    <p style="font-size:12px;color: #9E9E9E;"><span class="zmdi zmdi-tap-and-play"></span> Clique para solicitar um orçamento</p>
                                </a>
                            </li>
                        </c:if>
                    </c:if>
                </c:forEach>
            </ul>
            <div data-role="popup" id="solicitarOrcamento">

                <div data-role="header">
                    <h1 class='nd-title'>Solicitar orçamento?</h1>
                </div>

                <div data-role="content">
                    <input type="hidden" id="solicitarOrcamentoId"/>
                    <input type="hidden" id="solicitarOrcamentoFarmacia"/>
                    <p>Tem certeza que deseja solicitar um orçamento para <span id="solicitarOrcamentoNome"></span></p>
                    <a href="#" onclick="solicitarOrcamento()" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-check'></i> Sim</a>
                    <a href="#" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-cancel'></i> Cancelar</a>
                </div>

            </div>
            <div data-role="popup" id="cancelarOrcamento">

                <div data-role="header">
                    <h1 class='nd-title'>Cancelar solicitação?</h1>
                </div>

                <div data-role="content">
                    <input type="hidden" id="cancelarSolicitarOrcamentoId"/>
                    <p>Tem certeza que deseja cancelar esta solicitação?</p>
                    <a href="#" onclick="cancelarSolicitacao()" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-check'></i> Cancelar</a>
                    <a href="#" data-rel="back" data-role="button" data-inline="true" class="ui-btn ui-btn-primary"><i class='zmdi zmdi-cancel'></i> Não</a>
                </div>

            </div>
        </div>
    </div>
</div>