<%-- 
    Document   : busca
    Created on : 26/10/2015, 12:50:23
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_produto">
    <div data-role="popup"  id="lerMais">
        <div data-role="header" role="banner" class="ui-header ui-bar-inherit">
        </div>
        <div role="main" class="ui-content">
            <span onclick="$('#lerMais').popup('close');"><i class="zmdi zmdi-close" style="font-size: 25px; position: absolute; top: 7px; right: 10px;"></i></span>
            <div id="lerMaisContent" style="margin-top: 17px;">                
            </div>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
        <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <a title="Carrinho" onclick="carrinho();" href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>
        <h1 onclick="lerMais('${produto.descricao}')">${produto.descricao}</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
<!--        <h3 style="font-weight: 500;">${produto.descricao}<br></h3>-->
        <center style="margin: 0 auto;">
            <c:forEach var="imagem" items="${produto.produtoImagemList}">
                <c:if test="${imagem.principal}">
                    <a rel="gallery_${produto.id}" href="/files/produtos/${imagem.imagem}" class="swipebox">
                        <img style="height:25vw;max-height: 25%; box-shadow: 0px 0px 2px 1px #bbb;" src="/files/produtos/${imagem.imagem}">
                    </a>
                </c:if>
            </c:forEach>
            <div class="row center-xs" style="width: 100%;">
                <c:set var="idx" value="${0}"/>
                <c:forEach var="imagem" items="${produto.produtoImagemList}" varStatus="statusLoop">
                    <c:if test="${idx == 0}">
                    </c:if>
                    <c:if test="${idx != 0 && idx % 5 == 0}">
                    </div>
                    <div class="row center-xs">
                    </c:if>
                    <c:if test="${!imagem.principal}">
                        <c:set var="idx" value="${idx+1}"/>
                        <div class="col-xs-2" style="box-shadow: 0px 0px 2px 1px #bbb;margin-left:5px; margin-right:5px;width:16.667%;display:inline-block;background-color:white;">
                            <a rel="gallery_${produto.id}" href="/files/produtos/${imagem.imagem}" class="swipebox">
                                <img style="width:70%;vertical-align: middle;" src="/files/produtos/${imagem.imagem}">
                            </a>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </center>
        <div data-role="collapsible">
            <h3>Informações</h3>
            <input type="hidden" id="inputProdutoInfo" value="${produto.informacoes}">
            <div class='inset' id="insetProdutoInfo" style="white-space: pre-wrap;">
            </div>
            <script>
                $("#insetProdutoInfo").html($("#inputProdutoInfo").val());
            </script>
        </div>
        <button onclick="adicionarCarrinho(${produto.id});" class="ui-btn ui-btn-inline" style="color: #4CAF50;box-shadow: 1px 1px 1px 1px;"><i class="zmdi zmdi-shopping-cart-add"></i> Adicionar ao carrinho</button>
        <br>
        <br>
        <button onclick="adicionarCarrinho(${produto.id}, 'S');" class="ui-btn  ui-btn-inline" style="background-color:#4CAF50;color:white;"><i class="zmdi zmdi-check"></i> Comprar agora</button>
    </div>


</div>