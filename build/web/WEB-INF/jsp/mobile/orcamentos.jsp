<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_orcamentos">
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/home" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1>Orçamentos</h1>
        <ul data-role="nd2tabs" data-swipe="true">
            <li data-tab="orcamentos" data-tab-active="true" id="tabOrcamentos">Orçamentos</li>
            <li data-tab="pedidos">Pedidos</li>
        </ul>
    </div>
    <select style="" id="filtroDataOrcamentos" onchange="listarOrcamentos();">
        <option value="0">Todos</option>
        <option value="7" selected>Últimos 7 dias</option>
        <option value="15">Últimos 15 dias</option>
        <option value="30">Últimos 30 dias</option>
    </select>
    <div role="main" class="ui-content" data-inset="false">
        <div data-role="nd2tab" id="abaOrcamentos" data-tab="orcamentos">
            
        </div>
        <div data-role="nd2tab" id="abaPedidos" data-tab="pedidos">
            
        </div>
    </div>
</div>