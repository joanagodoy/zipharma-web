<%-- 
    Document   : busca
    Created on : 26/10/2015, 12:50:23
    Author     : Usuario
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_busca_categoria">
    
    <div data-role="header" data-position="fixed">
        <a data-rel="back" id="busca_back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1>Buscar Por Categoria</h1>
    </div>
       
    <ul data-role="listview" data-inset="false">
       <!--<li data-role="list-divider">Categorias</li>-->
    </ul>
    <!--<hr class="inset">-->
    <c:forEach var="categoria" items="${categorias}">
        <c:if test="${fn:length(categoria.subcategoriaList) gt 0}">
            <div style="padding-left: 10px;padding-right: 10px" data-role="collapsible" data-inset="false" data-collapsed-icon="carat-d" data-expanded-icon="carat-d" data-iconpos="right">
                <h3>${categoria.descricao}</h3>
                <ul data-role="listview" data-icon="false">
                    <c:forEach var="sub" items="${categoria.subcategoriaList}">
                        <c:if test="${sub.status}">
                            <li><a href="${pageContext.servletContext.contextPath}/mobile/produto/busca?categoria=${sub.id}" data-ajax='true' data-transition="slide">${sub.descricao}</a></li>
                            </c:if>
                     </c:forEach>
                </ul>
            </div>
        </c:if>
    </c:forEach>
    
</div>