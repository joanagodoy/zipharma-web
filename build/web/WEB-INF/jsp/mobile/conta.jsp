<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_conta">
    <div data-role="popup" id="alterarSenha">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Alterar Senha</h3>
                <span title="Fechar" onclick="$('#alterarSenha').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            <form id="formAlterarSenha" onsubmit="return alterarSenha();">
                <label>Senha Atual</label>
                <input required data-clear-btn="true" type="password" name="senhaAtual">
                <label>Nova Senha</label>
                <input required data-clear-btn="true" type="password" name="novaSenha">
                <label>Confirmar Nova Senha</label>
                <input required data-clear-btn="true" type="password" name="novaSenha2">
                <button type="submit" id="buttonSubmitAlterarSenha" style="display: none;"></button>
            </form>
            <center>
                <button onclick="$('#buttonSubmitAlterarSenha').click();" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Alterar</button>
                <button onclick="$('#alterarSenha').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Cancelar</button>
            </center>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
       <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Minha Conta</h1>
        <a title="Novo Endereço" href="${pageContext.servletContext.contextPath}/mobile/endereco?id=0" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-plus"></i></a>
        <ul data-role="nd2tabs" data-swipe="true">
            <li data-tab="info" data-tab-active="true" id="tabInfoConta">Conta</li>
            <li data-tab="enderecos">Endereços</li>
        </ul>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <div data-role="nd2tab" id="abaInfoConta" data-tab="info">
            <label>Nome</label>
            ${userConta.nome}
            <br>
            <br>
            <label>Email</label>
            ${userConta.email}
            <br>
            <br>
            <c:if test="${userConta.cadastro == null}">                
                <center>
                    <a title="Alterar Senha" href="#alterarSenha" onclick="$('#formAlterarSenha').get(0).reset();" class="ui-btn ui-btn-inline wow fadeIn" data-rel="popup" data-position-to="window" data-inline="true" data-transition="pop" style="background-color: #4CAF50;color: white;">Alterar Senha</a>
                </center>
            </c:if>
        </div>
        <div data-role="nd2tab" data-tab="enderecos">
            <c:if test="${fn:length(enderecos)>0}">
                <ul data-role="listview" data-icon="false">
                    <c:forEach var="end" items="${enderecos}">
                        <li style="background-color: #D6FFD6">
                            <a href="${pageContext.servletContext.contextPath}/mobile/endereco?id=${end.id}" data-ajax="true" data-transition="slide">
                                <h3><span class="internal zmdi zmdi-city-alt" style="vertical-align: top;margin-right: 5px;"></span>${end.descricao}</h3>
                                <p>${end.logradouro} (${end.cidade} - ${end.estado})</p>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
            <c:if test="${fn:length(enderecos)==0}">
                <br>
                <center>
                    <span style="font-size: 14px; font-weight: 100;">Nenhum registro à ser exibido.</span>
                </center>
            </c:if>
        </div>
    </div>
    <div data-role="footer" data-position="fixed">    
    </div> 
</div>