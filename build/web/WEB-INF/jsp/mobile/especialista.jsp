<%-- 
    Document   : busca
    Created on : 26/10/2015, 12:50:23
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_especialista">
    <div data-role="popup"  id="lerMaisEspecialista">
        <div data-role="header" role="banner" class="ui-header ui-bar-inherit">
        </div>
        <div role="main" class="ui-content">
            <span onclick="$('#lerMaisEspecialista').popup('close');"><i class="zmdi zmdi-close" style="font-size: 25px; position: absolute; top: 7px; right: 10px;"></i></span>
            <div id="lerMaisEspecialistaContent" style="margin-top: 17px;">                
            </div>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
        <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a title="Carrinho" onclick="carrinho();" href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1 onclick="lerMaisEspecialista('${especialista.nome}')">${especialista.nome}</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <center style="margin: 0 auto;">
           <c:forEach var="imagem" items="${especialista.especialistaImagemList}">
                <c:if test="${imagem.principal}">
                    <a rel="gallery_${especialista.id}" href="/files/especialistas/${imagem.imagem}" class="swipebox">
                        <img style="height:25vw;max-height: 25%; box-shadow: 0px 0px 2px 1px #bbb;" src="/files/especialistas/${imagem.imagem}">
                    </a>
                </c:if>
            </c:forEach>
            <div class="row center-xs" style="width: 100%;">
                <c:set var="idx" value="${0}"/>
                <c:forEach var="imagem" items="${especialista.especialistaImagemList}" varStatus="statusLoop">
                    <c:if test="${idx == 0}">
                    </c:if>
                    <c:if test="${idx != 0 && idx % 5 == 0}">
            </div>
            <div class="row center-xs">
                    </c:if>
                    <c:if test="${!imagem.principal}">
                        <c:set var="idx" value="${idx+1}"/>
                        <div class="col-xs-2" style="box-shadow: 0px 0px 2px 1px #bbb;margin-left:5px; margin-right:5px;width:16.667%;display:inline-block;background-color:white;">
                            <a rel="gallery_${especialista.id}" href="/files/especialistas/${imagem.imagem}" class="swipebox">
                                <img style="width:70%;vertical-align: middle;" src="/files/especialistas/${imagem.imagem}">
                            </a>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <br>
            ${especialista.especialidade.cr.sigla} ${especialista.crm}    
        </center>
<!--        <div>
            
            <h3>Clínica</h3> ${especialista.clinica.nome}
            <h3>Cidade</h3> ${especialista.clinica.cidade}
        </div>-->
        <br>
        <br>
       
            <h3>Informações</h3>
            <input type="hidden" id="inputEspecialistaInfo" value="${especialista.informacoes}">
            <div class='inset' id="insetEspecialistaInfo" style="white-space: pre-wrap;">
            </div>
            <script>
                $("#insetEspecialistaInfo").html($("#inputEspecialistaInfo").val());
            </script>
           
 
        <br>
        <br>
        <button onclick="window.location = 'ligar/'+${especialista.telefone}" class="ui-btn  ui-btn-inline" style="background-color:#4CAF50;color:white;"><i class="zmdi zmdi-phone"></i> Agendar uma Consulta </button>
        <br>
<!--        <div data-role="collapsible">
            <h3>Ver Telefone</h3>
            <input type="hidden" id="inputEspecialistaTelefone" value="${especialista.telefone}">
            <div class='inset' id="insetEspecialistaTelefone" style="white-space: pre-wrap;">
            </div>
            <script>
                $("#insetEspecialistaTelefone").html($("#inputEspecialistaTelefone").val());
            </script>
        </div>-->
    </div>


</div>