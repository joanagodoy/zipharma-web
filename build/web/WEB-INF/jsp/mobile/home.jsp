<%-- 
    Document   : index
    Created on : 16/10/2015, 16:36:59
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>


<!--<ul data-role="nd2tabs" data-swipe="true">
    <li id="tabPrincipal" data-tab-active="true" data-tab="principal">Menu</li>
<c:if test="${cliente != null}">
<li data-tab="tabChat">Chat</li>
</c:if>
</ul>-->

</div>
<style>
    .ui-input-text:after, .ui-input-search:after {
        width: 94% !important;
    }
</style>
<div data-role="popup"  id="popupTermos">
    <div data-role="header" style="position: relative;">
        <h3>Termos de Uso e Política de Privacidade</h3>
    </div>
    <div role="content" style="padding-left:10px;padding-right:10px;">
        <br>
        <div style="height: 250px; overflow: auto;">
            A ZIPPHARMA dispõe a seguir os termos de uso de sua plataforma de marketplace focada no comércio farmacêutico. Ao baixar nossos aplicativos ou acessar a plataforma em versão web, você, usuário FARMÁCIA ou CLIENTE, manifesta plena concordância com estes termos de uso e com a política de privacidade de nossa plataforma.<br><br>
            CONSIDERAÇÕES INICIAIS<br>
            A ZIPPHARMA é uma plataforma online que busca viabilizar, através da disponibilização de espaço para que FARMÁCIAS possam ofertar e concluir vendas de produtos farmacêuticos a CLIENTES, contando com meio de pagamento exclusivo, de forma dinâmica e econômica para ambos os lados.
            <br><br>
            A ZIPPHARMA é somente uma plataforma que facilita a conexão de VENDEDORES e CLIENTES. As FARMÁCIAS não possuem qualquer tipo de vínculo de prestação de serviços com a ZIPPHARMA, que também não responde pelas relações que ocorrem entre elas e CLIENTES na plataforma. Assim, não temos responsabilidade sobre direitos, deveres, perdas ou danos de qualquer natureza que advinham dessa relação.
            <br><br>
            A ZIPPHARMA não se responsabiliza pelo estrito cumprimento dos preceitos legais que regulamentam a atividade das FARMÁCIAS, que devem garantir a mais lídima conduta na condução de negócios no ambiente da plataforma.
            <br><br>
            Ao acessar a plataforma e serviços agregados à ela, em navegador web ou via aplicativo, você CLIENTE, concluindo ou não processo de compra de medicamento de FARMÁCIA inserida na plataforma, manifesta plena concordância com os termos a seguir dispostos, ciente de que poderão ocorrer atualizações periódicas conforme o caso.
            <br><br>
            A ZIPPHARMA, nem quaisquer de seus empregados ou prepostos solicitará, por qualquer meio, físico ou eletrônico, que seja informada sua senha para atendimento de chamados de qualquer natureza.
            <br><br>
            A ZIPPHARMA poderá definir pacotes de serviços diferenciados, gratuitos ou pagos, para CLIENTES e/ou FARMÁCIAS, agregando serviços de acordo com a faixa de preços definida, não estando obrigada a estender tais funcionalidades aos usuários gratuitos ou adquirentes de pacotes inferiores.
            <br><br>
            DO CADASTRO DOS USUÁRIOS<br>
            Quando cria uma conta na ZIPPHARMA, você manifesta plena ciência e concordância em fornecer informações verdadeiras nos campos de cadastro. Se por algum motivo detectarmos alguma discrepância em seus dados, de acordo com a gravidade do caso, nos reservamos no direito de suspender o seu acesso ou ainda terminá-lo de forma definitiva.
            <br><br>
            DO LOGIN E SENHA<br>
            Você é responsável pela guarda de seus dados de login e senha, criados no cadastro na ZIPPHARMA. Você é responsável, única e exclusivamente, por todas as atividades que ocorram em sua conta e se compromete a nos notificar imediatamente em caso de usos não autorizados de sua conta ou quaisquer outras violações de segurança. A ZIPPHARMA não será responsável por quaisquer perdas e danos resultantes de acessos não autorizados ou uso de sua conta.
            <br><br>
            Em caso de falecimento de algum usuário pedimos que nos seja enviado um e-mail para: contato@zippharma.com.br. Serão exigidos alguns dados do interessado e a remoção da conta do usuário será feita por nossa equipe técnica.
            <br><br>
            RESPONSABILIDADES DO CLIENTE
            Você, CLIENTE, deverá escolher o produto que deseja adquirir, bem como a FARMÁCIA de sua preferência, efetuando o pagamento pelo meio de pagamento disponível na ZIPPHARMA.
            <br><br>
            Você declara estar ciente de suas obrigações legais referentes ao fornecimento das respectivas receitas médicas necessárias, de acordo com a categoria de medicamento adquirido, condição absoluta para a conclusão da negociação realizada na plataforma.
            <br><br>
            Embora a ZIPPHARMA realize criterioso processo de seleção de FARMÁCIAS para ingresso na plataforma, o cumprimento da legislação que regulamenta a comercialização de medicamentos, seja do ponto de vista técnico, consumerista ou demais, é de integral e exclusiva responsabilidade dela; eventuais falhas e problemas ocorridos que não sejam relacionadas ao funcionamento da plataforma ZIPPHARMA deverão ser reclamadas exclusivamente à FARMÁCIA, restando a ZIPPHARMA isenta de responsabilidade nesses casos.
            <br><br>
            Se você for menor de 18 anos, somente fará uso da ZIPPHARMA sob supervisão de um responsável legal.
            <br><br>
            Você não utilizará a ZIPPHARMA como meio de divulgação de materiais ou informações de natureza comercial, sob qualquer hipótese, sob pena de impedimento definitivo de acesso à plataforma e ressarcimento de eventuais prejuízos que causar à ZIPPHARMA, se houverem.
            Você se compromete a não publicar conteúdos que ofendam a dignidade humana ou expressem racismo, dados inverídicos, pornografia, apologia criminosa, preconceito de gênero, ofensas, calúnia ou difamação;
            <br><br>
            RESPONSABILIDADES DA FARMÁCIA<br>
            Você, FARMÁCIA, receberá dos CLIENTES da ZIPPHARMA pedidos de orçamento de produtos farmacêuticos, devendo portanto, para melhores resultados na utilização da plataforma, manter rigoroso controle de estoque e constante atenção às notificações de entrada de pedidos de orçamento.
            <br><br>
            Você declara estar ciente de suas obrigações legais referentes ao fornecimento das respectivas receitas médicas necessárias, de acordo com a categoria de medicamento adquirido, condição absoluta para a conclusão da negociação realizada na plataforma, desde já excluindo a ZIPPHARMA de qualquer responsabilidade oriundos de descumprimento legal por sua parte ou pelo CLIENTE.
            <br><br>
            A ZIPPHARMA fornece ferramenta para envio digital de receitas, o que não supre suas obrigações legais de atestar veracidade e realizar retenção, nos termos da lei, no caso de aquisição de medicamentos de uso controlado. É de sua exclusiva responsabilidade, FÁRMÁCIA, operacionalizar o trâmite regular de receitas médicas, sob pena de exclusão sumária da plataforma e sanções legais concernentes.
            <br><br>
            Você poderá ofertar/comercializar na ZIPPHARMA somente aqueles produtos cuja venda não esteja expressamente proibida por estes termos ou pela lei vigente, sendo vedada a utilização de qualquer meio de comunicação com CLIENTE, disponibilizado em algumas versões da plataforma ou externos, para direcionamento de vendas por outro meio que não a ZIPPHARMA.
            <br><br>
            É de sua exclusiva responsabilidade o cumprimento da legislação que regulamenta a comercialização de medicamentos, seja do ponto de vista técnico, consumerista ou demais, desde já concordando em isentar a ZIPPHARMA de responsabilidade em caso de descumprimento de tais normas.
            <br><br>
            A logística de entrega dos produtos e o trâmite de receitas de medicamentos controlados é de sua  exclusiva responsabilidade.
            <br><br>
            Você não utilizará a ZIPPHARMA como meio de divulgação de materiais ou informações de natureza comercial, sob qualquer hipótese, sob pena de impedimento definitivo de acesso à plataforma e ressarcimento de eventuais prejuízos que causar à ZIPPHARMA, se houverem.
            <br><br>
            Você se compromete a não publicar conteúdos que ofendam a dignidade humana ou expressem racismo, dados inverídicos, pornografia, apologia criminosa, preconceito de gênero, ofensas, calúnia ou difamação de outras FARMÁCIAS, CLIENTES, equipe ZIPPHARMA ou terceiros;
            <br><br>
            DOS PRODUTOS<br>
            Nós somos responsáveis pela plataforma, que foi desenvolvida para comercialização exclusiva de produtos farmacêuticos. CLIENTES e FARMÁCIAS deverão utilizar nossos serviços apenas para tal fim, utilizando-se dos meios de comunicação e pagamento exclusivos disponibilizados no aplicativo.
            <br><br>
            Em caso de descumprimento deste preceito, a ZIPPHARMA reserva-se no direito de impedir imediatamente o acesso à plataforma, bem como cobrar as taxas de serviços e prejuízos verificados.
            <br><br>
            DA PROPRIEDADE INTELECTUAL<br>
            Ao disponibilizar qualquer conteúdo na ZIPPHARMA, você nos concede uma licença não exclusiva e irrevogável para a publicação deste conteúdo. Você declara que tem todos os direitos necessários para outorgar esta licença e que o seu conteúdo não viola direitos de propriedade intelectual de terceiros, sendo você o único e exclusivo responsável pela publicação deste conteúdo. A ZIPPHARMA poderá monitorar, editar ou remover conteúdos, no todo ou em parte, que não sigam os critérios acima; e poderá divulgar o seu conteúdo por qualquer motivo – seja para responder a reclamações de violação de direitos de terceiros, pela segurança de nossos usuários, por exigência da lei ou por quaisquer outros motivos.
            <br><br>
            Você concede à ZIPPHARMA um direito não exclusivo, bem como uma licença para reprodução, distribuição, apresentação pública, uso e exploração do curso para fins de controle de qualidade e entrega, marketing, promoção, demonstração ou operação do site e dos produtos, podendo a ZIPPHARMA utilizar para tais fins sua aparência, imagem ou voz renunciando desde já a quaisquer direitos de privacidade, publicidade ou quaisquer outros direitos desta natureza, nos termos da lei.
            <br><br>
            DA OPERACIONALIZAÇÃO DE COMPRAS E VENDAS<br>
            A compra e venda inicia-se na plataforma ZIPPHARMA, mediante busca realizada pelo CLIENTE entre as FARMÁCIAS de sua preferência para solicitar orçamento dos produtos desejados. Em caso de aprovação de orçamento recebido, o CLIENTE utiliza o meio de pagamento e procede com a finalização da compra. A confirmação de aprovação do pagamento e seu respectivo recebimento se darão através do meio de pagamento –  IUGU (http://c.iugu.com/).
            <br><br>
            O CLIENTE tem ciência de que o meio de pagamento opera exclusivamente com pagamentos via cartão de crédito.
            <br><br>
            A FARMÁCIA, ao receber a confirmação de pagamento da compra do CLIENTE, enviado pelo meio de pagamento, deverá proceder a entrega do medicamento, respeitando o prazo estimado informado no contato.
            <br><br>
            A FARMÁCIA deve inserir no orçamento seus custos de frete, sendo o único meio de cobrança de tais valores admitido durante o uso dos serviços ZIPPHARMA.
            <br><br>
            Os valores recebíveis pela FARMÁCIA serão creditados em conta exclusiva do meio de pagamento, sendo que os créditos de cada venda são realizados em até 30 (trinta) dias da data da operação, descontados a taxa de serviço ZIPPHARMA, de 5% (cinco por cento), e os custos operacionais do meio de pagamento, quais sejam 4,5% de taxa de serviço e R$ 0,30 (trinta centavos) por operação.
            <br><br>
            A FARMÁCIA poderá realizar transferência dos créditos de venda para conta corrente de sua titularidade a partir de 30 (trinta) dias da data do crédito, respeitado o valor mínimo de transferência de R$ 200,00 (duzentos reais), não havendo custos para tal operação, exceto se houver adiantamento de crédito, hipótese em que será aplicada taxa estabelecida pelo meio de pagamento.
            <br><br>
            A ZIPPHARMA se reserva o direito de iniciar e processar cancelamentos, sem a necessidade de sua intervenção ou aprovação, caso detecte indícios de fraude em negociações, estejam elas pendentes de aprovação ou já aprovadas, em caso de cancelamento ou em caso de erro técnico no processamento da transação;
            <br><br>
            Em caso de contestação de cobrança indevida, iniciada pelo responsável pelo cartão de crédito junto à operadora de seu cartão, estas serão analisadas pela própria operadora do cartão e pelo meio de pagamento.
            <br><br>
            A ZIPPHARMA não se responsabiliza por problemas no processamento de pagamento ou problemas técnicos do equipamento dos usuários que venham a impedir a regular operação de compras e vendas.
            <br><br>
            LIMITAÇÃO DE RESPONSABILIDADE
            A ZIPPHARMA fornece um ambiente para que FARMÁCIAS e CLIENTES realizem compra e venda de produtos farmacêuticos de maneira ágil e prática. Por isso, a ZIPPHARMA não será responsável, sob quaisquer circunstâncias, por eventuais perdas e danos relacionados ao uso de nossos serviços. Nos reservamos o direito de modificar, suspender ou descontinuar temporariamente os serviços que prestamos.
            <br><br>
            ISENÇÃO<br>
            Você concorda em isentar a ZIPPHARMA e seus diretores, funcionários, parceiros e colaboradores, de todos e quaisquer danos, perdas, responsabilidades e despesas resultantes de disputas entre você e terceiros, em conexão com a utilização de nossos serviços.
            <br><br>
            INDENIZAÇÃO<br>
            Você concorda em indenizar a ZIPPHARMA e seus diretores, funcionários, parceiros e colaboradores por todos e quaisquer danos, perdas, responsabilidades e despesas resultantes de reivindicações ou investigações feitas por terceiros e que estejam relacionadas ao conteúdo por você divulgado na plataforma ou à violação destes Termos de Uso.
            <br><br>
            SUPORTE TÉCNICO<br>
            Os suportes técnicos do aplicativo ocorrerão na medida em que eventuais falhas no serviço forem detectadas pelos programadores do aplicativo e noticiadas pelos usuários através dos canais oficiais de comunicação da ZIPPHARMA.
            <br><br>
            DA MARCA
            O nome “ZIPPHARMA” e logotipo são marcas registradas, e não podem ser copiadas, imitadas ou utilizadas, no todo ou em parte, sem a permissão prévia por escrito. Além disso, todos os cabeçalhos, gráficos personalizados, ícones e scripts são marcas comerciais da ZIPPHARMA, e não podem ser copiadas, imitadas ou utilizadas, no todo ou em parte, sem permissão prévia por escrito.
            <br><br>
            MUDANÇAS NOS TERMOS DE USO<br>
            Em função das constantes atualizações que a Plataforma ZIPPHARMA recebe, os Termos de Uso e Política de Privacidade são constantemente reformulados. Por isso, é de sua responsabilidade verifica-los periodicamente. O uso continuado do site após quaisquer alterações destes Termos de Uso indica sua concordância com os termos revistos.
            <br><br>
            DISPOSIÇÕES GERAIS<br>
            Caso você e a ZIPPHARMA tenham algum tipo de disputa que não possa se resolver de maneira amigável, desde já, que fica eleito o foro da comarca de Balneário Camboriú, Estado de Santa Catarina, para resolver controvérsias ou queixas oriundas da utilização de nosso site ou relacionadas a estes Termos de Uso.
            <br>
            <br>
            <h3 style="font-weight: 700;">Política de Privacidade</h3>
            A ZIPPHARMA adota a máxima transparência com você, usuário, CLIENTE e FARMÁCIA, sobre os dados obtidos no seu acesso à plataforma e como são utilizadas essas informações. Pedimos um pouco de sua atenção para nossa política de privacidade, lembrando-o de consultá-la periodicamente em função da possibilidade de alterações.
            <br><br>
            1- DADOS E INFORMAÇÕES COLETADOS<br>
            FARMÁCIAS:<br>
            Além dos dados de cadastro fornecidos por seu representante durante sua inscrição na plataforma, também são registrados os dados lançados por você durante as trocas de informações via chat com os clientes e dados de acesso ao meio de pagamento que instrumentaliza as transações entre CLIENTES, ZIPPHARMA E FARMÁCIAS. Sendo assim, as informações necessárias para operacionalizar os procedimentos de natureza financeira, como dados bancários e afins, também serão armazenados para que sejam realizadas referidas operações e lançamentos fiscais concernentes.
            <br><br>
            CLIENTES:<br>
            Ao cadastrar-se na plataforma, os dados pessoais fornecidos por você são armazenados. Caso necessário, a ZIPPHARMA reserva-se no direito de solicitar novas informações indispensáveis ao uso pleno da plataforma. A FARMÁCIA que recebeu seu pedido receberá seu nome e item solicitado. Ao adquirir um produto de uma das FARMÁCIAS cadastradas em nossa plataforma, também receberemos e transmitiremos as informações exigidas por nosso parceiro de pagamentos para o devido processamento da transação, como os dados do cartão de crédito, CPF, endereço e data de nascimento. Os dados de pagamento sensíveis fornecidos nesta etapa, como o número do cartão de crédito e código de segurança, não são guardados em nossos servidores. Além das informações recebidas durante os processos de inscrição e pagamento, descritos acima, também recebemos informações suas quando você entra em contato conosco ou responde a uma das nossas pesquisas.
            <br><br>
            DOS COOKIES<br>
            A ZIPPHARMA facilita o reconhecimento de seus acessos através do uso de cookies, que são dados armazenados que auxiliam no reconhecimento de seus dados, possibilitando que você possa acessar a plataforma sem, por exemplo, digitar sempre seu usuário e senha a cada acesso. É possível desativar esse armazenamento em seu navegador, o que poderá acarretar em problemas de acesso à ZIPPHARMA.
            <br><br>
            OUTROS DADOS ARMAZENADOS<br>
            Dados como IP, localização geográfica aproximada e histórico de navegação e acesso são armazenados durante seu uso da plataforma. A ZIPPHARMA se utiliza desses dados durante seu acesso porém não os compartilha com terceiros.
            <br><br>
            DO USO DAS INFORMAÇÕES COLETADAS<br>
            As informações coletadas são utilizadas unicamente para prover acesso irrestrito aos serviços oferecidos na ZIPPHARMA.
            <br><br>
            Seus dados pessoais são necessários para o gerenciamento da sua conta e seu atendimento, identificar potenciais atividades fraudulentas e garantir a aplicação dos Termos de Uso. Seus canais de comunicação serão utilizados para contata-lo(a) em caso de necessidades técnicas ou ainda informa-lo(a) de novas funcionalidades, rotinas de manutenção e informações importantes sobre a ZIPPHARMA e/ou FARMÁCIAS cadastradas na plataforma.
            <br><br>
            DA GUARDA DAS INFORMAÇÕES<br>
            É de nossa responsabilidade a guarda de suas informações pessoais. Somente serão divulgadas em caso de intervenção de parceiro para otimização de nossa prestação de serviços, hipótese na qual a confidencialidade de tais informações será condição absoluta para o compartilhamento. Também, em caso de necessidade de cumprimento forçado dos Termos de Uso, reservamo-nos no direito de divulga-las conforme as regras estabelecidas no documento referido.
            Os outros dados armazenados poderão ser fornecidos a terceiros para razões de marketing, publicidade ou outros usos similares – desde que não se possa identifica-lo(a) através dos mesmos.
            <br><br>
            Você, CLIENTE, concorda que a ZIPPHARMA não se responsabiliza pelo mau uso que a FARMÁCIA venha a fazer com os dados fornecidos durante seu contato via chat, quando possível, recaindo exclusivamente sobre ela a responsabilidade sobre o uso correto dessas informações.
            <br><br>
            DA PROTEÇÃO DOS DADOS<br>
            Rotinas de segurança e monitoramento são constantemente realizadas a cada criação, envio ou registro de suas informações pessoais. Por meio de tecnologia SSL (Secure Socket Layer), informações como senhas e dados financeiros são transmitidos de maneira totalmente segura. Senhas e informações confidenciais são registradas sob alto padrão criptográfico em nossos servidores.
            <br><br>
            DA POLÍTICA DE PRIVACIDADE DOS PARCEIROS ZIPPHARMA<br>
            A presente política de privacidade limita-se à ZIPPHARMA somente. É importante que você tenha ciência da política de privacidade de cada parceiro que eventualmente componha a operação de nossos serviços, que poderão ter políticas distintas.
            <br><br>
            DAS ALTERAÇÕES NA POLÍTICA DE PRIVACIDADE<br>
            É bem provável que nossa política de privacidade e termos de uso sejam atualizados e alterados com o passar do tempo. Ao continuar utilizando-se do nosso site, você manifesta concordância plena com as novas regras definidas. Por isso, é importante consulta-los periodicamente.
            <br><br>
            DA CONCORDÂNCIA<br>
            Ao cadastrar-se e utilizar nossa plataforma e serviço, você manifesta total concordância com a política de privacidade e Termos de Uso.
        </div>
        <center>
            <br>
            <button onclick="aceitarTermos();" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Aceito</button>
            <button onclick="recusarTermos();" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Não Aceito</button>
            <br>
            <br>
        </center>
    </div>
</div>
<div data-role="popup"  id="popupLocal">
    <div data-role="header" style="position: relative;">
        <h3>Alterar localização</h3>
    </div>
    <script>var enderecos = [];var end_principal = 0;</script>
    <div role="content" style="padding-left:10px;padding-right:10px;">
        <select onchange="changeLocal();" id="enderecos">
            <c:if test="${fn:length(enderecos)==0}">
                <option value="">ENDEREÇO PRINCIPAL</option>
            </c:if>
            <c:forEach var="end" items="${enderecos}">
                <c:if test="${end.principal}">
                    <script>end_principal =${end.id};</script> 
                </c:if>
                <option class="end_${end.id}" value="${end.id}">${end.logradouro} (${end.cidade} - ${end.estado})</option>
                <script>enderecos.push({descricao: '${end.descricao}', cliente: '${end.cliente}', id: '${end.id}', cep: '${end.cep}', endereco: '${end.logradouro}', complemento: '${end.complemento}', bairro: '${end.bairro}', cidade: '${end.cidade}', estado: '${end.estado}'});</script>
            </c:forEach></select>
        <form modelAttribute="endereco" id="localForm">
            CEP
            <input type="text" name="cep" id="local_cep" value="${local_cep}" placeholder="">
            Endereço
            <input type="text" name="logradouro" id="local_endereco" value="${local_endereco}" placeholder="" style="">
            Complemento
            <input type="text" name="complemento" id="local_complemento" value="${local_comp}" placeholder="">
            Bairro
            <input type="text" name="bairro" id="local_bairro" value="${local_bairro}" placeholder="">
            Cidade
            <input type="text" name="cidade" id="local_cidade" value="${local_cidade}" placeholder=""> 
            <input type="hidden" name="estado" id="local_estado" value="${local_estado}"> 
            <input type="hidden" name="principal" id="" value="true"> 
            <input type="hidden" name="cliente" id="local_cliente" value="${cliente.id}"> 
            <input type="hidden" name="descricao" id="local_descricao" value="Principal"> 
            <input type="hidden" name="id" id="local_id"> 
        </form>
        <button style="background-color: #4CAF50; color: white;" onclick="salvarLocal();">SALVAR</button>
        <br>
    </div>
</div>
<div role="main" class="ui-content" data-inset="false">
    <!--Main-->
    <c:if test="${cliente != null}">
        <div id="divLocal" class="divLocal" onclick="">
            <i class="zmdi zmdi-my-location"></i> <span id="enderecoPrincipal">${local_endereco}</span> <i class="zmdi zmdi-chevron-down"></i> 
        </div>
        <br>
        <br>
    </c:if>
    <ul data-role="listview" data-icon="false" class="menu">
        <c:if test="${cliente != null}">
            <li>
                <a href="${pageContext.servletContext.contextPath}/mobile/chat" data-ajax="true" data-transition="slide">
                    <i class="lazy ui-thumbnail ui-thumbnail-circular external"><i class="internal zmdi zmdi-comment-dots"></i></i>
                    <h1>Iniciar atendimento<p>Escolha uma farmácia</p></h1> 
                </a>
            </li>
        </c:if>
        <li style="display:none;">
            <!--<a href="" onclick="$('#leftpanel').panel('open');">-->
            <a href="${pageContext.servletContext.contextPath}/mobile/busca_categoria" data-ajax="true" data-transition="slide">
                <i class="lazy ui-thumbnail ui-thumbnail-circular external"><i class="internal zmdi zmdi-search"></i></i>
                <h1>Buscar por Categoria<p>Encontre produtos pelas suas categorias</p></h1>
            </a>
        </li>
        <li>
            <a href="#" onclick="window.location = '../receita/adicionarRMV?id=${cliente.id}&alt=${cliente.salt}';" data-ajax="true" data-transition="slide">
                <i class="external lazy ui-thumbnail ui-thumbnail-circular"><i class="internal zmdi zmdi-camera"></i></i>
                <h1>Foto da receita<p>Envie foto da receita ou produto</p></h1>
            </a>
        </li>
        <li>
            <a href="#" onclick="window.location = 'codigoBarras'" data-ajax="true" data-transition="slide">
                <i class="lazy ui-thumbnail ui-thumbnail-circular external"><i class="internal zmdi zmdi-view-week"></i></i>
                <h1>Código de barras <p style="font-size: 12px;">Encontre produtos pelo código de barras</p></h1>
            </a>
        </li>
        <!--        <li>
                    <a href="${pageContext.servletContext.contextPath}/mobile/ligar" data-ajax="true" data-transition="slide">
                        <i class="lazy ui-thumbnail ui-thumbnail-circular external"><i class="internal zmdi zmdi-phone"></i></i>
                        <h1>Ligar<p>Entre em contato com uma farmácia</p></h1>
                    </a>
                </li>-->
        <!--        <li>
                    <a href="" onclick="window.location = '../sair';" data-ajax="true">
                        <i class="lazy ui-thumbnail ui-thumbnail-circular external"><i class="internal zmdi zmdi-close"></i></i>
                        <h1>Sair</h1>
                    </a>
                </li>-->
    </ul>
    <!--<a href="#" class="ui-btn nd2-btn-icon-block" onclick="buscarProdutosDestaque()" style="width:100%;display:none;"><i class="zmdi zmdi-plus"></i> Carregar Mais</a>-->

    <c:forEach var="categoria" items="${categorias}">
        <c:if test="${fn:length(categoria.subcategoriaList) gt 0}">
            <div style="padding-left: 10px;padding-right: 10px" data-role="collapsible" data-inset="false" data-collapsed-icon="carat-d" data-expanded-icon="carat-d" data-iconpos="right">
                <h3>${categoria.descricao}</h3>
                <ul data-role="listview" data-icon="false">
                    <c:forEach var="sub" items="${categoria.subcategoriaList}">
                        <c:if test="${sub.status}">
                            <li><a href="${pageContext.servletContext.contextPath}/mobile/produto/busca?categoria=${sub.id}" data-ajax='true' data-transition="slide">${sub.descricao}</a></li>
                            </c:if>
                        </c:forEach>
                </ul>
            </div>
        </c:if>
    </c:forEach>

    <%@include file="footer.jsp" %>


