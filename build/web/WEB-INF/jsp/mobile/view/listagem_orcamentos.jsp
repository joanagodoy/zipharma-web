<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${fn:length(orcamentos)>0}">
    <ul data-role="listview" id="listViewOrcamentos" data-icon="false">
        <c:forEach var="o" items="${orcamentos}">
            <c:set var="color" value="none"/>
            <c:set var="status" value="${o.statusDescricao}"/>
            <c:if test="${o.statusDescricao == 'Pendente'}">
                <c:set var="color" value="#FFFED6"/>
                <c:if test="${o.aguardandoOrcamento == 0}">
                    <c:set var="status" value="Solicite um orçamento!"/>
                </c:if>
                <c:if test="${o.aguardandoOrcamento > 0}">
                    <c:set var="status" value="Aguardando orçamento"/>
                </c:if>
                <c:if test="${o.aguardandoAprovacao > 0}">
                    <c:set var="status" value="Verifique o orçamento"/>
                </c:if>
                <c:if test="${o.ok > 0}">
                    <c:set var="status" value="Aguardando entrega"/>
                </c:if>
            </c:if>
            <c:if test="${o.statusDescricao == 'Cancelado'}">
                <c:set var="color" value="#FFDAD6"/>
            </c:if>
            <c:if test="${o.statusDescricao == 'Concluído' || o.aguardandoAprovacao > 0 || o.ok > 0}">
                <c:set var="color" value="#D6FFD6"/>
            </c:if>
            <li style="background-color: ${color}">
                <a href="${pageContext.servletContext.contextPath}/mobile/orcamento?id=${o.id}" data-ajax="true" data-transition="slide">
                    <h2 style="color: #33393d">Orçamento #${o.id}<span style="font-weight: 400;font-size:13px;"> - ${o.dataEmissao}</span><span style="margin-left: 5px; font-size: 20px; color: #626562;vertical-align: middle;" class="internal zmdi zmdi-calendar-note"></span></h2>
                    <p><b>${status}</b></p>
                    <table>
                        <tr>
                            <td><span class="internal zmdi zmdi-alarm" style="font-size: 20px; color: #7DC17D; vertical-align: middle;"></span></td>
                            <td><span style="vertical-align: top; padding: 1px 3px 1px 0px; font-size: 10px;">${o.aguardandoOrcamento}</span></td>
                            <td><span class="internal zmdi zmdi-assignment-alert" style="font-size: 20px; color: #ECC171; vertical-align: middle;"></span></td>
                            <td><span style="vertical-align: top; padding: 1px 3px 1px 0px; font-size: 10px;">${o.aguardandoAprovacao}</span></td>
                            <td><span class="internal zmdi zmdi-local-shipping" style="font-size: 20px; color: #71A1EC; vertical-align: middle;"></span></td>
                            <td><span style="vertical-align: top; padding: 1px 3px 1px 0px; font-size: 10px;">${o.ok}</span></td>
                            <td><span class="internal zmdi zmdi-close-circle" style="font-size: 20px; color: #EC7171; vertical-align: middle;"></span></td>
                            <td><span style="vertical-align: top; padding: 1px 3px 1px 0px; font-size: 10px;">${o.cancelados}</span></td>
                        </tr>
                    </table>
                </a>
            </li>    
        </c:forEach>
    </ul>
</c:if>
<c:if test="${fn:length(orcamentos)==0}">
    <br>
    <center>
        <span style="font-size: 14px; font-weight: 100;">Nenhum registro à ser exibido.</span>
    </center>
</c:if>