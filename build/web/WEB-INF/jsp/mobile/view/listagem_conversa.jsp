<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="last" value=""/>
<jsp:useBean id="now" class="java.util.Date" scope="request" />
<fmt:formatDate value="${now}" var="now" pattern="yyyyMMdd" />
<c:forEach var="item" items="${conversa}"  varStatus="status">
    <c:if test="${item.usuario == null}">
        <c:set var="classe" value="cliente"/>
    </c:if>
    <c:if test="${item.usuario != null}">
        <c:set var="classe" value="usuario"/>
    </c:if>
    <li>
        <a  
            <c:if test="${item.pedido != null && item.mensagem == 'Enviado'}">onclick="visualizarPedidoDialog(${item.pedido});"</c:if> 
            <c:if test="${item.mensagem == null}">href="/files/chat/${item.imagem}" class="swipebox ${classe}"</c:if>
            <c:if test="${item.mensagem != null}">href="#" class="${classe}"</c:if>
                data-ajax="false" style="padding-top:0px;text-decoration:none;
            <c:if test="${fn:length(conversa) gt status.index+1 && conversa[status.index+1].usuario.id == item.usuario.id }">padding-bottom:0px;</c:if> 
                " data-transition="slide">
            <c:if test="${item.usuario.id != last && item.usuario.id != null}">
                <h2 style="color: #33393d;font-size:12px !important;">${item.usuario.nome}</h2>
            </c:if>
            <p class="speech ${classe}" style="display:inline-block;max-width:70%;white-space: normal;padding: 10px;border-radius: 10px;<c:if test="${item.mensagem == null}">height: 300px;min-width:85%;</c:if>">
                <c:set var="pattern" value="HH:mm"/>
                <fmt:formatDate value="${item.data}" var="data" pattern="yyyyMMdd" />
                <c:if test="${data != now}">
                    <c:set var="pattern" value="dd/MM/yyyy HH:mm"/>    
                </c:if>
                <span style="font-size:11px;font-weight:100;color: darkslategray;"><fmt:formatDate pattern="${pattern}" value="${item.data}"/></span>
                <br/>
                <c:if test="${item.mensagem == null}">

                    <img src="/files/chat/min_${item.imagem}" style="height:95%;overflow: hidden;width: 100%;"/>

                </c:if>
                <c:if test="${item.mensagem != null && item.pedido == null}">
                    ${item.mensagem}
                </c:if>
                <c:if test="${item.pedido != null}">
                    <c:if test="${item.mensagem == 'Enviado'}">
                        <b>Orçamento #${item.pedido} enviado</b> <span style="cursor: pointer; font-size: 21px; vertical-align: -3px; font-weight: 700;" title="Visualizar Pedido" class="internal zmdi zmdi-search"></span>
                    </c:if>
                    <c:if test="${item.mensagem == 'Aceito'}">
                        <b>Orçamento #${item.pedido} aceito</b>
                    </c:if>
                    <c:if test="${item.mensagem == 'Negado'}">
                        <b>Orçamento #${item.pedido} negado</b>
                    </c:if>
                    <c:if test="${fn:contains(item.mensagem, 'Solicitado;') || fn:contains(item.mensagem, 'SolicitadoFeito;')}">
                        <b>Orçamento solicitado</b>
                    </c:if>
                </c:if>
            </p>
            <input type="hidden" class="id" value="${item.id}"/>
        </a>
    </li>
    <c:set var="last" value="${item.usuario.id}"/>
</c:forEach>