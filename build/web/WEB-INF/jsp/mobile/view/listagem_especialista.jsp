<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:forEach var="item" items="${especialistas}">
    <li>
        <a href="${pageContext.servletContext.contextPath}/mobile/especialista?id=${item.id}" data-ajax="true" data-transition="slide">
            <img data-original="/files/especialistas/${item.especialistaImagemList[0].imagem}" class="lazy ui-thumbnail ui-thumbnail-circular" />
            <h2 style="color: #33393d">${item.nome}</h2>
            <p><span class="internal zmdi zmdi-male" style="padding-right: 3px; vertical-align: middle; font-size: 18px;"></span>
                <c:forEach var="i" begin="1" end="5">
                    <c:if test="${i<=item.qualidade}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(230, 100, 120); text-shadow: 1px 1px 1px black;"></span>
                    </c:if>
                    <c:if test="${i>item.qualidade}">
                        <span class="internal zmdi zmdi-favorite" style="vertical-align: middle; color: rgb(187, 187, 187); text-shadow: 1px 1px 1px rgb(187, 187, 187);"></span>
                    </c:if>
                </c:forEach>
            </p>
            <h3 style="color: #33393d">Cínica ${item.clinica.nome} (${item.clinica.cidade} - ${item.clinica.estado}) </h3>
        </a>
    </li>
</c:forEach>