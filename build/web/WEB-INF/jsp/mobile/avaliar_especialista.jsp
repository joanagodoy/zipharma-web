<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST">
    <input type="hidden" name="open" id="open">
</form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_avaliar">
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/pedido?id=${idPedido}" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Avaliação #${idPedido}</h1>
    </div>
    <div role="main" style="font-size: 25px;" class="ui-content" data-inset="false">
        <form id="formAvaliacao" action="finalizarAvaliacao">
            <input type="hidden" name="idPedido" id="idPedidoAvaliacao" value="${idPedido}">
            <input type="hidden" name="qualidade">
            <input type="hidden" name="rapidez">
            <p>Qualidade do Atendimento</p>
            <span class="internal zmdi zmdi-favorite qualidade" id="qualidade1" onclick="qualidadeAtendimento(1);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite qualidade" id="qualidade2" onclick="qualidadeAtendimento(2);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite qualidade" id="qualidade3" onclick="qualidadeAtendimento(3);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite qualidade" id="qualidade4" onclick="qualidadeAtendimento(4);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite qualidade" id="qualidade5" onclick="qualidadeAtendimento(5);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <br>
            <p>Rapidez na Entrega</p>
            <span class="internal zmdi zmdi-favorite rapidezEntrega" id="rapidezEntrega1" onclick="rapidezEntrega(1);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite rapidezEntrega" id="rapidezEntrega2" onclick="rapidezEntrega(2);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite rapidezEntrega" id="rapidezEntrega3" onclick="rapidezEntrega(3);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite rapidezEntrega" id="rapidezEntrega4" onclick="rapidezEntrega(4);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <span class="internal zmdi zmdi-favorite rapidezEntrega" id="rapidezEntrega5" onclick="rapidezEntrega(5);" title="" style="cursor: pointer; color: rgb(187, 187, 187)"></span>
            <br>
            <p>Observação</p>
            <textarea id="avaliacaoObs" name="observacao" style="width: 70%;"></textarea>
        </form>
    </div>
    <div data-role="footer" data-position="fixed">
        <button type="button" onclick="enviarAvaliacao();" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Enviar</button>
    </div> 
</div>