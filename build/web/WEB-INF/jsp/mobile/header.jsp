<%-- 
    Document   : index
    Created on : 16/10/2015, 16:36:59
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:set var="version" value="${3.21}"/>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zip Pharma</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script>
            $(document).bind("mobileinit", function() {
                $.extend($.mobile, {
                    pageLoadErrorMessage: "Verifique sua conexão"
                });
            });
        </script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/mobile/jquery.mobile.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/nativedroid2.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/waves.min.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/animate.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/geral.css?v=${version}">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/swipebox.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/nd2/nativedroid2.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/waves.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/wow.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/swipebox.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/pagar.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/avaliar.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/geral.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/jquery.lazyload.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/busca.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/home.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/produto.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/especialista.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/carrinho.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/login.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/conta.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/receita.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/chat.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/busca_especialista.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/endereco.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/finalizarPedido.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/orcamento.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/pedido.js?v=${version}" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/inputmask.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery.form.js" ></script>
        <script type="text/javascript" src="https://js.iugu.com/v2"></script>
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <script>
            function search(a) {
                $.mobile.changePage("${pageContext.servletContext.contextPath}/mobile/produto/busca?query=" + $("#formSearch input").val(), {transition: "slide"});
            }
            $(document).on("mobileinit", function() {
                $.extend($.mobile, {
                    pageLoadErrorMessage: "Verifique sua conexão"
                });
            });
            $(document).on("pageinit", "#page", function() {
                if ("${open}".length > 0) {
                    $.mobile.changePage("${open}", {transition: "slide"});
                }
                $("img.lazy").lazyload({
                    effect: "fadeIn"
                });
                $(document).on("swipeleft swiperight", "#page", function(e) {
                    // We check if there is no open panel on the page because otherwise
                    // a swipe to close the left panel would also open the right panel (and v.v.).
                    // We do this by checking the data that the framework stores on the page element (panel: open).
                    if ($.mobile.activePage.jqmData("panel") !== "open" && (!$("#tabPrincipal")[0] || $("#tabPrincipal").hasClass("nd2Tabs-active"))) {
                        if (e.type === "swipeleft") {
                            $("#rightpanel").panel("open");
                        } else if (e.type === "swiperight") {
                            $("#leftpanel").panel("open");
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="ui-loader-background"> </div>
        <div data-role="page" id="page">

            <%@include file="menu.jsp"%>
            <script>
                var aceitouTermos = true;
            </script>
            <c:if test="${termos!=null}">
                <script>
                    aceitouTermos = ${termos};
                </script>
            </c:if>
            <div data-role="panel" id="bottomsheetblock" class="ui-bottom-sheet" data-animate="false" data-position='bottom' data-display="overlay">
                <div class='row around-xs'>
                    <c:if test="${cliente != null}">
                         <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='${pageContext.servletContext.contextPath}/mobile/conta' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='true'><i class='zmdi zmdi-account-calendar zmd-2x'></i><strong>Conta</strong></a>
                        </div>
                        <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='#page_sobre' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop">
                                <i class='zmdi zmdi-info zmd-2x'></i><strong>Sobre</strong>
                            </a>
                            <div data-role="popup"  id="page_sobre">
                                <div data-role="header" style="position: relative;">
                                    <h1>Sobre</h1>
                                    <span title="Fechar" onclick="$('#page_sobre').popup('close');" style="font-size: 25px; cursor: pointer; top: 28%; right: 3%; position: absolute;" class="internal zmdi zmdi-close"></span>
                                </div>
                                <div role="content" style="padding-left:10px;padding-right:10px;">
                                    <p>Zip Pharma é uma plataforma que foi desenvolvida para ligar de forma simples e usual pessoas e farmácias.</p>
                                    <p>Para mais informações acesse <span onclick="window.location = 'site';">www.zippharma.com.br</span></p>
                                    <p style="font-size: 10px;">© Mobiletec Sistemas. Todos direitos reservados</p>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='${pageContext.servletContext.contextPath}/mobile/logout' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='false'><i class='zmdi zmdi-close-circle zmd-2x'></i><strong>Logout</strong></a>
                        </div>
                    </c:if>
                    <c:if test="${cliente == null}">
                        <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='${pageContext.servletContext.contextPath}/mobile/login' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='true'><i class='zmdi zmdi-sign-in zmd-2x'></i><strong>Entrar</strong></a>
                        </div>
                        <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='#' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='false'><i class='zmdi zmdi-account-add zmd-2x'></i><strong>Cadastrar</strong></a>
                        </div>
                        <div class='col-xs-auto' style="display:inline-block;width:33%;    text-align: center;">
                            <a href='${pageContext.servletContext.contextPath}/mobile/logout' class='ui-bottom-sheet-link ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button' data-ajax='false'><i class='zmdi zmdi-city zmd-2x'></i><strong>Trocar local</strong></a>
                        </div>
                    </c:if>
                </div>
            </div>

            <div data-role="header" data-position="fixed">
                <!--<a href="#" class="ui-btn ui-btn-left"><i class="zmdi zmdi-menu"></i></a>-->
                <a href="#leftpanel" class="ui-btn ui-btn-left"><i class="zmdi zmdi-local-hospital"></i></a>
                <!--<a href="#leftpanel" class="ui-btn ui-btn-left"><img src="../../../resources/img/Logo Zip.png"></a>-->
                <a href="#bottomsheetblock" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-account-o"></i></a>
                <h1 style="margin: 0 65px;">
                    <img onclick="window.location = '${pageContext.servletContext.contextPath}/mobile/home/'" style="height:35px;vertical-align: middle;" src="${pageContext.servletContext.contextPath}/resources/img/capsula.png"/>
                </h1>