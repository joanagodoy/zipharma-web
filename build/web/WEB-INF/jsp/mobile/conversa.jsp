<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>
<div data-role="page" id="page_conversa">
    <div data-role="popup" id="popupPedido" data-transition="slidedown">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo"></h3>
                <span title="Fechar" onclick="$('#popupPedido').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
        </div>
    </div>
    <div data-role="popup" id="popupEnderecosCliente" data-transition="slidedown">
        <div data-role="header" style="position: relative;">
            <center>
                <h3 name="titulo">Solicitar Orçamento</h3>
                <span title="Fechar" onclick="$('#popupEnderecosCliente').popup('close');" style="position: absolute; right: 5px; top: 0px; font-size: 25px; cursor: pointer;" class="internal zmdi zmdi-close"></span>
            </center>
        </div>
        <div role="content" style="padding:10px;padding-top:0px;" name="conteudo">
            <center>
                <p>Selecione o endereço</p>
                <select id="selectEnderecosCliente"><c:forEach var="end" items="${enderecos}">
                        <option value="${end.id}">${end.logradouro} (${end.cidade} - ${end.estado})</option>                       
                    </c:forEach></select>
                <button onclick="solicitarOrcamentoFarmacia(${farmacia.id});" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: #4CAF50; color: white;"><i class="zmdi zmdi-check" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Solicitar</button>
                <button onclick="$('#popupEnderecosCliente').popup('close');" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" style="background-color: rgba(255, 0, 0, 0.7); color: white;"><i class="zmdi zmdi-close" style="font-size: 22px; font-weight: 600; vertical-align: -2px;"></i> Fechar</button>
            </center>
        </div>
    </div>
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/chat" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
            <c:if test="${!silenciado}">
            <a href="#" data-ajax="false" onclick="silenciar(this,${farmacia.id});" class="ui-btn ui-btn-right wow fadeIn" style="margin-right:50px" data-wow-delay='1.2s'><i class="zmdi zmdi-volume-up"></i></a>
            </c:if>
            <c:if test="${silenciado}">
            <a href="#" data-ajax="false" onclick="silenciar(this,${farmacia.id});" class="ui-btn ui-btn-right wow fadeIn" style="margin-right:50px" data-wow-delay='1.2s'><i class="zmdi zmdi-volume-off"></i></a>
            </c:if>
        <a href="#" onclick="dialogSolicitarOrcamento();" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart"></i></a>
        <h1>${farmacia.nome}</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">
        <div id="conversa">
            <a href="#" class="ui-btn nd2-btn-icon-block" data-clear-btn="true" onclick="buscarConversa(true)" style="display:none;"><i class="zmdi zmdi-plus"></i> Carregar Mais</a>
            <br>
            <ul data-role="listview" data-icon="false">

            </ul>
            <br>
            <br>
        </div>
        <input type="hidden" id="page_conversaFarmacia" value="${farmacia.id}">
    </div>
    <div data-role="footer" data-tap-toggle="false">
        <div style="position: absolute; bottom: 0px; width: 96%; padding: 2%; background-color: #EEEFE6; box-shadow: 0px 0px 1px 1px #ddd;">
            <div style="background-color: white;padding:6px;">
                <input type="text" data-role="none" class="inputChat" placeholder="Digite sua mensagem aqui" style="background-color: white !important;padding-left:4px;display:inline-block;width:85%;border:0px;border-bottom: 1px solid #bbb;left: 1%;" name="mensagem"  data-inline="true">
                <div style="display:inline-block;width:10%;height: 26px;line-height: 26px;text-align: center;" >
                    <i class="zmdi zmdi-mail-send iconSend"  onclick="enviarMensagem();" style="display:none;color:#949494;vertical-align: middle;"></i>
                    <i class="zmdi zmdi-camera iconCamera" onclick="window.location = 'chat/adicionarImagem?farmacia=${farmacia.id}&id=${cliente.id}&alt=${cliente.salt}';" style="color:#949494;vertical-align: middle;"></i>
                </div>
            </div>
        </div>
    </div>
</div>