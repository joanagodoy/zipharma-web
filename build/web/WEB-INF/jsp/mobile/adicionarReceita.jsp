<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_adicionarReceita">
    
    <div data-role="header" data-position="fixed">
        <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <h1>Adicionar receita</h1>
    </div>
    
    <div role="main" class="ui-content" data-inset="false">
        
        <form id="page_adicionarReceitaForm" action="adicionar" enctype="multipart/form-data"  method="post" onsubmit="return adicionarReceita();">
            <label>Nome do médico</label>
            <input type="text" required name="medico"/>
                <label>Código do Profissional</label>
            <input type="text" required name="crm"/>
            <label>Receita</label>
            <button type="button" onclick="$('#page_adicionarReceitaForm [name=imagem]').click();" class="btn-mini"><i class="zmdi zmdi-camera"></i> Adicionar</button>
            <input onchange="readURL(this);" style="visibility: hidden;width:0px;height:0px;" type="file" name="imagem" accept="image/*;">
            <center>
                <img id="adicionarReceitaImage" style="width:90%;display:none;" src="#"/>
                <button type="submit" style="display:none;"></button>
            </center>
        </form>
    </div>

    <div data-role="footer" data-position="fixed">
        <a href="#" data-role="toast"
           data-toast-message="Simple toast message set by data-message" onclick="$('#page_adicionarReceitaForm [type=submit]').click();" data-transition="slide" data-ajax="false" style="width:100%;font-size:1.2em;" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Salvar</a>
    </div>
    
</div>