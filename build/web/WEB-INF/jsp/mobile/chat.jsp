<%-- 
    Document   : busca
    Created on : 26/10/2015, 12:50:23
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="${pageContext.servletContext.contextPath}/mobile/home/" method="POST"><input type="hidden" name="open" id="open"></form>
<script type="text/javascript">
    document.getElementById("open").value = window.location.href.substr(window.location.href.charAt("mobile/"));
    document.forms[0].submit();
</script>

<div data-role="page" id="page_chat">
    <style>.countBubl {float:left;background:#4CAF50 !important;color:#fff !important;border-radius: 50% !important;padding-right: 5px;padding-left: 5px;}</style>
    <div data-role="header" data-position="fixed">
        <a href="${pageContext.servletContext.contextPath}/mobile/home" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
        <!--<a href="#" class="ui-btn ui-btn-right wow fadeIn" data-wow-delay='1.2s'><i class="zmdi zmdi-shopping-cart-add"></i></a>-->
        <h1>Chat</h1>
    </div>
    <div role="main" class="ui-content" data-inset="false">

        <ul data-role="listview" data-icon="false" id="ulConversas">
            <%@include file="view/listagem_chat.jsp" %>
        </ul>

    </div>
</div>