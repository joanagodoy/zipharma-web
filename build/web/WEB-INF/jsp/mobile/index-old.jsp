<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zip Pharma</title>
        <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/resources/js/mobile/jquery.mobile.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/nativedroid2.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/waves.min.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/animate.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/geral.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/swipebox.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/nd2/nativedroid2.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/waves.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/wow.min.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/login.js" ></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/mobile/geral.js" ></script>

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    </head>
    <body>
        <script>
            // This is called with the results from from FB.getLoginStatus().
            function statusChangeCallback(response) {
                console.log('statusChangeCallback');
                console.log(response);
                // The response object is returned with a status field that lets the
                // app know the current login status of the person.
                // Full docs on the response object can be found in the documentation
                // for FB.getLoginStatus().
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    testAPI();
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
//                    document.getElementById('status').innerHTML = 'Please log ' +
//                            'into this app.';
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
//                    document.getElementById('status').innerHTML = 'Please log ' +
//                            'into Facebook.';
                }
            }

            // This function is called when someone finishes with the Login
            // Button.  See the onlogin handler attached to it in the sample
            // code below.
            function checkLoginState() {
                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '454506254755914',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true,
                    version: 'v2.2'
                });

                FB.getLoginStatus(function (response) {
                    statusChangeCallback(response);
                });

            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            function testAPI() {
//                FB.api('/me/permissions', function (response) {
//                    alert(JSON.stringify(response));
//                });
                FB.api('/me', {locale: 'pt_BR', fields: 'name, email,birthday, location'}, function (response) {
//                    alert(JSON.stringify(response));
                    
                    efetuarLoginFacebook(response.id, response.name, response.email, response.location.name, response.birthday);
                });
            }
        </script>

        <div class="ui-loader-background"> </div>

        <div data-role="page" id="page_login_cadastro">
            <div data-role="header" data-position="fixed">
                <h1>Zip Pharma</h1>
            </div>
            <div role="main" class="ui-content" data-inset="false">
                <center><h3 style="margin: 0px;font-weight: bold;">Cadastre-se</h3></center>
                <center><h6 style="margin: 20px;"><a href="#page_login" style="color:#222;text-decoration: none;">Já tem cadastro? <span style="color: #4CAF50;">Clique aqui</span></a></h6></center>
                <button type="button" style="background: #4c69ba;
                        background: -webkit-gradient(linear, center top, center bottom, from(#4c69ba), to(#3b55a0));
                        background: -webkit-linear-gradient(#4c69ba, #3b55a0);
                        border-color: #4c69ba;" onclick="FB.login(function (response) {
                                    checkLoginState();
                                }, {scope: 'public_profile,email,user_location,user_birthday'});" class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button" data-max-rows="5" data-size="xlarge" data-show-faces="false" data-auto-logout-link="false">Entrar com Facebook</button>
                <center><h6 style="margin: 15px;">ou</h6></center>
                <form onsubmit="cadastrar(this);
                        return false;">
                    <label>Nome</label>
                    <input type="text" name="nome" required="" data-clear-btn="true" >
                    <label>E-mail</label>
                    <input type="email" name="email" required="" data-clear-btn="true" >
                    <label>Senha</label>
                    <input type="password" name="senha" required="" data-clear-btn="true">
                    <button class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Cadastrar</button>
                </form>
            </div>
        </div>
        <div data-role="page" id="page_login">
            <div data-role="header" data-position="fixed">
                <a data-rel="back" class="ui-btn ui-btn-left"><i class="zmdi zmdi-arrow-back"></i></a>
                <h1>Zip Pharma</h1>
            </div>
            <div role="main" class="ui-content" data-inset="false">
                <h3>Entre com seus dados</h3>
                <form onsubmit="efetuarLogin(this);
                        return false;">
                    <input type="email" name="email" required="" data-clear-btn="true" placeholder="Entre com seu e-mail">
                    <input type="password" name="senha" required="" data-clear-btn="true">
                    <button class="ui-btn ui-btn-raised clr-primary waves-effect waves-button waves-effect waves-button">Entrar</button>
                    <input type="hidden" name="local" value="${local}"/>
                </form>
            </div>
        </div>
    </body>
</html>

