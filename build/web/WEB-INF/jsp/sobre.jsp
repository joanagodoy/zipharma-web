<%-- 
    Document   : listagem_produto
    Created on : 23/10/2015, 17:25:18
    Author     : Usuario
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zip Pharma</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="${pageContext.servletContext.contextPath}/resources/img/icon.png" type="image/x-icon" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/mobile/nd2/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <style>body{font-family: 'Roboto';} 
            /*.footMenu tr td {cursor:pointer;}*/
            .footMenu {   
                /*                border-collapse: separate; float:left;
                                          border-spacing: 25px 0px;*/
                font-weight: 400;
                font-size: 13px;color:white;

            }
            li {display: inline;padding: 10px;cursor:pointer;}
            html,
            body {
                margin:0;
                padding:0;
                height:100%;
            }
            #wrapper {
                min-height:100%;
                position:relative;
            }
            #header {


            }
            #content {
                padding-bottom:100px; /* Height of the footer element */
            }
            #footer {
                margin-left:5%;
                width:90%;
                height:60px;
                position:absolute;
                bottom:0;
                left:0;
            }
        </style>
        <!--[if lt IE 7]>
                <style type="text/css">
                        #wrapper { height:100%; }
                </style>
        <![endif]-->

    </head>
    <body style="background: url('${pageContext.servletContext.contextPath}/resources/img/fundo_web.png') no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;">
        <div id="wrapper">

            <div id="header">
            </div><!-- #header -->

            <div id="content">
                <center>
                    <img style="height: 20%;margin-top:0%;" src="${pageContext.servletContext.contextPath}/resources/img/home3.png"/>
                    <div style='width:70%;text-align: left;'>
                        <h3 style="font-weight:200;">Como funciona?</h3>
                        <p style="font-weight: 100;color:#464646;font-size:14px;">Zip Pharma é uma plataforma que foi desenvolvida para ligar de forma simples e usual pessoas e farmácias.</p>
                        <center><video style="width:90%;max-width:600px;" controls>
                                <source src="${pageContext.servletContext.contextPath}/resources/img/video.mp4" type="video/mp4"></video>
                        </center>
                        <p style="font-weight: 100;color:#464646;font-size:14px;">Através do App Zip Pharma os usuários escolhem as farmácias que desejam interagir, dessa forma podem solicitar orçamentos de qualquer produto , conversar com um atendente pelo chat, enviar foto da receita ou do produto, marcar os horários para uso de seus remédios e refazer pedidos antigos apenas com um click. As farmácias ficam responsáveis por devolver todas essas informações, assim como valores, taxa e tempo de entrega e responder  qualquer dúvida no chat.</p>
                        <h3 style="font-weight:200;">E como funciona a cobrança?</h3>
                        <p style="font-weight: 100;color:#464646;font-size:14px;">Quando uma conversa no chat é finalizada a farmácia envia uma solicitação em forma de orçamento descrevendo todos os itens e valores para que o usuário aceite o pedido. Quando o usuário manda orçamentos para as farmácias, ele recebe de volta várias solicitações e escolhe a que melhor lhe agradar. Para concluir, você deverá apenas inserir as informações do seu cartão de crédito e receber seu pedido de forma segura e rápida.</p>
                    </div>
                </center>
            </div><!-- #content -->

            <div id="footer">
                <div class="footMenu">
                    <ul>
                        <li onclick="window.location = '${pageContext.servletContext.contextPath}';">INÍCIO</li>
                        <li onclick="window.location = '${pageContext.servletContext.contextPath}/sobre';">COMO FUNCIONA</li>
                        <li  onclick="window.location = '${pageContext.servletContext.contextPath}/contato';">CONTATO</li>
                        <li>TERMOS</li>
                        <ul style="float:right;list-style-type:none;">
                            <li>© 2016 ZIP PHARMA</li>
                        </ul>
                    </ul>
                </div>
            </div><!-- #footer -->

        </div>

    </body>
</html>

