<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/default.css"/>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-2.1.0.js"></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/inputmask.js"></script>
        <title>Zip Pharma</title>
    </head>
    <body style="display: none;">

        <div id="barraTopo">
            <span style="margin-left: 20px; font-weight: 500; font-size: 20px; cursor: pointer;" onclick="teste();">Zip Pharma</span>
            (<a href='${pageContext.servletContext.contextPath}/logout'>Sair</a>)
        </div>
        <%@include file="menu.jsp" %>

        <script>

            function  teste() {
                if ($("#menuLateral").css("display") != 'none') {
                    $("#menuLateral").hide();
                } else {
                    $("#menuLateral").show();
                }
            }

        </script>