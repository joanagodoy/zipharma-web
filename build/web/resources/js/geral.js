$.postJSON = function (url, data, callback) {
    return jQuery.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'type': 'POST',
        'url': url,
        'data': JSON.stringify(data),
        'dataType': 'json',
        'success': callback
    });
};


function getTipoUsuario(id) {
    switch (id) {
        case 0:
            return "Administrador";
        case 1:
            return "Usuário";
    }
    return "";
}

function carregarForm(form, object) {
    for (var key in object) {
        if (typeof object[key] == "boolean") {
            form.find("[name='" + key + "']").val(object[key] + "");
        } else if (typeof object[key] == "object") {
            for (var key2 in object[key]) {
                form.find("[name='" + key + "." + key2 + "']").val(object[key][key2]);
            }
        } else {
            form.find("[name='" + key + "']").val(object[key]);
        }
    }
}

function dateFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + '/' + mm + '/' + yyyy;
}

function dateTimeFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var m = today.getMinutes();
    if (hh < 10)
    {
        hh = '0' + hh;
    }
    if (m < 10)
    {
        m = '0' + m;
    }
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + '/' + mm + '/' + yyyy + ' ' + hh + ":" + m;
}

function timeFormatter(cellvalue, options, rowObject) {
    if (cellvalue == null || typeof cellvalue == "undefined") {
        return "";
    }
    var today = new Date(cellvalue);
    var dd = today.getHours();
    var mm = today.getMinutes();
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }
    return today = dd + ':' + mm;
}


$(function ()
{

    $("div[data-toggle='tooltip']").tooltip();

    $('.selectpicker').selectpicker({
    });

    $(".justNumbers").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
    $(".justNumbersNegative").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [188, 110, 46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39 || event.keyCode == 109 || event.keyCode == 189)) {
                    // let it happen, don't do anything
                    return;
                } else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });


//    $("input.valor").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
    $(".cnpj").mask('99.999.999/9999-99');
    $(".cpf").mask('999.999.999-99');
    $(".cep").mask("99999-999");
    $(".telefone").mask("(99) 9999-9999");
    $(window).resize(function () {
        $(".ui-dialog-content").dialog("option", "position", ['center', 'center']);
    });

    $(".data").datepicker({
        format: "dd/mm/yyyy",
        endDate: "+0d",
        todayBtn: "linked",
        language: "pt-BR",
        daysOfWeekDisabled: "0",
        calendarWeeks: false,
        autoclose: true,
        todayHighlight: true
    });
    $('.data').css("cursor", "pointer");

    $("#loadingDialog").dialog({
        autoOpen: false,
        modal: true,
        height: 160,
        width: 300,
        closeOnEscape: false,
        resizable: false,
        dialogClass: 'noTitle'
    });
    gps();

});

function alertar(mensagem, tipo) {
    var html = '';
    var id = "alert_" + (Math.random() * 10000).toFixed(0);
    if (typeof tipo == "undefined" || tipo == "alert") {
        html += '<div id="' + id + '" class="alert alert-warning alert-dismissible fade in"  role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Atenção!</strong> ';
    } else if (tipo == "success") {
        html += '<div id="' + id + '" class="alert alert-success alert-dismissible fade in" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Muito bem!</strong> ';
    } else if (tipo == "danger") {
        html += '<div id="' + id + '" class="alert alert-danger alert-dismissible fade in" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Ocorreu um erro!</strong> ';
    } else if (tipo == "info") {
        html += '<div id="' + id + '" class="alert alert-info alert-dismissible fade in" role="alert">';
        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        html += '<strong>Aviso!</strong> ';
    }
    html += mensagem;
    html += '</div>';
    $("#alertContainer").append(html);
    var tempo = mensagem.length * 0.12 * 1000;
    if (tempo < 4000) {
        tempo = 4000;
    }
    setTimeout("$('#" + id + "').alert('close');", tempo);
}

function openLoading(msg) {
    $("#loadingDialog .msg").html(msg);
    $("#loadingDialog").dialog('open');
}

function closeLoading() {
    $("#loadingDialog").dialog('close');
}

function hideMenu() {
    if (!$(".icon-menu-3").hasClass('active')) {
        $.post("SRV.MenuCollapsed", {collapsed: "true"});
        $(".icon-menu-3").addClass('active');
        $(".leftMenu").addClass('hideMenu');
        setTimeout("$('#leftMenuContent').css('visibility','hidden');", 500);
        $(".leftMenu").hover(function (event) {
            $('#leftMenuContent').css('visibility', 'visible');
            $("#leftMenuContent").addClass("hover");
        }, function (event) {
            $("#leftMenuContent").removeClass("hover");
        });
    } else {
        $.post("SRV.MenuCollapsed", {});
        $(".leftMenu").unbind("mouseenter");
        $(".leftMenu").unbind("mouseleave");
        $('#leftMenuContent').css('visibility', 'visible');
        $(".icon-menu-3").removeClass('active');
        $(".leftMenu").removeClass('hideMenu');
        $("#leftMenuContent").removeClass('hover');
    }
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function openExcluirDialog(id) {
    $("#msgExcluir").html("Tem certeza que deseja excluir este registro?");
    $("#itemExcluir").val(id);
    $("#excluirForm").modal("show");
}

function createConfirmation(id, titulo, mensagem, funcao, funcaoClose, append) {
    var dialog = '<div id="' + id + '" style="z-index:2000;" class="modal message hide fade in" data-keyboard="false" data-backdrop="static">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
            '<h3 id="dialogHeader">' + titulo + '</h3>' +
            '</div>' +
            '<div class="modal-body" >' +
            mensagem +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-success" onclick="' + funcao + '" data-dismiss="modal">Confirmar</button>' +
            '<button type="button" class="btn btn-default" onclick="' + funcaoClose + '" data-dismiss="modal">Cancelar</button>' +
            '</div>' +
            '</div>';
    if (append) {
        $("#confirmacoesDialog").append(dialog);
    } else {
        $("#confirmacoesDialog").html(dialog);
    }
}

function printGrid(grid, title) {
    // empty the print div container.
    $('#prt-container').empty();
    // copy and append grid view to print div container.
    $('#gview_' + grid).clone().appendTo('#prt-container').css({'page-break-after': 'auto'});
    // remove navigation divs.
    $('#prt-container div').remove('.ui-jqgrid-toppager,.ui-jqgrid-titlebar,.ui-jqgrid-pager');
    // print the contents of the print container.
    $('#prt-container').printElement({leaveOpen: true, printMode: 'popup', pageTitle: title, overrideElementCSS: true, printBodyOptions: {classNameToAdd: '', styleToAdd: ".alternateRowColor:not(.ui-state-hover):not(.ui-state-highlight) { background-color: khaki !important; background-image: none !important;}th{color: white;font-weight: bold;background-color:#24A0DA;}td{font-size: 12px;}body{font-family: Verdana;}"}});
}

function printTable(grid, title) {
    // empty the print div container.
    $('#prt-container').empty();
    // copy and append grid view to print div container.
    $("#prt-container").html("<center><img src='resources/img/logo.png' style='width:120px;height:90px;'> <h3>" + title + "</h3></center><br/>");
    $('#' + grid).clone().appendTo('#prt-container').css({'page-break-after': 'auto'});
    // remove navigation divs.
    $('#prt-container div').remove('.ui-jqgrid-toppager,.ui-jqgrid-titlebar,.ui-jqgrid-pager');
    // print the contents of the print container.
    $('#prt-container').printElement({leaveOpen: true, printMode: 'popup', pageTitle: title, overrideElementCSS: true, printBodyOptions: {classNameToAdd: '', styleToAdd: ".alternateRowColor:not(.ui-state-hover):not(.ui-state-highlight) { background-color: khaki !important; background-image: none !important;}th{color: white;font-weight: bold;background-color:#24A0DA;}td{font-size: 12px;}body{font-family: Verdana;}"}});
}