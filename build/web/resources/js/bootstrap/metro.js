function openMedicamentos() {
    $("#gridMedicamentos").jqGrid({
        url: "SRV.BuscarVacinacoes",
        datatype: "json",
        height: 230,
        loadonce: true,
        hidegrid: false,
        altRows: true,
        altclass: 'alternateRowColor',
        mtype: "POST",
        multiselect: false,
        rowNum: 1000,
        colNames: ['Data', 'Quantidade', 'Medicamento', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colModel: [
            {name: 'dtData', index: 'dtData', formatter: dateFormatter, align: 'center', width: 85, search: false},
            {name: 'qtMedicamento', index: 'qtMedicamento', align: 'center', width: 90, search: false},
            {name: 'dsMedicamento', index: 'dsMedicamento', align: 'center', width: 320, search: false},
            {name: 'acoes', formatter: function(cellvalue, options, rowObject) {
                    if (rowObject['flagenviado'] != 'E') {
                        return '<img src="img/editar.png" width=18 height=18 style="cursor:pointer" onclick="carregaEditaMedicamento(\'' + JSON.stringify(rowObject).replace(new RegExp('"', 'g'), "\\'") + '\');"> <img src="img/excluir.png" width=18 height=18 style="cursor:pointer" onclick="excluirMedicamento(\'' + rowObject['id'] + '\');">';
                    }
                    return "";
                }, align: 'center', width: 60, search: false, sortable: false},
            {name: 'dtInicio', hidden: true, formatter: dateFormatter},
            {name: 'dtFim', hidden: true, formatter: dateFormatter},
            {name: 'id', hidden: true},
            {name: 'cdVendedor', hidden: true},
            {name: 'sequencia', hidden: true},
            {name: 'cdFazenda', hidden: true},
            {name: 'nrLote', hidden: true},
            {name: 'nrGalpao', hidden: true},
            {name: 'dtData', hidden: true, formatter: dateFormatter},
            {name: 'cdMedicamento', hidden: true},
            {name: 'qtMedicamento', hidden: true},
            {name: 'cdLaboratorio', hidden: true},
            {name: 'idMetodo', hidden: true},
            {name: 'vlPartida', hidden: true},
            {name: 'dtInicio', hidden: true},
            {name: 'dtFim', hidden: true},
            {name: 'idTipo', hidden: true},
            {name: 'cdDoenca', hidden: true},
            {name: 'dtSistema', hidden: true, formatter: dateFormatter},
            {name: 'idNf', hidden: true},
            {name: 'idOperacao', hidden: true},
            {name: 'dsSintomas', hidden: true},
            {name: 'dsSinais', hidden: true},
            {name: 'dsModo', hidden: true},
            {name: 'dsObservacao', hidden: true},
            {name: 'flagenviado', hidden: true},
            {name: 'flagexcluido', hidden: true},
            {name: 'dsMedicamento', hidden: true}
        ]
    });
}

function cadastrarMedicamento() {
    if ($.trim($("#med_quantidade").val()).length == 0 || isNaN(new Number($("#med_quantidade").val()))) {
        metroAlert('A quantidade digitada é inválida, digite um valor númerico');
        return;
    }
    alertInfo("Cadastrando");
    $.post("SRV.CadastrarMedicamento", {
        dtData: $("#med_data").val(),
        cdMedicamento: $("#med_medicamento").val(),
        qtMedicamento: $("#med_quantidade").val(),
        vlPartida: $("#med_partida").val(),
        cdDoenca: $("#med_doenca").val(),
        cdLaboratorio: $("#med_laboratorio").val(),
        idMetodo: $("#med_metodo").val(),
        dtInicio: $("#med_dataInicio").val(),
        dtFim: $("#med_dataFim").val(),
        idNf: $('input[name=med_nota]:checked').val(),
        idOperacao: $('input[name=med_operacao]:checked').val(),
        dsSintomas: $("#med_sintomas").val(),
        dsSinais: $("#med_sinais").val(),
        dsModo: $("#med_modo").val(),
        dsObservacao: $("#med_observacao").val(),
        id: $("#med_id").val()
    }).done(function(data) {
        fecharInfo();
        $("#gridMedicamentos").GridUnload();
        openMedicamentos();
        metroAlertClose();
        $('#med_reset').click();
        $('#cadastroMedicamento').modal('hide');
        $('#medicamentos').modal('show');
        $(".medicamentos .text4").html("Último medicamento foi " + $("#med_medicamento option:selected").text() + ", com " + $("#med_quantidade").val() + " un. em " + $("#med_data").val());
    });
}

function carregaEditaMedicamento(rowObject) {
    var obj = JSON.parse(rowObject.replace(new RegExp("'", 'g'), "\""));
    $("#med_id").val(obj.id);
    $("#med_data").val(dateFormatter(obj.dtData));
    $.post("SRV.BuscarMedicamentos").done(function(data) {
        var options1 = "";
        for (var i = 0; i < data.length; i++) {
            options1 += "<option value='" + data[i].cdMedicamento + "'>" + data[i].dsMedicamento + "</option>";
        }
        $("#med_medicamento").html(options1);
        $("#med_medicamento").val(obj.cdMedicamento);
        $.post("SRV.BuscarLaboratorios", {cdMedicamento: $("#med_medicamento").val()}).done(function(data) {
            var options2 = "";
            for (var i = 0; i < data.length; i++) {
                options2 += "<option value='" + data[i].cdLaboratorio + "'>" + data[i].dsLaboratorio + "</option>";
            }
            $("#med_laboratorio").html(options2);
            $.post("SRV.BuscarDoencas", {cdMedicamento: $("#med_medicamento").val()}).done(function(data) {
                var options3 = "";
                for (var i = 0; i < data.length; i++) {
                    options3 += "<option value='" + data[i].cdDoenca + "'>" + data[i].dsDoenca + "</option>";
                }
                $("#med_doenca").html(options3);
                $("#med_quantidade").val(obj.qtMedicamento);
                $("#med_partida").val(obj.vlPartida);
                $("#med_doenca").val(obj.cdDoenca);
                $("#med_laboratorio").val(obj.cdLaboratorio);
                $("#med_metodo").val(obj.idMetodo);
                $("#med_dataInicio").val(dateFormatter(obj.dtInicio));
                $("#med_dataFim").val(dateFormatter(obj.dtFim));
                if (obj.idNf == 'N') {
                    $("#med_nota").prop('checked', false);
                } else {
                    $("#med_nota").prop('checked', true);
                }
                $('input:radio[name=med_operacao][value=' + obj.idOperacao + ']').click();
                $("#med_sintomas").val(obj.dsSintomas);
                $("#med_sinais").val(obj.dsSinais);
                $("#med_modo").val(obj.dsModo);
                $("#med_observacao").val(obj.dsObservacao);
                $('#cadastroMedicamento').modal('show');
            });
        });
    });
}

function excluirMedicamento(id) {
    alertInfo("Excluindo");
    $.post("SRV.ExcluirMedicamento", {id: id}).done(function(data) {
        $("#gridMedicamentos").GridUnload();
        openMedicamentos();
        fecharInfo();
    });
}

function carregaCadastroMedicamento() {
    $("#med_id").val(-1);
    $.post("SRV.BuscarMedicamentos").done(function(data) {
        var options = "";
        for (var i = 0; i < data.length; i++) {
            options += "<option value='" + data[i].cdMedicamento + "'>" + data[i].dsMedicamento + "</option>";
        }
        $("#med_medicamento").html(options);
        med_medicamentoChanged();
        $('#cadastroMedicamento').modal('show');
    });
}

function med_medicamentoChanged() {
    $.post("SRV.BuscarLaboratorios", {cdMedicamento: $("#med_medicamento").val()}).done(function(data) {
        var options = "";
        for (var i = 0; i < data.length; i++) {
            options += "<option value='" + data[i].cdLaboratorio + "'>" + data[i].dsLaboratorio + "</option>";
        }
        $("#med_laboratorio").html(options);
    });
    $.post("SRV.BuscarDoencas", {cdMedicamento: $("#med_medicamento").val()}).done(function(data) {
        var options = "";
        for (var i = 0; i < data.length; i++) {
            options += "<option value='" + data[i].cdDoenca + "'>" + data[i].dsDoenca + "</option>";
        }
        $("#med_doenca").html(options);
    });
}

function openMortalidade() {
    $("#gridMortalidade").jqGrid({
        url: "SRV.BuscarMortalidades",
        datatype: "json",
        height: 210,
        loadonce: true,
        hidegrid: false,
        altRows: true,
        altclass: 'alternateRowColor',
        mtype: "POST",
        multiselect: false,
        rowNum: 1000,
        footerrow: true,
        loadComplete: function() {
            $("#gridMortalidade").jqGrid('footerData', 'set', {dtData: 'Total:', qtNatural: $("#gridMortalidade").jqGrid('getCol', 'qtNatural', false, 'sum'), qtAtaque: $("#gridMortalidade").jqGrid('getCol', 'qtAtaque', false, 'sum'), qtLocomotor: $("#gridMortalidade").jqGrid('getCol', 'qtLocomotor', false, 'sum'), qtAscite: $("#gridMortalidade").jqGrid('getCol', 'qtAscite', false, 'sum'), qtCaquexia: $("#gridMortalidade").jqGrid('getCol', 'qtCaquexia', false, 'sum'), qtOutros: $("#gridMortalidade").jqGrid('getCol', 'qtOutros', false, 'sum')});
        },
        colNames: ['Data', 'Natural', 'Ataque', 'Locomotor', 'Ascite', 'Caquexia', 'Outros', '', '', '', ''],
        colModel: [
            {name: 'dtData', index: 'dtData', formatter: dateFormatter, align: 'center', width: 85, search: false},
            {name: 'qtNatural', formatter: blankZeroFormat, index: 'qtNatural', align: 'center', width: 90, search: false},
            {name: 'qtAtaque', formatter: blankZeroFormat, index: 'qtAtaque', align: 'center', width: 90, search: false},
            {name: 'qtLocomotor', formatter: blankZeroFormat, index: 'qtLocomotor', align: 'center', width: 90, search: false},
            {name: 'qtAscite', formatter: blankZeroFormat, index: 'qtAscite', align: 'center', width: 90, search: false},
            {name: 'qtCaquexia', formatter: blankZeroFormat, index: 'qtCaquexia', align: 'center', width: 90, search: false},
            {name: 'qtOutros', formatter: blankZeroFormat, index: 'qtOutros', align: 'center', width: 90, search: false},
            {name: 'acoes', formatter: function(cellvalue, options, rowObject) {
                    if (rowObject['flagenviado'] != 'E') {
                        return '<img src="img/editar.png" width=18 height=18 style="cursor:pointer" onclick="carregaEditaMortalidade(\'' + JSON.stringify(rowObject).replace(new RegExp('"', 'g'), "\\'") + '\');"> <img src="img/excluir.png" width=18 height=18 style="cursor:pointer" onclick="excluirMortalidade(\'' + rowObject['id'] + '\');">';
                    }
                    return "";
                }, align: 'center', width: 60, search: false, sortable: false},
            {name: 'id', hidden: true},
            {name: 'flagenviado', hidden: true},
            {name: 'flagexcluido', hidden: true}
        ]
    });
}

function cadastrarMortalidade() {
    if (isNaN(new Number($("#mor_qtNatural").val())) || isNaN(new Number($("#mor_qtAtaque").val())) || isNaN(new Number($("#mor_qtLocomotor").val())) || isNaN(new Number($("#mor_qtAscite").val())) || isNaN(new Number($("#mor_qtCaquexia").val())) || isNaN(new Number($("#mor_qtOutros").val()))) {
        metroAlert('A quantidade digitada é inválida, digite um valor númerico');
        return;
    }
    if ($.trim($("#mor_qtNatural").val()).length == 0 && $.trim($("#mor_qtAtaque").val()).length == 0 && $.trim($("#mor_qtLocomotor").val()).length == 0 && $.trim($("#mor_qtAscite").val()).length == 0 && $.trim($("#mor_qtCaquexia").val()).length == 0 && $.trim($("#mor_qtOutros").val()).length == 0) {
        metroAlert('Digite pelo menos uma quantidade');
        return;
    }
    alertInfo("Cadastrando");
    $.post("SRV.CadastrarMortalidade", {
        dtData: $("#mor_dtData").val(),
        qtNatural: $("#mor_qtNatural").val(),
        qtAtaque: $("#mor_qtAtaque").val(),
        qtLocomotor: $("#mor_qtLocomotor").val(),
        qtAscite: $("#mor_qtAscite").val(),
        qtCaquexia: $("#mor_qtCaquexia").val(),
        qtOutros: $("#mor_qtOutros").val(),
        id: $("#mor_id").val()
    }).done(function(data) {
        fecharInfo();
        $("#gridMortalidade").GridUnload();
        openMortalidade();
        metroAlertClose();
        $('#mor_reset').click();
        $('#cadastroMortalidade').modal('hide');
        $('#mortalidade').modal('show');
        $(".mortalidade .text4").html("Na data de " + $("#mor_dtData").val() + " houveram " + $("#mor_qtNatural").val() + " mortes naturais, " + $("#mor_qtAtaque").val() + " por ataque, e " + (new Number($("#mor_qtCaquexia").val()) + new Number($("#mor_qtAscite").val()) + new Number($("#mor_qtLocomotor").val()) + new Number($("#mor_qtOutros").val())) + " eliminados");
    });
}

function carregaEditaMortalidade(rowObject) {
    var obj = JSON.parse(rowObject.replace(new RegExp("'", 'g'), "\""));
    $("#mor_id").val(obj.id);
    $("#mor_dtData").val(dateFormatter(obj.dtData));
    $("#mor_qtNatural").val(obj.qtNatural);
    $("#mor_qtAtaque").val(obj.qtAtaque);
    $("#mor_qtLocomotor").val(obj.qtLocomotor);
    $("#mor_qtAscite").val(obj.qtAscite);
    $("#mor_qtCaquexia").val(obj.qtCaquexia);
    $("#mor_qtOutros").val(obj.qtOutros);
    $('#cadastroMortalidade').modal('show');
}

function excluirMortalidade(id) {
    alertInfo("Excluindo");
    $.post("SRV.ExcluirMortalidade", {id: id}).done(function(data) {
        $("#gridMortalidade").GridUnload();
        openMedicamentos();
        fecharInfo();
    });
}

function carregaCadastroMortalidade() {
    $("#mor_id").val(-1);
    $('#cadastroMortalidade').modal('show');
}

function mor_qtChanged() {
    var quantidade = new Number($("#mor_qtCaquexia").val()) + new Number($("#mor_qtAscite").val()) + new Number($("#mor_qtLocomotor").val()) + new Number($("#mor_qtOutros").val());
    $("#mor_qtTotal").val(quantidade);
}

function blankZeroFormat(cellvalue) {
    if (cellvalue === undefined || (cellvalue == null) || cellvalue === '') {
        return 0;
    }
    return cellvalue;
}

function openHistorico() {
    $("#gridHistorico").jqGrid({
        url: "SRV.BuscarHistoricos",
        datatype: "json",
        height: 210,
        loadonce: true,
        hidegrid: false,
        altRows: true,
        altclass: 'alternateRowColor',
        mtype: "POST",
        multiselect: false,
        rowNum: 15000,
        colNames: ['Tipo', 'Descrição', 'Valor', 'Média Filial'],
        colModel: [
            {name: 'idTipo', index: 'idTipo', formatter: his_tipoFormatter, align: 'center', width: 120, search: false},
            {name: 'dsHistorico', index: 'dsHistorico', formatter: dateFormatter, align: 'center', width: 90, search: false},
            {name: 'vlHistorico', index: 'vlHistorico', formatter: dateFormatter, align: 'center', width: 90, search: false},
            {name: 'vlMediafilial', index: 'vlMediafilial', align: 'center', width: 120, search: false}
        ]
    });
}

function his_tipoFormatter(cellvalue) {
    if (cellvalue == '0') {
        return 'Ativo';
    }
    if (cellvalue == '1') {
        return 'Anterior';
    }
    if (cellvalue == '2') {
        return 'Fechado';
    }
}

function openPeso() {
    $("#gridPeso").jqGrid({
        url: "SRV.BuscarPesos",
        datatype: "json",
        height: 210,
        loadonce: true,
        hidegrid: false,
        altRows: true,
        altclass: 'alternateRowColor',
        mtype: "POST",
        multiselect: false,
        rowNum: 1000,
        colNames: ['Data', 'Idade', 'Peso Médio', 'Amostras', 'Mortalidade', '', '', '', ''],
        colModel: [
            {name: 'dtPeso', index: 'dtPeso', formatter: dateFormatter, align: 'center', width: 85, search: false},
            {name: 'idade', formatter: blankZeroFormat, index: 'idade', align: 'center', width: 90, search: false},
            {name: 'qtPesomedio', formatter: blankZeroFormat, index: 'qtPesomedio', align: 'center', width: 90, search: false},
            {name: 'qtAmostras', formatter: blankZeroFormat, index: 'qtAmostras', align: 'center', width: 90, search: false},
            {name: 'qtMortalidade', formatter: blankZeroFormat, index: 'qtMortalidade', align: 'center', width: 90, search: false},
            {name: 'acoes', formatter: function(cellvalue, options, rowObject) {
                    if (rowObject['flagenviado'] != 'E') {
                        return '<img src="img/editar.png" width=18 height=18 style="cursor:pointer" onclick="carregaEditaPeso(\'' + JSON.stringify(rowObject).replace(new RegExp('"', 'g'), "\\'") + '\');"> <img src="img/excluir.png" width=18 height=18 style="cursor:pointer" onclick="excluirPeso(\'' + rowObject['id'] + '\');">';
                    }
                    return "";
                }, align: 'center', width: 60, search: false, sortable: false},
            {name: 'id', hidden: true},
            {name: 'flagenviado', hidden: true},
            {name: 'flagexcluido', hidden: true}
        ]
    });
}

function cadastrarPeso() {
    if (isNaN(new Number($("#pes_qtAmostras").val())) || isNaN(new Number($("#pes_qtPesomedio").val())) || isNaN(new Number($("#pes_qtMortalidade").val()))) {
        metroAlert('A quantidade digitada é inválida, digite um valor númerico');
        return;
    }
    if ($.trim($("#pes_qtAmostras").val()).length == 0){
        metroAlert('Digite a quantidade de amostras');
        return;
    }
    if ($.trim($("#pes_qtPesomedio").val()).length == 0){
        metroAlert('Digite o peso médio');
        return;
    }
    alertInfo("Cadastrando");
    $.post("SRV.CadastrarPeso", {
        dtPeso: $("#pes_dtPeso").val(),
        qtAmostras: $("#pes_qtAmostras").val(),
        qtPesomedio: $("#pes_qtPesomedio").val(),
        qtMortalidade: $("#pes_qtMortalidade").val(),
        idBrkill: $('input[name=pes_idBrkill]:checked').val(),
        id: $("#pes_id").val()
    }).done(function(data) {
        fecharInfo();
        $("#gridPeso").GridUnload();
        openPeso();
        metroAlertClose();
        $('#pes_reset').click();
        $('#cadastroPeso').modal('hide');
        $('#peso').modal('show');
        $(".peso .text4").html("Em "+$("#pes_dtPeso").val()+" obteve-se peso médio de "+$("#pes_qtPesomedio").val()+" KG em uma amostra de "+$("#pes_qtAmostras").val()+" aves");
    });
}

function carregaEditaPeso(rowObject) {
    var obj = JSON.parse(rowObject.replace(new RegExp("'", 'g'), "\""));
    $("#pes_id").val(obj.id);
    $("#pes_dtPeso").val(dateFormatter(obj.dtPeso));
    $("#pes_qtAmostras").val(obj.qtAmostras);
    $("#pes_qtPesomedio").val(obj.qtPesomedio);
    $("#pes_qtMortalidade").val(obj.qtMortalidade);
     if (obj.idBrkill == '0') {
                    $("#pes_idBrkill").prop('checked', false);
                } else {
                    $("#pes_idBrkill").prop('checked', true);
                }
    $('#cadastroPeso').modal('show');
}

function excluirPeso(id) {
    alertInfo("Excluindo");
    $.post("SRV.ExcluirPeso", {id: id}).done(function(data) {
        $("#gridPeso").GridUnload();
        openPeso();
        fecharInfo();
    });
}

function carregaCadastroPeso() {
    $("#pes_id").val(-1);
    $('#cadastroPeso').modal('show');
}

function openEstoque() {
    $("#gridEstoque").jqGrid({
        url: "SRV.BuscarEstoques",
        datatype: "json",
        height: 210,
        loadonce: true,
        hidegrid: false,
        altRows: true,
        altclass: 'alternateRowColor',
        mtype: "POST",
        multiselect: false,
        rowNum: 1000,
        colNames: ['Data', 'Quantidade', 'Identificador', '', '', '', ''],
        colModel: [
            {name: 'dtEstoque', index: 'dtEstoque', formatter: dateFormatter, align: 'center', width: 85, search: false},
            {name: 'qtEstoque', index: 'qtEstoque', align: 'center', width: 90, search: false},
            {name: 'idEstoque',  index: 'idEstoque', align: 'center', width: 90, search: false},
            {name: 'acoes', formatter: function(cellvalue, options, rowObject) {
                    if (rowObject['flagenviado'] != 'E') {
                        return '<img src="img/editar.png" width=18 height=18 style="cursor:pointer" onclick="carregaEditaEstoque(\'' + JSON.stringify(rowObject).replace(new RegExp('"', 'g'), "\\'") + '\');"> <img src="img/excluir.png" width=18 height=18 style="cursor:pointer" onclick="excluirEstoque(\'' + rowObject['id'] + '\');">';
                    }
                    return "";
                }, align: 'center', width: 60, search: false, sortable: false},
            {name: 'id', hidden: true},
            {name: 'flagenviado', hidden: true},
            {name: 'flagexcluido', hidden: true}
        ]
    });
}

function cadastrarEstoque() {
    if (isNaN(new Number($("#est_qtEstoque").val()))) {
        metroAlert('A quantidade digitada é inválida, digite um valor númerico');
        return;
    }
    if ($.trim($("#est_qtEstoque").val()).length == 0){
        metroAlert('Digite a quantidade do estoque');
        return;
    }
   
    alertInfo("Cadastrando");
    $.post("SRV.CadastrarEstoque", {
        dtEstoque: $("#est_dtEstoque").val(),
        qtEstoque: $("#est_qtEstoque").val(),
        idEstoque: $('input[name=est_idEstoque]:checked').val(),
        id: $("#est_id").val()
    }).done(function(data) {
        fecharInfo();
        $("#gridEstoque").GridUnload();
        openEstoque();
        metroAlertClose();
        $('#est_reset').click();
        $('#cadastroEstoque').modal('hide');
        $('#estoque').modal('show');
        $(".estoque .text4").html("Estoque em "+$("#est_qtEstoque").val()+" KG em "+$("#est_dtEstoque").val());
    });
}

function carregaEditaEstoque(rowObject) {
    var obj = JSON.parse(rowObject.replace(new RegExp("'", 'g'), "\""));
    $("#est_id").val(obj.id);
    $("#est_dtEstoque").val(dateFormatter(obj.dtEstoque));
    $("#est_qtEstoque").val(obj.qtEstoque);
     $('input:radio[name=est_idEstoque][value=' + obj.idEstoque + ']').click();
    $('#cadastroEstoque').modal('show');
}

function excluirEstoque(id) {
    alertInfo("Excluindo");
    $.post("SRV.ExcluirEstoque", {id: id}).done(function(data) {
        $("#gridEstoque").GridUnload();
        openEstoque();
        fecharInfo();
    });
}

function carregaCadastroEstoque() {
    $("#est_id").val(-1);
    $('#cadastroEstoque').modal('show');
}