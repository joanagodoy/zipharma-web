function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#adicionarReceitaImage').attr('src', e.target.result);
        }
        $("#adicionarReceitaImage").show();
        reader.readAsDataURL(input.files[0]);
    }
}

function adicionarReceita() {
    if ($('#page_adicionarReceitaForm input[type="file"]').get(0).files.length === 0) {
        alertarMobile("Selecione uma imagem para enviar");
        return false;
    }
    $.mobile.loading('show');
    $("#page_adicionarReceitaForm").ajaxSubmit({success: receitaAdicionada});
    return false;
}

function receitaAdicionada() {
    $.mobile.loading('hide');
    window.history.back();
}