$(document).on("pageinit", "#page_finalizar", function() {
    Inputmask().mask(document.querySelectorAll("input"));
    enderecoChanged($("#page_finalizarForm [name='endereco']"));
    formaPagamentoChanged($("#page_finalizarForm [name='formaPagamento']"));
});

function onCidadeFiltroChange(c) {
    try {
        $("#checkboxAll").prop("checked", false).checkboxradio("refresh");
        $(".finalizarFarmacias div div:not(:first)").each(function(index) {
            if ($(this).find(".cidade").html() == $(c).val().split(";")[0]) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    } catch (e) {

    }
}

function checkNmb(campo) {
    var valor = 0;
    if (!isNaN($(campo).val().replace(",", "."))) {
        valor = new Number($(campo).val().replace(",", "."));
    }
    valor = new Number(valor.toFixed(2));
    $(campo).val(valor.toFixed(2).toString().replace(".", ","));
}

function formaPagamentoChanged(e) {
    $(".dadosCartao").hide();
    $(".dadosDinheiro").hide();
    $(".dadosCartao").find('input').each(function(index) {
        $(this).prop("required", false);
    });
    $(".dadosDinheiro").find('input').each(function(index) {
        $(this).prop("required", false);
    });
    if ($(e).val() == 1) {
        $(".dadosCartao").show();
        $(".dadosCartao").find('input').each(function(index) {
            $(this).prop("required", true);
        });
    } else {
        $(".dadosDinheiro").show();
        $(".dadosDinheiro").find('input').each(function(index) {
            $(this).prop("required", true);
        });
    }
}

function enderecoChanged(e) {
    var required = false;
    if ($(e).val() == "N") {
        required = true;
        $(".dadosEndereco").show();
    } else {
        $(".dadosEndereco").hide();
    }
    $(".dadosEndereco").find('input').each(function(index) {
        $(this).prop("required", required);
    });
}

function checkAllFarmacia() {
    var check;
    if ($("#checkboxAll").prop("checked")) {
        check = true;
    } else {
        check = false;
    }
    $(".finalizarFarmacias div div:not(:first):not(:hidden)").each(function(index) {
        $(this).find("input").prop("checked", check).checkboxradio("refresh");
        $(this).find("input").change();
    });
}

function dialogFarmacias() {
    $("#checkboxAll").prop("checked", false).checkboxradio("refresh");
    onCidadeFiltroChange($("#filtroCidade"));
    $('#popupFarmacias').popup({positionTo: 'window'}).popup('open');

}

function finalizarPedido() {
    if ($("#page_finalizarForm [name='endereco']").val() == "N" && ($("#page_finalizarForm [name='descricao']").val().trim().length == 0 || $("#page_finalizarForm [name='logradouro']").val().trim().length == 0 || $("#page_finalizarForm [name='bairro']").val().trim().length == 0)) {
        alertarMobile("Digite o endereço de entrega completo");
        return false;
    }
    var farmacias = [];
    $(".checkboxFarmacia").each(function() {
        if ($(this).prop("checked") == true) {
            farmacias.push($(this).attr("name").toString().replace("checkboxFarmacia", ""));
        }
    });
    if (farmacias.length < 1) {
        alertarMobile("Selecione no mínimo uma farmácia!");
        return false;
    }
    var form = $("#page_finalizarForm").serializeArray();
    form.push({name: 'farmacias', value: farmacias.length == 0 ? '' : farmacias.toString()});
    $("#page_finalizarSubmit").hide();

    //Enviar form via post
    $.post("finalizarOrcamento", form).done(function(data) {
        //Preparar a mudança de página para a orçamentos
        setTimeout('$.mobile.changePage("../orcamentos")', 500);
        //Voltar para o home antes de trocar a página
        window.history.go(-2);
    });
    return false;
}

function onClickCheckFarmacia(campo) {

    var idFarmacia = $(campo).attr("name").toString().replace("checkboxFarmacia", "");
    var b = true;
    if ($("#selectFarmaciaDisabled option[value=" + idFarmacia + "]").prop("selected") == true) {
        b = false;
    }
    $("#selectFarmaciaDisabled option[value=" + idFarmacia + "]").prop("selected", b);
    $("#selectFarmaciaDisabled").selectmenu("refresh");


}