$(document).on("pageinit", "#page_produto", function() {
    $("#swipebox-overlay").remove();
    $('.swipebox').swipebox({
        useCSS: false, // false will force the use of jQuery for animations
        useSVG: false, // false to force the use of png for buttons
        initialIndexOnArray: 0, // which image index to init when a array is passed
        hideCloseButtonOnMobile: false, // true will hide the close button on mobile devices
        hideBarsDelay: 3000, // delay before hiding bars on desktop
        videoMaxWidth: 1140, // videos max width
        beforeOpen: function() {
        }, // called before opening
        afterOpen: null, // called after opening
        afterClose: function() {
        }, // called after closing
        loopAtEnd: false // true will return to the first image after the last image is reached
    });
});

function lerMais(nome) {
    //verificar se pode clicar (pelo tamanho)
    if (nome.length > 15) {
        $("#lerMaisContent").html(nome);
        setTimeout("$('#lerMais').popup({ positionTo: 'window' }).popup('open');", 50);
    }
}

function carrinho() {
    setTimeout('$.mobile.changePage("../carrinho", {transition: "slide"});', 1000);
    window.history.go(-2);
}

function adicionarCarrinho(id, carrinho) {
    $.mobile.loading('show');
    $.post("carrinho/adicionar", {id: id}).done(function(data) {
        $.mobile.loading('hide');
        if (carrinho == 'S') {
            window.history.go(-3);
            setTimeout('$.mobile.changePage("../carrinho", {transition: "slide"});', 500);
            //Voltar para o home antes de trocar a página
            
        } else {
            if (data) {
                alertarMobile("Produto adicionado ao carrinho");
            } else {
                alertarMobile("Ocorreu um erro no carrinho");
            }
        }
    });
}