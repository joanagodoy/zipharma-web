$(document).on("pageinit", "#page_pagar", function() {
    $("#pagamentoCod").val('');
    $(".bandeirasCartao").hide();
    iniciarSessaoIugu();
});

var tokenCc = null;

function iniciarSessaoIugu() {

    $.post("../iugu/iniciarSessao").done(function(data) {
        Iugu.setAccountID(data);
        Iugu.setTestMode(true);
        if ($("#pagamentoCartaoCredito").val() != '') {
            verificarCartao();
        }
    });

}

function validaCartao() {
    tokenCc = null;
    var v = $("#pagamentoCartaoCredito").val();
    if (v.indexOf(".") >= 0 || v.indexOf(",") >= 0 || v.indexOf("-") >= 0 || v.indexOf("+") >= 0 || isNaN(v)) {
        alertarMobile("Cartão inválido, digite apenas números!");
        $("#pagamentoCartaoCredito").focus();
        return false;
    } else if (v.length != 16) {
        alertarMobile("Preencha todos os dígitos do cartão!");
        $("#pagamentoCartaoCredito").focus();
        return false;
    }
    return true;
}

function verificarCartao() {
    $(".bandeirasCartao").hide();
    if (validaCartao()) {
        var ccNumber = $("#pagamentoCartaoCredito").val();
        var result = Iugu.utils.getBrandByCreditCardNumber(ccNumber);
        if (result == false) {
            alertarMobile("Cartão de crédito inválido!");
            $("#pagamentoCartaoCreditoBandeira").val("");
            $("#pagamentoCartaoCredito").focus();
            return false;
        } else {
            //As bandeiras suportadas são "visa", "mastercard", "amex" e "diners".
            $("#imgBandeira" + result).show();
            if (result == 'visa') {
                result = 'Visa';
            } else if (result == 'mastercard') {
                result = 'Mastercard';
            } else if (result == 'amex') {
                result = 'American Express';
            } else if (result == 'diners') {
                result = 'Diners Club';
            }
            $("#pagamentoCartaoCreditoBandeira").val(result.toString());
            return true;
        }
    }
    return false;
}

function dialogConfirmacaoPagamento() {
    setTimeout("$('#confirmarPagamento').popup({ positionTo: 'window' }).popup('open');", 50);
}

var charging = 0;
function charge() {
    $.mobile.loading('show');
    if (tokenCc != null && charging == 0) {
        charging = 1;
        $.post("../iugu/charge", {
            tokenCc: tokenCc,
            idPedido: $("#idPedidoPagar").val(),
            cartao: $("#pagamentoCartaoCredito").val(),
            nome: $("#pagamentoNome").val(),
            sobrenome: $("#pagamentoSobrenome").val(),
            mes: $("#pagamentoMes").val(),
            ano: $("#pagamentoAno").val()
        }
        ).done(function(data) {
            $("#confirmarPagamento").popup('close');
            $.mobile.loading('hide');
            if (data.indexOf("true;") >= 0 || data.indexOf("false;") >= 0) {
                alertarMobile(data.split(";")[2]);
                setTimeout(function() {
                    $.mobile.changePage("pedido?id=" + $("#idPedidoPagar").val(), {
                        type: "post",
                        changeHash: true
                    });
                }, 2000);
            } else {
                alertarMobile('Ocorreu algum erro, tente novamente!');
            }
        });
    } else {
        $.mobile.loading('hide');
        alertarMobile("Tente novamente!");
    }
}

function efetuarPagamento() {
    $.mobile.loading('show');
    if (verificarCartao() && verificaMesAno($("#pagamentoMes")) && verificaMesAno($("#pagamentoAno"))) {
        $.post("../iugu/gerarTokenCc", {
            cartao: $("#pagamentoCartaoCredito").val(),
            cod: $("#pagamentoCod").val(),
            nome: $("#pagamentoNome").val(),
            sobrenome: $("#pagamentoSobrenome").val(),
            mes: $("#pagamentoMes").val(),
            ano: $("#pagamentoAno").val()
        }).done(function(data) {
            $.mobile.loading('hide');
            if (data.indexOf('erro') == -1 && data != '') {
                tokenCc = data;
                dialogConfirmacaoPagamento();
            } else {
                tokenCc = null;
                alertarMobile('Ocorreu algum erro!');
            }
        });
    } else {
        $.mobile.loading('hide');
    }
    return false;
}

function verificaMesAno(campo) {
    var v = $(campo).val();
    if (v.indexOf(".") >= 0 || v.indexOf(",") >= 0 || v.indexOf("-") >= 0 || v.indexOf("+") >= 0 || isNaN(v)) {
        alertarMobile("Digite apenas números!");
        $(campo).focus();
        return false;
    } else if (v.length != 2 && v.length != 4) {
        alertarMobile("Preencha utilizando 2 dígitos para o mês e 4 dígitos para o ano!");
        $(campo).focus();
        return false;
    }
    return true;
}