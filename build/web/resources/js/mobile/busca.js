$(document).on("pageinit", "#page_busca", function() {
    $.mobile.loading('show');
    setTimeout("buscarProdutos();", 100);
});

function buscarProdutos() {
    $.post("buscarProdutos", {query: $("#busca_query").val(), categoria: $("#busca_categoria").val(), offset: $("#busca ul li").length, max: MAX_RESULT}).done(function(data) {
        $("#busca ul").append(data);
        setTimeout('$("img.lazy").lazyload({effect: "fadeIn",skip_invisible: false, threshold :400});', 250);
        if (data.trim().length == 0 || $("#busca ul li").length % MAX_RESULT != 0) {
            $("#busca > a").hide();
        } else {
            $("#busca > a").show();
        }
        if ($("#busca ul li").length == 0) {
            $("#busca").html("<br><center><span style='font-size: 14px; font-weight: 100;'>Nenhum registro à ser exibido.</span></center>");
        }
        $('#busca ul').listview('refresh');
        $.mobile.loading('hide');
    });
    
}
