function efetuarLogin(f) {
    $.mobile.loading('show');
    $.post("efetuarLogin", {email: $(f).find("[name=email]").val(), senha: $(f).find("[name=senha]").val()}).done(function (data) {
        $.mobile.loading('hide');
        if (data) {
            window.location = "home";
        } else {
            $(f)[0].reset();
            alertarMobile("E-mail ou senha incorretos!");
        }
    });
}

function efetuarLoginFacebook(id, nome, email, cidade, nascimento) {
    $.mobile.loading('show');
    $.post("efetuarLogin", {facebook: id, nome: nome, email: email, cidade: cidade, nascimento: nascimento}).done(function (data) {
        $.mobile.loading('hide');
        if (data) {
            window.location = "home";
        } else {
            alertarMobile("Algum erro ocorreu na sincronização!");
        }
    });
}

function cadastrar(f) {
    if ($(f).find("[name=senha]").val().trim().length < 6) {
        alertarMobile("A senha deve ter 6 caracteres pelo menos");
        return;
    }
    $.mobile.loading('show');
    $.post("cadastrar", {nome: $(f).find("[name=nome]").val(), email: $(f).find("[name=email]").val(), senha: $(f).find("[name=senha]").val()}).done(function (data) {
        $.mobile.loading('hide');
        if (data) {
            window.location = "home";
        } else {
            alertarMobile("Este e-mail está cadastrado!");
        }
    });
}