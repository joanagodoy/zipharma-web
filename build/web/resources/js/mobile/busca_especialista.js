$(document).on("pageinit", "#page_busca_especialista", function() {
    $.mobile.loading('show');
    setTimeout("buscarEspecialistas();", 100);
});

function buscarEspecialistas() {
    $.post("buscarEspecialistas", {query: $("#busca_query_especialidade").val(), especialidade: $("#busca_especialidade").val(), offset: $("#busca_especialista ul li").length, max: MAX_RESULT}).done(function(data) {
        $("#busca_especialista ul").append(data);
        setTimeout('$("img.lazy").lazyload({effect: "fadeIn",skip_invisible: false, threshold :400});', 250);
        if (data.trim().length == 0 || $("#busca_especialista ul li").length % MAX_RESULT != 0) {
            $("#busca_especialista > a").hide();
        } else {
            $("#busca_especialista > a").show();
        }
        if ($("#busca_especialista ul li").length == 0) {
            $("#busca_especialista").html("<br><center><span style='font-size: 14px; font-weight: 100;'>Nenhum registro à ser exibido.</span></center>");
        }
        $('#busca_especialista ul').listview('refresh');
        $.mobile.loading('hide');
    });
}
