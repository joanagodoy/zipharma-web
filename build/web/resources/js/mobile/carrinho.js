$(document).on("pageinit", "#page_carrinho", function() {
    $('.swipebox').swipebox({
        useCSS: true,
        useSVG: true,
        initialIndexOnArray: 0,
        hideCloseButtonOnMobile: false,
        hideBarsDelay: 3000,
        videoMaxWidth: 1140,
        beforeOpen: function() {
        },
        afterOpen: null,
        afterClose: function() {
        },
        loopAtEnd: false
    });
    setTimeout('$("#tabItensCarrinho").click();', 100);
});

function carrinhoAdicionar(id, idItem) {
    $.mobile.loading('show');
    $.post("carrinho/adicionar", {id: id, idItem: idItem}).done(function(data) {
    });
    var identificador = "";
    if (id == 0) {
        identificador = ".card_item_" + idItem;
    } else {
        identificador = ".card_" + id;
    }
    $(identificador).find(".quantidade").val(new Number($(identificador).find(".quantidade").val()) + 1);
    $.mobile.loading('hide');
}

function adicionarProduto(descricao) {
    if (descricao.trim().length > 0) {
        $.mobile.loading('show');
        $.post("carrinho/adicionar", {descricao: descricao}).done(function(data) {
            $.mobile.changePage(window.location.href, {
                allowSamePageTransition: true,
                transition: 'none',
                changeHash: false,
                reloadPage: true
            });
        });
    }
}

function carrinhoDiminuir(id, idItem) {
    $.mobile.loading('show');
    var identificador = "";
    if (id == 0) {
        identificador = ".card_item_" + idItem;
    } else {
        identificador = ".card_" + id;
    }
    if (new Number($(identificador).find(".quantidade").val()) > 1) {
        $.post("carrinho/diminuir", {id: id, idItem: idItem}).done(function(data) {
        });
        $(identificador).find(".quantidade").val(new Number($(identificador).find(".quantidade").val()) - 1);
    }
    $.mobile.loading('hide');
}

function carrinhoRemover(id, idItem) {
    $.mobile.loading('show');
    $.post("carrinho/remover", {id: id, idItem: idItem}).done(function(data) {
    });
    var identificador = "";
    if (id == 0) {
        identificador = ".card_item_" + idItem;
    } else {
        identificador = ".card_" + id;
    }
    $(identificador).remove();
    $.mobile.loading('hide');
}

function removerReceita(id) {
    $.mobile.loading('show');
    $.post("receita/remover", {id: id}).done(function(data) {
        if (data) {
            $(".receita_" + id).remove();
        }
        $.mobile.loading('hide');
    });
}

function alterarReceitaCarrinho(){
    $.mobile.loading('show');
    $.post("receita/alterar", $("#alterarReceitaForm").serialize()).done(function(data) {
        if (data) {
            alertarMobile("Registros Salvos com Sucesso");
        }
        $.mobile.loading('hide');
    });
}


function finalizarCarrinho() {
    $.mobile.loading('show');
    $.get("carrinho/confirmar").done(function(data) {
        $.mobile.loading('hide');
        if (data == "E") {
            alertarMobile("Adicione produtos ou receitas para continuar");
        } else if (data == "R") {
            alertarMobile("Você precisa incluir receita para este tipo de medicamento");
        } else {
            $.mobile.changePage("carrinho/finalizar", {
                type: "GET",
                changeHash: true
            });
        }
    });
}
