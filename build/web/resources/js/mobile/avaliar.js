function enviarAvaliacao() {

    if (verificarPreenchimentoAvaliacao()) {
        $.mobile.loading('show');
        $.post("finalizarAvaliacao", $("#formAvaliacao").serialize()
                ).done(function(data) {
            $.mobile.changePage("pedido?id=" + $("#idPedidoAvaliacao").val(), {
                type: "post",
                changeHash: true
            });
        });
    } else {
        alertarMobile("Avaliação não preenchida!");
    }

}

function verificarPreenchimentoAvaliacao() {
    if (qtdQualidade == -1) {
        return false;
    } else if (qtdRapidezEntrega == -1) {
        return false;
    }
    return true;
}

var qtdQualidade = -1;
function qualidadeAtendimento(v) {

    $(".qualidade").css("color", "rgb(187, 187, 187)");
    $(".qualidade").css("text-shadow", "none");
    if (v != qtdQualidade) {
        for (var a = 1; a <= v; a++) {
            $("#qualidade" + a).css("color", "rgb(230, 100, 120)");
            $("#qualidade" + a).css("text-shadow", "1px 1px 1px black");
        }
        qtdQualidade = v;
    } else {
        qtdQualidade = 0;
    }
    $("#formAvaliacao [name='qualidade']").val(qtdQualidade);

}

var qtdRapidezEntrega = -1;
function rapidezEntrega(v) {

    $(".rapidezEntrega").css("color", "rgb(187, 187, 187)");
    $(".rapidezEntrega").css("text-shadow", "none");
    if (v != qtdRapidezEntrega) {
        for (var a = 1; a <= v; a++) {
            $("#rapidezEntrega" + a).css("color", "rgb(230, 100, 120)");
            $("#rapidezEntrega" + a).css("text-shadow", "1px 1px 1px black");
        }
        qtdRapidezEntrega = v;
    } else {
        qtdRapidezEntrega = 0;
    }
    $("#formAvaliacao [name='rapidez']").val(qtdRapidezEntrega);
}